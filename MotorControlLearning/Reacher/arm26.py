import mujoco_py
import os

mj_path = mujoco_py.utils.discover_mujoco()
xml_path = os.path.join(mj_path, 'model', 'arm26.xml')

model = mujoco_py.load_model_from_path(xml_path)
sim = mujoco_py.MjSim(model)
view = mujoco_py.MjViewer(sim)

for _ in range(1000):
	sim.step()
	view.render()

view.render()