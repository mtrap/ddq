'''
data processing for linear implementation
Author: Matthew Trappett
Date: April 24, 2022
'''
import plotly.graph_objs as go
import plotly.express as px
import pandas as pd
import numpy as np
import pickle


def scatter_w_error(config, mean, stdev, title, ylabel):
    fig = go.Figure([
            go.Scatter(
                y=mean,
                mode='lines',
                line=dict(color='rgb(31, 119, 180)'),
                name=ylabel,
            ),
            go.Scatter(
                y=mean+stdev,
                mode='lines',
                marker=dict(color="#444"),
                line=dict(width=0),
                showlegend=False
            ),
            go.Scatter(
                y=mean-stdev,
                marker=dict(color="#444"),
                line=dict(width=0),
                mode='lines',
                fillcolor='rgba(68, 68, 68, 0.3)',
                fill='tonexty',
                showlegend=False
            )
        ])

    fig.update_layout(
    yaxis_title=ylabel,
    xaxis_title='Episodes',
    title=title,
    hovermode="x", 
    legend=dict(
                yanchor="bottom",
                y=0.01,
                xanchor="right",
                x=0.99,
                )
    )
    return fig


def save_plot(fig, path, plotname):
    fig.write_html(path + plotname +'.html')

def save_error_plots(all_data, config, metric_str):
    data_df = pd.concat(all_data, axis=1)
    data_mean = data_df.mean(axis=1)
    data_stdev = data_df.std(axis=1)
    rep = config['repeats']
    data_fig = scatter_w_error(config, data_mean, data_stdev,f'{metric_str} averaged over {rep} repeats', f'Average {metric_str}')
    save_plot(data_fig, config['GraphPath'], f'{metric_str}wErrorLine')
    # print(data_df)
    return {f'All_{metric_str}': data_df, f'{metric_str}Mean': data_mean, f'{metric_str}Stdev': data_stdev}

# def save_hist(all_data, config, metric_str):
#     data_df = pd.concat(all_data, axis=1)
#     data_mean = data_df.mean(axis=1)
#     data_stdev = data_df.std(axis=1)
#     rep = config['repeats']
#     data_fig = px.hist()
#     save_plot(data_fig, config['GraphPath'], f'{metric_str}wErrorLine')
#     return {f'All_{metric_str}': data_df, f'{metric_str}Mean': data_mean, f'{metric_str}Stdev': data_stdev}

def add(a,b):
    return a + b

def process_results(config, results_list):
    # each element in results_list is a dict
    
    all_obs = {k:[] for k in results_list[0].keys() if 'obs' in k}
    actions = []
    all_rewards = []
    TD_errors = []
    # epsilons = []
    losses = []
    best_agent = None
    best_average_reward = -np.inf
    for d in results_list:
        all_rewards.append(d['Total Rewards'])
        actions.append(d['Actions Taken'])
        TD_errors.append(d['TDErrors'])
        # epsilons.append(d['Epsilon vals'])
        losses.append(d['loss_tracker'])
        # save agent if best average reward
        if d['Average reward'] > best_average_reward:
            best_average_reward = d['Average reward']
            best_agent = d['Self Object']
        for k1 in all_obs.keys():
            all_obs[k1].append(d[k1])

    if config['debug']:
        for k in all_obs.keys():
            save_error_plots(all_obs[k], config, k)

    reward_data = save_error_plots(all_rewards, config, 'Rewards')
    td_error_dataa = save_error_plots(TD_errors, config, 'TD Errors')
    # epsilon_data = save_error_plots(epsilons, config, 'EpsilonValues')
    loss_data = save_error_plots(losses, config, 'Losses')

    # heat map
    if not config['tune'] and config['debug']:
        hmap = px.imshow(results_list[0]['network params'], aspect="auto", 
            labels=dict(x='Episode', y="Weight", color="Connection Strength"),)
        hmap.update_layout(title='Weights Values During Training',)
        save_plot(hmap, config['GraphPath'], 'Hmap')

    # histograam of actions
    if config['debug']:
        hist_dict = {str(i):0 for i in range(max(actions[0])+1)}
        def hist_data(aarr):
            for val in aarr:
                hist_dict[str(val)]+=1

        _ = list(map(hist_data, actions))
        hist = px.histogram(x=hist_dict.keys(), y=hist_dict.values())
        hist.update_layout(bargap=0.2,  yaxis_title='Number of Times Action Taken',
                            xaxis_title='Action',
                            title='How many times agent Performed action',
                            )
        save_plot(hist, config['GraphPath'], 'Hist')
    # find e
    # save data
    best_agent.save()

    # save data for future processing if we want.
    with open(config['DataPath'] + 'rewards_DataMeanStdev.txt', 'wb') as f:
        pickle.dump(reward_data, f)

def tune_sub_process(config, data):
    # this config has altered config folder
    # data is a list with result_dicts in it
    # take the min of (200,episodes)
    # number to sum
    sum_num = min(200, config['episodes'])
    r_sum = []
    for d in data:
        slice_ = d['Total Rewards'].to_numpy()[-sum_num:]
        r_sum.append(np.mean(slice_))
    r_mean = np.mean(r_sum)
    r_std = np.std(r_sum)
    return [config, r_mean, r_std]

# def best_results(alphas, kappas, mean_rs):
#     best_index = np.argmax(mean_rs)
#     best_alpha = alphas[best_index]
#     best_kappa = kappas[best_index]
#     best_reward = mean_rs[best_index]
#     return (best_alpha, best_kappa, best_reward)

def process_tune_data(config, tune_results):
    # tune results will be a list of tuples from tune_sub_process
    # for tup in tune_results:
    #     tup[0] = tup[0][0]
    t_r = sorted(tune_results, key=lambda x: x[1], reverse=True)
    # data = np.stack(t_r)
    lr_rates = {key:[] for key in config['tune param']}
    for sub_result in t_r:
        for k in lr_rates.keys():
            lr_rates[k].append(sub_result[0][k])
    # print('lr rates: ', lr_rates)
    text_array = make_text_array(lr_rates)
    # print('text array', text_array)
    mean_rs = [a[1] for a in t_r]
    # print('Means: ', mean_rs)
    error = [a[2] for a in t_r]
    # print('Errors: ', error)
    assert len(lr_rates['alpha']) == len(text_array) == len(mean_rs) == len(error), 'Arrays for graph length mismatch'
    fig = go.Figure(data=go.Scatter(x=lr_rates['alpha'], y=mean_rs, mode="markers", 
            hovertemplate = '<b>Reward</b>: %{y}<br>'    + '<b>%{text}</b>',
            text = text_array,            
            error_y=dict(
                        type='data', # value of error bar given in data coordinates
                        array=error,
                        visible=True)))
    fig.update_xaxes(type="log")
    fig.update_layout(title=f'Tune Results for {config["env"]}',
                   xaxis_title=str(config['tune param']),
                   yaxis_title='Average returns over last 200 episodes')
    # fig.show()
    save_plot(fig, config['ParentPath'], "/Tune_results")

    return {'Mean Rewards': mean_rs, 'error': error}, t_r[0]


def make_text_array(lrs:dict):
    text_array = ["" for _ in range(len(lrs['alpha']))]
    for k in lrs.keys():
        for i in range(len(lrs[k])):
            text_array[i] += str(k) + ': ' + str(lrs[k][i]) + '<br>'
    return text_array


# def process_tune_data_k(config, tune_results):
#     # tune results will be a list of tuples from tune_sub_process
#     t_r = sorted(tune_results, key=lambda x: x[0][1])
#     kappas = []
#     for t in tune_results:
#         kappas.append(t[0][1])
#         t[0] = t[0][0]
#     data = np.stack(t_r)
#     ks = np.array(kappas)
#     alphas = data[:,0]
#     mean_rs = data[:,1]
#     error = data[:,2]
#     fig = go.Figure(data=go.Scatter(x=alphas, 
#                                     y=mean_rs, 
#                                     mode="markers", 
#                                     hovertemplate =
#                                     '<b>Reward</b>: %{y}<br>'+
#                                     '<br><b>Alpha</b>: %{x}<br>'+
#                                     '<b>%{text}</b>',
#                                     text = ['Kappa Value {}'.format(k) for k in ks],
#                                     error_y=dict(
#                                         type='data', # value of error bar given in data coordinates
#                                         array=error,
#                                         visible=True)))
#     fig.update_xaxes(type="log")
#     fig.update_layout(title=f'Tune Results for {config["env"]}',
#                    xaxis_title=str(config['tune param']),
#                    yaxis_title='Average returns over last 200 episodes')
#     # fig.show()
#     save_plot(fig, config['ParentPath'], "/Tune_results")
#     bests = best_results(alphas, ks, mean_rs)
#     return data, bests


    '''
def tune_sub_process(config, data):
    # this config has altered config folder
    # data is a list with result_dicts in it
    # take the min of 200,episodes
    # number to sum
    sum_num = min(200, config['episodes'])
    r_sum = []
    for d in data:
        slice_ = d['Total Rewards'].to_numpy()[-sum_num:]
        r_sum.append(np.mean(slice_))
    r_mean = np.mean(r_sum)
    r_std = np.std(r_sum)
    return [[config[t] for t in config['tune param']], r_mean, r_std]

def best_alpha(alphas, mean_rs):
    best_alpha = alphas[np.argmax(mean_rs)]
    return best_alpha

def process_tune_data(config, tune_results):
    # tune results will be a list of tuples from tune_sub_process
    for tup in tune_results:
        tup[0] = tup[0][0]
    t_r = sorted(tune_results, key=lambda x: x[0])
    data = np.stack(t_r)
    alphas = data[:,0]
    mean_rs = data[:,1]
    error = data[:,2]
    fig = go.Figure(data=go.Scatter(x=alphas, y=mean_rs, mode="markers", error_y=dict(
            type='data', # value of error bar given in data coordinates
            array=error,
            visible=True)))
    fig.update_xaxes(type="log")
    fig.update_layout(title=f'Tune Results for {config["env"]}',
                   xaxis_title=str(config['tune param']),
                   yaxis_title='Average returns over last 200 episodes')
    # fig.show()
    save_plot(fig, config['ParentPath'], "/Tune_results")
    best_a = best_alpha(alphas, mean_rs)

    return data, best_a


def process_tune_data_k(config, tune_results):
    # tune results will be a list of tuples from tune_sub_process
    t_r = sorted(tune_results, key=lambda x: x[0][0])
    kappas = []
    for t in tune_results:
        kappas.append(t[0][1])
        t[0] = t[0][0]
    data = np.stack(t_r)
    ks = np.array(kappas)
    alphas = data[:,0]
    mean_rs = data[:,1]
    error = data[:,2]
    fig = go.Figure(data=go.Scatter(x=alphas, 
                                    y=mean_rs, 
                                    mode="markers", 
                                    hovertemplate =
                                    '<i>Reward</i>: $%{y}'+
                                    '<br><b>Alpha</b>: %{x}<br>'+
                                    '<b>%{text}</b>',
                                    text = ['Kappa Value {}'.format(k) for k in ks],
                                    error_y=dict(
                                        type='data', # value of error bar given in data coordinates
                                        array=error,
                                        visible=True)))
    fig.update_xaxes(type="log")
    fig.update_layout(title=f'Tune Results for {config["env"]}',
                   xaxis_title=str(config['tune param']),
                   yaxis_title='Average returns over last 200 episodes')
    # fig.show()
    save_plot(fig, config['ParentPath'], "/Tune_results")
    best_a = best_alpha(alphas, mean_rs)
    best_k = best_alpha(ks, mean_rs)
    return data, (best_a, best_k)

'''


