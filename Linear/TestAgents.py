# TestAgents.py
# from TileFAAlgorithms import GreedyGQ, GDQ, QAgent, RandomAgent

import pickle
import glob
import datetime

import gym
import numpy
import torch
import os
import sys

# For recording
from pyvirtualdisplay import Display
from gym.wrappers.monitoring.video_recorder import VideoRecorder




def test_agent(folder, agent):
    display = Display(visible=False, size=(1400, 900))
    _ = display.start()

    env = gym.make('Acrobot-v1')

    videoNameString = folder + '/' + str(agent) + "AB.mp4"
    video = VideoRecorder(env, videoNameString)
    agent.epsilon = 0
    for _ in range(3):
        obs = env.reset()
        env.render()
        video.capture_frame()
        # while True:
        while True:
            # epsilon = 0 since for no exploring
            action = agent.chooseAction(obs)
            # action = env.action_space.sample()
            obs, reward, done, info = env.step(action)
            env.render()
            video.capture_frame()
            if done:
                break
    video.close()
    env.close()
    return



def main(agentPath):
    # check if parent Agents folder exists or make one
    VID_PATH = os.getcwd() + '/Videos'
    if not os.path.isdir(VID_PATH):
        os.makedirs(VID_PATH)


    # use pickle to recreate trained agents
    with open(agentPath, 'rb') as af:
        agent = pickle.load(af)

    test_agent(VID_PATH,  agent)


if __name__=='__main__':
    filename = '/Users/matthewtrappett/Downloads/LinearResults/prod_qa_Acrobot_v1_August_10_2022_11_53_13/Agents/Q_Learning.agent'
    # filename = '/Users/matthewtrappett/Downloads/LinearResults/prod_greedy_Acrobot_v1_August_08_2022_11_49_26/Agents/Greedy_GQ_0.05_0.05.agent'
    # filename = '/Users/matthewtrappett/Downloads/LinearResults/prod_gdq_Acrobot_v1_August_23_2022_11_20_23/Agents/GDQ_w_kappa_0.75.agent'
    main(filename)


