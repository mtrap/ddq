### About
Project for implementing Gradient Temporal Difference-Difference Q-learning Algorithms.
This method introduces a regularization term onto the GTD2 objective function.


### Installation
To install dependencies:     

pip install -r requirements.txt     


### Unfinished contains all the interesting stuff
The unfinished contains all the different environments:
For linear function approximation:
* Mountain car
* Cart Pole
* Acrobot

For Actor-Critic:
* MountainCar Continuous
* Pendulum