'''
Main file to drive Parallel learning for linear Greedy-GQ and DDQ.
'''

import sys
import ray
import gym
import numpy as np
import random
import utils.TileCoderSutton as TC
import torch
import logging
import matplotlib.pyplot as plt
import socket
from utils.sendmail import sendmail
from scipy.signal import savgol_filter
from collections import deque
import time



@ray.remote
class learner(object):
    def __init__(self, config, memory, param_server):
        # shared global objects
        self.parameter_server = param_server
        self.memory = memory

        # create actors
        self.actors = [actor.remote(config,self.memory, self.parameter_server) for _ in range(config['numActors'])]

        # hyperparameters
        self.gamma = .99
        self.alpha = config['alpha']
        self.beta = config['beta']

        # how many updates to perform
        self.numUpdates = config['n_updates']

        
        # N time update steps to update slow weights
        self.n = 0 # counter
        self.N = config['slow_update'] # update slow weights after N steps
        self.batch_size = config['mem_batch']
        self.numActions = config['numActions']
        
        self.target_w = ray.get(self.parameter_server.get_weights.remote())
        self.w = ray.get(self.parameter_server.get_weights.remote())
        self.target_eta = ray.get(self.parameter_server.get_etas.remote())
        self.old_eta = ray.get(self.parameter_server.get_etas.remote())

        # Scale the inputs for feature representation
        # self.obs_low = [-1.2,  -0.07] # low for mountainCar
        # self.obs_high =  [0.6,  0.07] # high for mountain car
        # cart pole
        self.obs_high = [4.8000002e+00, 3.4028235e+38, 4.1887903e-01, 3.4028235e+38]
        self.obs_low = [-4.8000002e+00, -3.4028235e+38, -4.1887903e-01, -3.4028235e+38]

        # tile Coding constants
        self.numTilings = 10
        self.iht_num = 10**6
        self.iht = TC.IHT(self.iht_num)
        
        # Trace values
        self.lam = 0
        self.trace = torch.zeros(len(self.target_w))
        logging.basicConfig(level=logging.INFO)
        
    def update(self):
        '''
        This function grabs some memories from the memory replay distributed
        object. The learner uses these objects to compute the dw to update weights.
        '''
        # # get weights from parameter server
        # if self.n % self.N == 0:
        #     self.target_w = ray.get(self.parameter_server.get_weights.remote())
        # get the target weights everytime
        self.w = ray.get(self.parameter_server.get_weights.remote())
        self.eta = ray.get(self.parameter_server.get_etas.remote())
        self.n += 1
        
        # repeat n times, compute average dw and then send to parameter vector.
        d_etas = torch.zeros((self.batch_size, len(self.target_w)))
        d_weights = torch.zeros((self.batch_size, len(self.target_w)))
        
        # # batch of memories; returns self.numMemories samples
        # memories = ray.get(self.memory.get_memories.remote())
        # print(f'Memories from learner: {memories}')
        
        for i in range(self.batch_size):
            # get a memory
            memory = ray.get(self.memory.get_memories.remote())

            # the memory won't be filling up at the beginning
            if memory == 0:
                d_etas[i,:] = torch.zeros(len(self.target_w))
                d_weights[i,:] = torch.zeros(len(self.target_w))
                continue

            # get a trajectory from memory
            (s, a, s_next, r), done, eps = memory

            # get next best action
            bestAction = self.bestAction(s_next)

            # get state representations 
            features = self.getFeatures(s, a)
            features_next = self.getFeatures(s_next, bestAction)

            # Importance Sampling Ratio
            rho = 1/(1-eps) if a == self.bestAction(s) else 0

            # Trace 
            current_trace = rho * (features + self.gamma * self.lam * self.trace)
            # skip importance sampling
            current_trace = features

            # Check for end of episode
            if done:
                t = r
            else:
                t = r + self.gamma * torch.matmul(features_next, self.w)
            
            # TD error
            temp_diff = t - torch.matmul(features, self.w)
            # print(f'td error: {temp_diff}')

            # simple q update
            d_weights[i,:] = self.alpha * temp_diff * current_trace

        # send updated weights to parameter_server
        d_w_average = torch.mean(d_weights, axis=0)


        # print('Weights should be 10**6 long')
        # norm of the weights should converge to steady state
        # print(f'd_w_average: {torch.sum(d_w_average)}')
        # print(f'eta average: {d_eta_average}, W averge: {d_w_average}')
        self.parameter_server.recieve.remote(d_w_average)
    
    def getFeatures(self, state, action):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/(self.obs_high[i] - self.obs_low[i]) for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled, [action])
        zeros = torch.zeros(self.iht_num)
        zeros[indices] = 1
        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float())) 
              
            
    def bestAction(self, state):
        # Get action value array
        actions = [torch.dot(self.target_w, self.getFeatures(state, act)) for act in range(self.numActions)]
        # The max of action value is the best action
        action = torch.argmax(torch.tensor(actions)).item()
        return action

    def learn(self):
        # start actor
        obj_refs = [act.generate_experience.remote() for act in self.actors]
        
        while not ray.get(self.memory.batchable.remote()):
            time.sleep(1)

        print(f'Beginning {self} updating...')
        for _ in range(self.numUpdates):
            self.update()

        # stop generating xp
        for act in self.actors:
            ray.kill(act)

        print('Finished Learning')
        return ray.get(self.memory.get_stats.remote())



@ray.remote
class actor(object):
    def __init__(self, config, memory, param_server):
        # distributed objects
        self.exp_mem = memory
        self.param_server = param_server
        self.config = config
        
        # learner
        # create a number of learners
        # self.learners = [learner.remote(config) for _ in range(config['numLearners'])]

        # hyperparameters
        self.epsilon = config['start epsilon']
        
        self.env_string = config['env']
        self.n_episodes = config['episodes']
        self.w = ray.get(self.param_server.get_weights.remote())
        self.numActions = config['numActions']

        # Scale the inputs for feature representation
        # self.obs_low = [-1.2,  -0.07] # low for mountainCar
        # self.obs_high =  [0.6,  0.07] # high for mountain car
        # cart pole
        self.obs_high = [4.8000002e+00, 3.4028235e+38, 4.1887903e-01, 3.4028235e+38]
        self.obs_low = [-4.8000002e+00, -3.4028235e+38, -4.1887903e-01, -3.4028235e+38]

        # tile Coding constants
        self.numTilings = 10
        self.iht_num = 10**6
        self.iht = TC.IHT(self.iht_num)
        logging.basicConfig(level=logging.INFO)

        # Stats
        self.rewards = np.zeros(self.n_episodes)
        self.steps = np.zeros(self.n_episodes)

        
    def getFeatures(self, state, action):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/(self.obs_high[i] - self.obs_low[i]) for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled, [action])
        zeros = torch.zeros(self.iht_num)
        zeros[indices] = 1
        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float()))  


    def chooseAction(self, state):
        # epsilon Q action policy.
        # if less than epsilon than take random action
        if random.random() < self.epsilon:
            return random.randrange(self.numActions)
        else:
            return self.bestAction(state)
        
    def bestAction(self, state):
        # Get action value array
        # sync the weight vector here
        self.w = ray.get(self.param_server.get_weights.remote()) # best place
        actions = [torch.dot(self.w, self.getFeatures(state, a)) for a in range(self.numActions)]
        # The max of action value is the best action
        action = torch.argmax(torch.tensor(actions)).item()
        return action

            
    def generate_experience(self):
        print(f'Generating xp: agent{self}')
        env = gym.make(self.env_string)
        self.numActions = env.action_space.n

        # keep running episodes until killed
        while True:
            # self.exp_mem.inspect_mem.remote()
            # print("Weights: ", self.w)
            tot_rew = 0
            obs = env.reset()
            while True:
                action = self.chooseAction(obs)
                new_obs, reward, done, info = env.step(action)
                trajectory = (obs, action, new_obs, reward)
                self.exp_mem.store_trajectory.remote((trajectory, done, self.epsilon))
                tot_rew += reward
                obs = new_obs
                # print(tot_rew)
                if done:
                    break
            if self.epsilon > 0.05:
                self.epsilon -= (2/self.config['episodes'])
            # print(f'Tot reward: {tot_rew}')
            ray.get(self.exp_mem.save_data.remote(tot_rew))


            

@ray.remote
class exp_replay(object):
    def __init__(self, config):
        # fill up sequentially and draw from it randomly
        # self.total_length = 10000 # how many samples to store
        self.memory =  deque([], maxlen=config['numMemories'])
        self.mem_batch_size = config['mem_batch']
        logging.basicConfig(level=logging.INFO)
        self.counter = 0

        # data from learner
        self.rewards = []
        self.steps = []

        
    def store_trajectory(self, trajectory):
        self.memory.append(trajectory)
        # self.counter += 1

        
    def get_memories(self):
        return random.choice(self.memory)
    
    def inspect_mem(self):
        print([self.memory[i] for i in range(50)])
        return 

    def save_data(self, tot_rewards):
        self.rewards.append(tot_rewards)
        return

    def get_stats(self):
        return ({'rewards':self.rewards})

    def batchable(self):
        return len(self.memory) > self.mem_batch_size


@ray.remote
class parameter_server(object):
    def __init__(self, config):
        self.weight_norms = []
        # initialize weights
        self.target_weights = torch.rand(10**6 + 1)
        self.target_etas = torch.rand(10**6 + 1)
        logging.basicConfig(level=logging.INFO)
    
    def get_weights(self):
        return self.target_weights.clone().detach()
    
    def get_etas(self):
        return self.target_etas.clone().detach()
        
    def recieve(self, d_weight_mean):
        # print(f'Old weights sum: {torch.sum(self.target_weights)}')
        self.target_weights = self.target_weights + d_weight_mean
        self.weight_norms.append(torch.mean(self.target_weights))
        # print(f'New weights sum: {torch.sum(self.target_weights)}')

    def get_weight_norms(self):
        return self.weight_norms        


def main(episodes, nlearn, nact):

    config = {'episodes': episodes,
              'env': 'CartPole-v0',
              'numActions': 2, # 2 discrete actions for cartpole
              'start epsilon': .99999,
              'mem_batch': 32, 
              'numMemories': 256,
              'numActors': nact,  # num learners per actor 
              'alpha': 0.001,
              'beta': 0.001,
              'slow_update': 10,
              'n_updates': episodes, # episodes now means how many updates
              }

    memory = exp_replay.remote(config)
    param_server = parameter_server.remote(config)

    numLearners = nlearn
    learners = [learner.remote(config, memory, param_server) for _ in range(numLearners)]
    learn_ref = [learner.learn.remote() for learner in learners]
    finished = ray.get(learn_ref)
    rewards = []
    for i in finished:
        rewards.append(i['rewards'])

    weight_norms = ray.get(param_server.get_weight_norms.remote())

    fig, axs = plt.subplots(2, 1)
    for a in range(numLearners):
        axs[0].plot(np.arange(len(rewards[a])), rewards[a])
    axs[0].set_xlabel('Episode')
    axs[0].set_ylabel('Reward')
    axs[0].set_title('Rewards of actors')
    axs[0].legend([f'Learner {a}' for a in range(numLearners)])


    axs[1].plot(weight_norms)
    axs[1].set_xlabel('Num of Param Server Updates')
    axs[1].set_ylabel('Average Weight Value')
    axs[1].set_title(f'Ave weight value during training')

    fig.tight_layout()
    # plt.show()
    plt.savefig('results1.pdf')
    print('Finished')
    if config['episodes'] > 400:
        sendmail('mtrappett@icloud.com', config['env']+ ' Parallel has finished','All done on ' + socket.gethostname() + 
                                                            '\n' + str(config))

if __name__=='__main__':
    eps = 10
    nLearn = 1
    nActor = 1
    if len(sys.argv) == 2:
        # how many episodes
        eps = int(sys.argv[1])
    elif len(sys.argv) == 3:
        # episodes learners
        eps = int(sys.argv[1])
        nLearn = int(sys.argv[2])
    elif len(sys.argv) == 4:
        # how many updates, learners, acts
        eps = int(sys.argv[1])
        nLearn = int(sys.argv[2])
        nActor = int(sys.argv[3])

    cpus = 2 + nLearn*nActor + nLearn  # param server, mem + numActors * numLearners + numActors
    ray.init(num_cpus=cpus)

    main(eps, nLearn, nActor)

    ray.shutdown()
    