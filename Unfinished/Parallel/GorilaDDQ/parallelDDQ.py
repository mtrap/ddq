'''
Main file to drive Parallel learning for linear Greedy-GQ and DDQ.
'''

import sys
import ray
import gym
import numpy as np
import random
import TileCoderSutton as TC
import torch
import logging
import matplotlib.pyplot as plt
from tqdm import tqdm



@ray.remote
class learner(object):
    def __init__(self, config):
        # shared global objects
        self.parameter_server = config['param_server']
        self.memory = config['memory']
        
        # hyperparameters
        self.gamma = .99
        self.alpha = config['alpha']/11 # originally .1/11
        self.beta = .0001/11  # originally .0001/11
        self.epsilon = .1

        
        # N time update steps to update slow weights
        self.n = 0 # incrementer
        self.N = 10 # update slow weights after N steps
        self.numMemories = config['numMemories']
        self.numActions = config['numActions']
        
        self.target_w = ray.get(self.parameter_server.get_weights.remote())
        self.w = ray.get(self.parameter_server.get_weights.remote())
        self.target_eta = ray.get(self.parameter_server.get_etas.remote())
        self.old_eta = ray.get(self.parameter_server.get_etas.remote())

        # Scale the inputs for feature representation
        # self.obs_low = [-1.2,  -0.07] # low for mountainCar
        # self.obs_high =  [0.6,  0.07] # high for mountain car
        # cart pole
        self.obs_high = [4.8000002e+00, 3.4028235e+38, 4.1887903e-01, 3.4028235e+38]
        self.obs_low = [-4.8000002e+00, -3.4028235e+38, -4.1887903e-01, -3.4028235e+38]

        # tile Coding constants
        self.numTilings = 10
        self.iht_num = 10**6
        self.iht = TC.IHT(self.iht_num)
        
        # Trace values
        self.lam = 0
        self.trace = torch.zeros(len(self.target_w))
        logging.basicConfig(level=logging.INFO)
        
    def update(self):
        '''
        This function grabs some memories from the memory replay distributed
        object. The learner uses these objects to compute the dw to update weights.
        '''
        # get weights from parameter server
        if self.n % self.N == 0:
            self.target_w = ray.get(self.parameter_server.get_weights.remote())
        # get the target weights everytime
        self.w = ray.get(self.parameter_server.get_weights.remote())
        self.eta = ray.get(self.parameter_server.get_etas.remote())
        self.n += 1
        
        # repeat n times, compute average dw and then send to parameter vector.
        d_etas = torch.zeros((self.numMemories, len(self.target_w)))
        d_weights = torch.zeros((self.numMemories, len(self.target_w)))
        
        # # batch of memories; returns self.numMemories samples
        # memories = ray.get(self.memory.get_memories.remote())
        # print(f'Memories from learner: {memories}')
        # check if decreasing alpha helps learning.
        alpha_ = -1/100000 * self.n + self.alpha
        new_alpha = self.alpha #alpha_ if alpha_ > 0.0000001 else 0.0000001
        
        for i in range(self.numMemories):
            # get a memory
            memory = ray.get(self.memory.get_memories.remote())

            # the memory won't be filling up at the beginning
            if memory == 0:
                d_etas[i,:] = torch.zeros(len(self.target_w))
                d_weights[i,:] = torch.zeros(len(self.target_w))
                continue

            # get a trajectory from memory
            (s, a, s_next, r), done = memory

            # get next best action
            bestAction = self.bestAction(s_next)

            # get state representations 
            features = self.getFeatures(s, a)
            features_next = self.getFeatures(s_next, bestAction)

            # Importance Sampling Ratio
            rho = 1/(1-self.epsilon) if a == self.bestAction(s) else 0

            # Trace 
            current_trace = rho * (features + self.gamma * self.lam * self.trace)

            # Check for end of episode
            if done:
                t = r
            else:
                t = r + self.gamma * torch.matmul(features_next, self.target_w)
            
            # TD error
            temp_diff = t - torch.matmul(features, self.w)
            # print(f'td error: {temp_diff}')

            # x * eta
            xdoteta = torch.matmul(current_trace, self.eta)

            # secondary weights update
            d_etas[i,:] = self.beta * (temp_diff * current_trace - xdoteta * current_trace)    

            # Primary Weights update
            d_weights[i,:] = new_alpha * (temp_diff * current_trace - self.gamma * (1 - self.lam) * xdoteta * features_next)
    
        # send updated weights to parameter_server
        d_eta_average = torch.mean(d_etas, axis=0)
        d_w_average = torch.mean(d_weights, axis=0)
        # print('Weights should be 10**6 long')
        # print(f'd_w_average: {torch.sum(d_w_average)}')
        # print(f'eta average: {d_eta_average}, W averge: {d_w_average}')
        self.parameter_server.recieve.remote(d_w_average, d_eta_average)
    
    def getFeatures(self, state, action):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/(self.obs_high[i] - self.obs_low[i]) for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled, [action])
        zeros = torch.zeros(self.iht_num)
        zeros[indices] = 1
        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float())) 
              
            
    def bestAction(self, state):
        # Get action value array
        actions = [torch.dot(self.target_w, self.getFeatures(state, act)) for act in range(self.numActions)]
        # The max of action value is the best action
        action = torch.argmax(torch.tensor(actions)).item()
        return action

    def learn(self, actor):
        while True:
            self.update()



@ray.remote
class actor(object):
    def __init__(self, config):
        # distributed objects
        self.exp_mem = config['memory']
        self.param_server = config['param_server']
        
        # learner
        # create a number of learners
        self.learners = [learner.remote(config) for _ in range(config['numLearners'])]

        # hyperparameters
        self.epsilon = .1
        
        self.env_string = config['env']
        self.n_episodes = config['episodes']
        self.w = ray.get(self.param_server.get_weights.remote())
        self.numActions = config['numActions']

        # Scale the inputs for feature representation
        # self.obs_low = [-1.2,  -0.07] # low for mountainCar
        # self.obs_high =  [0.6,  0.07] # high for mountain car
        # cart pole
        self.obs_high = [4.8000002e+00, 3.4028235e+38, 4.1887903e-01, 3.4028235e+38]
        self.obs_low = [-4.8000002e+00, -3.4028235e+38, -4.1887903e-01, -3.4028235e+38]

        # tile Coding constants
        self.numTilings = 10
        self.iht_num = 10**6
        self.iht = TC.IHT(self.iht_num)
        logging.basicConfig(level=logging.INFO)

        # Stats
        self.rewards = np.zeros(self.n_episodes)
        self.steps = np.zeros(self.n_episodes)

        
    def getFeatures(self, state, action):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/(self.obs_high[i] - self.obs_low[i]) for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled, [action])
        zeros = torch.zeros(self.iht_num)
        zeros[indices] = 1
        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float()))  


    def chooseAction(self, state):
        # epsilon Q action policy.
        # if less than epsilon than take random action
        if random.random() < self.epsilon:
            return random.randrange(self.numActions)
        else:
            return self.bestAction(state)
        
    def bestAction(self, state):
        # Get action value array
        # sync the weight vector here
        self.w = ray.get(self.param_server.get_weights.remote())
        actions = [torch.dot(self.w, self.getFeatures(state, a)) for a in range(self.numActions)]
        # The max of action value is the best action
        action = torch.argmax(torch.tensor(actions)).item()
        return action
            
    def generate_experience(self):
        print(f'Generating xp: agent{self}')
        env = gym.make(self.env_string)
        self.numActions = env.action_space.n
        # start Learners
        for learner in self.learners:
            learner.learn.remote(self)

        for episode in range(self.n_episodes):
            # self.exp_mem.inspect_mem.remote()
            # print("Weights: ", self.w)
            tot_steps = 0
            tot_rew = 0
            obs = env.reset()
            # new_epsilon_ = -1/10**5 * episode + self.epsilon
            # new_epsilon = 0 if new_epsilon_ < 0 else new_epsilon_
            # turn off epsilon after a some time to test
            if episode == 700:
                self.epsilon = 0

            while True:
                action = self.chooseAction(obs)
                new_obs, reward, done, info = env.step(action)
                trajectory = (obs, action, new_obs, reward)
                self.exp_mem.store_trajectory.remote((trajectory, done))
                tot_rew += reward
                tot_steps += 1
                obs = new_obs
                # print(tot_rew)
                if done:
                    break
            # print(f'Tot reward: {tot_rew}')
            self.rewards[episode] = tot_rew
            self.steps[episode] = tot_steps
        # when episodes are done, stop learner
        for learner in self.learners:
            ray.kill(learner)
        return {'rewards':self.rewards, 'steps':self.steps}
            

@ray.remote
class exp_replay(object):
    def __init__(self):
        # create a numpy array of 1000 x 4, num of experiences x len of experiences.
        # fill up randomly and draw from it randomly
        self.total_length = 10000 # how many samples to store
        self.memory =  [0 for i in range(self.total_length)]
        # self.mem_batch_size = 2
        logging.basicConfig(level=logging.INFO)
        self.counter = 0
        
    def store_trajectory(self, trajectory):
        self.memory[self.counter % self.total_length] = trajectory
        self.counter += 1

        
    def get_memories(self):
        return random.choice(self.memory)
    
    def inspect_mem(self):
        print([self.memory[i] for i in range(50)])
        return 

@ray.remote
class parameter_server(object):
    def __init__(self):
        # initialize weights
        self.target_weights = torch.rand(10**6 + 1)
        self.target_etas = torch.rand(10**6 + 1)
        logging.basicConfig(level=logging.INFO)
    
    def get_weights(self):
        return self.target_weights.clone().detach()
    
    def get_etas(self):
        return self.target_etas.clone().detach()
        
    def recieve(self, d_weight_mean, d_eta_mean):
        # print(f'Old weights sum: {torch.sum(self.target_weights)}')
        self.target_weights += d_weight_mean
        self.target_etas += d_eta_mean
        # print(f'New weights sum: {torch.sum(self.target_weights)}')
        


class parallelDDQ():
    def __init__(self, config):
        self.memory = exp_replay.remote()
        self.param_server = parameter_server.remote()
        self.config = config
    # config = {'episodes': episodes,
    #           'env': 'CartPole-v0',
    #           'memory': memory,
    #           'param_server': param_server,
    #           'numActions': 3, # 3 discrete actions for cartpole
    #            'epsilon': .1,  
    #           'numMemories': 16,
    #           'numLearners': 1,  # num learners per actor 
    #           'alpha': 0.1,
    #           }
        self.numActors = config['numActors']


    def train(self):
        actors = [actor.remote(self.config) for _ in range(self.numActors)]
        act_ref = [actor.generate_experience.remote() for actor in actors]
        finished = ray.get(act_ref)
        # get the results from training, each finished will have a dictionary
        steps = []
        rewards = []
        for i in finished:
            rewards.append(i['rewards'])
            steps.append(i['steps'])

        # rewards and steps are nest lists depending on number of actors
        return rewards, steps

    def save_plot(self):
        # need to generalize this to more copies of actors
        fig, axs = plt.subplots(2, 1)
        for a in range(numActors):
            axs[0].plot(np.arange(len(rewards[a])), rewards[a])
            axs[0].plot(np.arange(len(rewards[a])), rewards[a])
        axs[0].set_xlabel('Episode')
        axs[0].set_ylabel('Reward')
        axs[0].set_title('Rewards of actors')
        axs[0].legend([f'Actor {a}' for a in range(numActors)])

        # steps
        for a in range(numActors):
            axs[1].plot(np.arange(len(steps[a])), steps[a])
            axs[1].plot(np.arange(len(steps[a])), steps[a])
        axs[1].set_xlabel('Episode')
        axs[1].set_ylabel('Num of steps')
        axs[1].set_title('Number of steps in an episode of actors')
        axs[1].legend([f'Actor {a}' for a in range(numActors)])

        fig.tight_layout()
        # plt.show()
        plt.savefig('results.pdf')
        print('Finished')


