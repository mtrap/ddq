'''
Main script for parallel Q learning
Parameter server initializes all the actors and learners
It will then has a function to start learning
Main.py is where we can specify which learner we want with learnerClass
'''

from Agents.ParallelQN import QLearner
from Agents.ParameterServer import ParameterServer as PS
from utils.sendmail import sendmail
from utils.DataProcessing import process_results

import numpy as np
import concurrent.futures
import gym
import torch
from datetime import datetime
import plotly.graph_objs as go
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from tqdm import tqdm
import ray
from scipy.signal import savgol_filter


# @ray.remote
# def trainWrapper(args):
#     config = args[0]
#     AGENT = args[1]
#     agent = AGENT(config)
#     print(f'Training agent: {agent}')
#     return agent.train()


def main(agent_str, env, numEpisodes, repeats, cpus, seed):
    '''
    folder needs to have the agent+env+datetime
    '''
    numEpisodes = int(numEpisodes)
    repeats = int(repeats)
    cpus = int(cpus)
    # torch.manual_seed(1423)

    print(f'Training with \n \
            Agent: {agent_str} \n \
            env: {env} \n \
            nEpisodes: {nEpisodes} \n \
            repeats: {repeats} \n \
            cpus: {cpus}')

    # agents dict
    agent_dict = {'q': QLearner,} #'Greedy_GQ': GreedyGQ, 'GTDD':GTDD}
    AGENT = agent_dict[agent_str]

    # set up paths for saving graphs and agent
    # agent + env + DT
    date_string = '{:%B_%d_%Y_%H_%M_%S}'.format(datetime.now())
    env_string = env.replace('-', '_')
    FolderName = '_'.join([agent_str, env_string, date_string])

    # check if Results folder exits and create one if not
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)

    PARENT = ROOT_PATH + FolderName
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'  
    DATA_STR   = 'Data/' 
    GRAPHSPATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTSPATH = os.path.join(PARENT, AGENTS_STR)
    DATAPATH = os.path.join(PARENT, DATA_STR)
    os.mkdir(GRAPHSPATH)
    os.mkdir(AGENTSPATH)
    os.mkdir(DATAPATH)




    # master config dictionary
    config = {# for all agents
              'env':        env,
              'episodes':   int(numEpisodes),
              'repeats':    int(repeats),
              'cpus':       int(cpus),
              # for different initial conditions
              # for cp and mountain car
              'alpha':      1e-3,
              'h_size':     100,
              # 'h2_size':  10,
              'gamma':      0.95,
              'epsilon':    1,
              'debug':      True, # saves lots of graphs to print out.
              'tune':       False,

              # dqn
              'c':          15, # zero means that a copy is made every time
              "force":      0.001, # originally 0.001 for discrete for MC
              'batch_size': 32,
              'maxmem':     256,
              # learner class
              'learnerClass': AGENT,

              # for Greedy-GQ/GTDD
              'beta':       0.00001,
              'kappa':      0.75,

              # paths
              'AgentPath': AGENTSPATH,
              'GraphPath': GRAPHSPATH,
              'DataPath' : DATAPATH,
                }

    # start ray
    ray.init(num_cpus=cpus)
    # Initialize Paramteter server
    # This initializes Actors and Learners
    ParaServ = PS(Config)
    # start learning
    data = PS.StartLearning()

    # no repeats for now
    #######################################################
    # # for a number of repeats train the agent on the env
    # result_ids_train = [trainWrapper.remote((config, AGENT)) for _ in range(config['repeats'])]
    # # as the agent finishes then 
    # data = []
    # while len(result_ids_train):
    #     done_ids_train, result_ids_train = ray.wait(result_ids_train)
    #     data += ray.get(done_ids_train)
    #######################################################
    # stop ray
    ray.shutdown()

    print('Finished Training...\nProcessing Data...')
    # process results, make graphs and save them and best agent
    process_results(config, data)



    # save config to know our parameters
    with open(GRAPHSPATH + '/params.txt', 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(GRAPHSPATH + '/params.txt', 'w') )
        prnt.pprint(config)


    # send email to signal done
    sendmail('mtrappett@icloud.com',  env + ' Nonlinear has finished',
        'All done on ' + socket.gethostname() + 
        '\n\nRun started on ' + 
        date_string + '\n\nCommandline script to download\n\nscp -r MTrappett@orthus.nic.uoregon.edu:'+PARENT+
         ' ~/Downloads/NonLinearResults')



if __name__=='__main__':
    assert len(sys.argv) >= 4, 'Need arguments: agent, env, numEpisodes'
    agent = sys.argv[1]
    env_num = sys.argv[2]
    nEpisodes = sys.argv[3]

    env_dict = {'mc':'MountainCar-v0' ,'cp':'CartPole-v0' ,'ab':'Acrobot-v1' }
    # optional repeat paramter, for production graph will want a lot.
    repeats = 1
    cpus = 4
    seed = 1423
    if len(sys.argv) > 4:
        repeats = sys.argv[4]
    if len(sys.argv) == 6:
        cpus = sys.argv[5]
    if len(sys.argv) == 7:
        seed = sys.argv[6]

    main(agent, env_dict[env_num], nEpisodes, repeats, cpus, seed)

    print('Finished')

















