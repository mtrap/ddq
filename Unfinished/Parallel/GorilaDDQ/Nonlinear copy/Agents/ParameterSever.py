'''
Global Memory 
Parameter server 
for Linear Gorila Q
Author: Matthew Trappett
Date: May 31, 2022
initializes replay memory, actors and learners.
'''

import ray
from collections import deque
from Agents.Actor import Actor as actor
from Agents.ReplayMemory import ReplayMemory
import random




@ray.remote
class ParameterServer(object):
    def __init__(self, config):
        self.config = config
        self.memory = ReplayMemory(config)
        self.weight_norms = []
        # initialize weights
        self.target_weights = torch.rand(10**6 + 1)
        self.target_etas = torch.rand(10**6 + 1)
        logging.basicConfig(level=logging.INFO)

        #save actors and learns
        learner = config['learnerClass']
        self.actors = [actor.remote(config, self.memory, self) for _ in range(config['numActors'])]
        self.learners = [learner.remote(config, self.memory, self) for _ in range(config['numLearners'])]
    
    def get_weights(self):
        return self.target_weights.clone().detach()
    
    def get_etas(self):
        return self.target_etas.clone().detach()
        
    def recieve(self, d_weight_mean):
        # print(f'Old weights sum: {torch.sum(self.target_weights)}')
        self.target_weights = self.target_weights + d_weight_mean
        self.weight_norms.append(torch.mean(self.target_weights))
        # print(f'New weights sum: {torch.sum(self.target_weights)}')

    def get_weight_norms(self):
        return self.weight_norms 

    def startLearning(self):
        # start actor
        obj_refs = [act.generate_experience.remote() for act in self.actors]
        
        while not ray.get(self.memory.batchable.remote()):
            time.sleep(1)

        print(f'Beginning {self} updating...')
        # todo: 
        # start each actor updating

        # stop generating xp
        for act in self.actors:
            ray.kill(act)

        print('Finished Learning')
        return ray.get(self.memory.get_stats.remote())