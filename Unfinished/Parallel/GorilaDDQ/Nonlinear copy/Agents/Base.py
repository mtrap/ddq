'''
Base for Parallel Q Learning
Author: Matthew Trappett
Date: May 31, 2022

Base tracks perofmance metrics
Variables:
memory
param server
hyperparameters
Methods:
bestaction
train
choose action
save agent
return data
'''

import numpy as np
import gym
import torch.nn as nn
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from copy import deepcopy
import ray


class Base():
    def __init__(self, config):
        torch.manual_seed(0)
        # shared global objects
        self.parameter_server = param_server
        self.memory = memory
        self.config = config

        # start env for set up
        env = gym.make(config['env'])
        # constants
        self.size_obs =   len(env.observation_space.sample())
        self.numActions = env.action_space.n
        self.gamma =      torch.tensor(config['gamma']).float()
        self.alpha =      config['alpha']
        self.beta =       config['beta']
        self.env =        config['env']
        self.epsilon =    config['epsilon']

        # weight update variables
        self.numUpdates = config['numUpdates']
        # N time update steps to update slow weights
        self.n = 0 # counter
        self.N = config['slow_update'] # update slow weights after N steps
        self.batch_size = config['mem_batch']

        # Scale the inputs for NN input
        obs_low =     np.asarray(env.observation_space.low).astype(float)
        obs_high =    np.asarray(env.observation_space.high).astype(float)
        self.spread = obs_high - obs_low

        # closing env
        env.close()


        # agent performance tracking
        self.total_rewards =   np.zeros(self.config['episodes'])
        self.observations =    []
        self.action_tracker =  []
        self.TDError_tracker = []
        self.epsilon_tracker = []
        self.loss_tracker =    []
        self.debug =           config['debug']

        #save weights if debug
        if self.debug:
            num_weights = sum(p.numel() for p in self.policy.parameters() if p.requires_grad)
            self.weight_history = np.zeros((num_weights, self.config['episodes']))


        # save last action to save a computational step
        self.lastBestAction = random.randrange(self.numActions)


    def ActionValues(self, obs):
        # scale state values
        scaled = [obs[i]/self.spread[i] for i in range(len(obs))]
        scaled_obs = torch.tensor(scaled,
                               requires_grad=True, dtype=torch.float32)
        return self.policy(scaled_obs)


    def chooseAction(self, obs):
        # epsilon Q action policy.
        # if less than epsilon than take random action
        if random.random() < self.epsilon:
            return torch.randint(0,self.numActions,(1,)).item()
        else:
            with torch.no_grad():
                # act_values = self.ActionValues(obs)
                act_values = self.policy(torch.from_numpy(obs).float())
            best = torch.argmax(act_values).item()
            return best


    def train(self):
        env = gym.make(self.config['env'])
        # env.force = self.config['force'] # can be altered for MC
        for episode in range(self.config["episodes"]):
            # resets for new episode
            obs = env.reset()
            rewards = 0

            while True:
                # take step and get new obs
                action = self.chooseAction(obs)
                next_obs, reward, done, info = env.step(action)

                # Udate weights 
                self.update((torch.from_numpy(obs), action, reward, torch.from_numpy(next_obs), done), episode)

                # increment trackers
                rewards += reward
                self.action_tracker.append(action)
                self.observations.append([obs[i]/self.spread[i] for i in range(len(obs))])

                # update obs
                obs = next_obs

                if  done:  
                    break

            if self.epsilon > 0.05:
                self.epsilon -= (2/self.config['episodes'])

            # Save Trackers
            self.epsilon_tracker.append(self.epsilon)
            self.total_rewards[episode] = rewards
            if self.debug:
                self.weight_history[:,episode] = torch.nn.utils.parameters_to_vector(self.policy.parameters()).detach()

        env.close()
        # print(f'Total updates: {self.tot_updates}')
        return self.return_data()


    def save(self):
        #Export trained agent to a file to later get for testing.
        agent_file = '/' + str(self) + '.agent'
        with open(self.config['AgentPath'] + agent_file, 'wb') as af:
            # save agent state
            pickle.dump(self, af)


    def return_data(self):
        # process the observations to turn into a pd DF for graphing.
        # return a list of pd.Series for each obs. 
        # len(obs) = # of obs
        data_dict = dict()
        stack_obs = np.stack(self.observations)
        # obs is scaled
        for ob in range(self.size_obs):
            data_dict[f'obs[{ob}]'] = pd.Series(stack_obs[:,ob])

        data_dict['Total Rewards'] =  pd.Series(self.total_rewards)
        data_dict['Actions Taken'] =  pd.Series(self.action_tracker)
        data_dict['Average reward'] = np.mean(self.total_rewards)
        data_dict['network params'] = self.weight_history
        data_dict['TDErrors'] =       pd.Series(self.TDError_tracker)
        data_dict['Self Object'] =    self
        data_dict['Epsilon vals'] =   pd.Series(self.epsilon_tracker)
        data_dict['loss_tracker'] =   pd.Series(self.loss_tracker)

        return data_dict

    def update(self, *args):
        pass















