'''
Actor for Gorila Q learning
author: Matthew Trappett
Date: May 31, 2022
'''

import ray
from Agents.Base import Base

@ray.remote
class Actor(Base):
	def __init__(self,config):
		super().__init__(config)


    def generateExperience(self):
        while True:
            self.train()