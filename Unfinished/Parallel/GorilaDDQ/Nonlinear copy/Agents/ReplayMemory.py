'''
Global Memory 
Parameter server 
for Linear Gorila Q
Author: Matthew Trappett
Date: May 31, 2022
'''

import ray
from collections import deque
from ParaallelQN import QLearner
import random



@ray.remote
class ReplayMemory(object):
    def __init__(self, config):
        # fill up sequentially and draw from it randomly
        # self.total_length = 10000 # how many samples to store
        self._memory =  deque([], maxlen=config['maxmem'])
        self.batch_size = config['batch_size']
        logging.basicConfig(level=logging.INFO)

        # data from learner
        self.rewards = []
        self.steps = []

        
    def store_trajectory(self, trajectory):
        self._memory.append(trajectory)
        # self.counter += 1

        
    def get_memories(self):
        return random.choice(self._memory)
    
    def inspect_mem(self):
        print([self._memory[i] for i in range(50)])
        return 

    def save_data(self, tot_rewards):
        self.rewards.append(tot_rewards)
        return

    def get_stats(self):
        return ({'rewards':self.rewards})

    def batchable(self):
        return len(self._memory) > self.batch_size


'''
class memory():
    def __init__(self, maxlength, batch_size=1):
        self.memories = deque([], maxlen=maxlength)
        self.batch_size = batch_size

    def push(self, trajectory):
        self.memories.append(trajectory)

    def sample(self):
        size = self.batch_size
        if not self.batchable:
            size = len(self.memories)
        samples = random.sample(self.memories, size)
        obs = torch.stack([a[0] for a in samples])
        actions = torch.tensor([a[1] for a in samples]).long()
        rewards = torch.tensor([a[2] for a in samples]).float()
        next_obs = torch.stack([a[3] for a in samples])
        dones = torch.tensor([a[4] for a in samples]).float()
        return obs, actions, rewards, next_obs, dones


    def batchable(self):
        return len(self.memories) > self.batch_size
        '''




