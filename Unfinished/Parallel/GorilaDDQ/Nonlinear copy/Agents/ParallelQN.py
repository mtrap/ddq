'''
Parallel Linear Q agent
Inherits from Base

This is a learner
In the main() function, where this is called, many learners may be called
connected to the same parameter server.
This class holds the update function with linear Q learning updates
it also starts the number of learners per actor to generate experience.

'''

from Agents.Base import Base

import numpy as np
import concurrent.futures
import gym
import torch.nn as nn
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from copy import deepcopy
from collections import deque


class QLearner(Base):
    def __init__(self,config):
        super().__init__(config)
        self.criterion = nn.SmoothL1Loss()
        self.optimizer = torch.optim.Adam(self.policy.parameters(), 
                                            lr=config['alpha'],)
        self.slow_policy = deepcopy(self.policy)
        self.slow_count = 0
        self.update_count = 0
        self.tot_updates = 0


    def update(self, trajectory, episode):
        '''
        inputs:
        trajectory -> tuple: (obs, action, reward, next_obs, done)
        episode -> int: 

        for DQN minimize the MSE loss.
        '''
        self.mem.push(trajectory)
        self.update_count += 1

        if self.update_count >= self.config['maxmem']/2:
            self.update_count = 0
            for _ in range(8):
                self.tot_updates += 1
                self.slow_count += 1
                self.optimizer.zero_grad()
                # get samples from memory
                obs, actions, rewards, next_obs, dones = self.mem.sample()

                with torch.no_grad():
                    next_actions = self.slow_policy(next_obs)
                best_next_action, _ = torch.max(next_actions, axis=1)
                ys = rewards + self.gamma * best_next_action

                act_vals = self.policy(obs)
                # print(f'act_vals: {act_vals}')

                stacked_actions = torch.stack([actions for _ in range(self.numActions)])
                # print(f'stacked_actions: {stacked_actions}')
                selected_act_vals = act_vals.gather(1, stacked_actions.T)[:,0]
                # print(f'selected_actions: {selected_actions}')
                # selected_actions, _ = torch.max(act_vals, axis=1)
                # print(f'shapes, ys: {ys.shape} ; act_vals: {selected_act_vals.shape}')
                loss = self.criterion(ys, selected_act_vals)
                # loss = torch.clamp(loss, min=-1, max=1)
                loss.backward(retain_graph=True) 
                self.optimizer.step()
                self.loss_tracker.append(loss.item())
                with torch.no_grad():
                    mean_td_error = torch.mean(ys - selected_act_vals).item()
                self.TDError_tracker.append(mean_td_error)

                if self.slow_count >= self.config['c']:
                    self.update_slow_policy()
                    self.slow_count = 0


    def __str__(self):
        return 'QNetwork'
        













