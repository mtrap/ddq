from TileFAAlgorithms import GDQ, QAgent, RandomAgent

import numpy as np
from multiprocessing import Pool
import gym
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys

def save_plot(df, path, plotname, repeats):
    pd.options.plotting.backend = "plotly"
    fig = df.plot()
    fig.update_xaxes(title_text="Episodes")
    fig.update_yaxes(title_text="Total Reward")
    fig.update_layout(height=500, width=900, title_text="MountainCar from Degris et al. 2012 \n Repeated "+ repeats +"x")
    fig.write_html(path + plotname +'.html')


def run_training(config, AGENT):
    env = gym.make(config['env'])
    num_states = len(env.observation_space.sample())    
    num_actions = env.action_space.n
    num_episodes = config["episodes"]
    epsilon = config["epsilon"]
    total_rewards = np.zeros(num_episodes)
    episode_lengths = np.zeros(num_episodes)

    # Create agent
    agent = AGENT(env, num_states, num_actions, config['alpha'], config['beta'], config['gamma'], config['kappa'], config['lambda'], config['numTiles'])

    for episode in range(num_episodes):

        obs = env.reset()
        rewards = 0
        agent.trace_reset()
        count = 0

        while True:
            count += 1
            new_action = agent.chooseAction(epsilon, obs)
            next_obs, reward, done, info = env.step(new_action)

            # Udate weights 
            agent.update(reward, obs, new_action, next_obs, episode, epsilon)

            rewards += reward
            obs = next_obs
            if  done:  
                break

        total_rewards[episode] = rewards
        episode_lengths[episode] = count

    # Test Agent
    frames = []
    for episode in range(10):
        obs = env.reset()

        while True:
            new_action = agent.chooseAction(0, obs)
            next_obs, reward, done, info = env.step(new_action)
            img = env.render(mode="human")
            obs = next_obs
            frames.append(img)
            if done:
                break

    write_gif([np.transpose(f, axes=[2,0, 1]) for f in frames],
             config['path'] + '/' + str(agent) + 'test.gif', fps=30)

    return (pd.Series(total_rewards), pd.Series(episode_lengths))



def repeat_mcar(config, AGENT):   
    # python parallel with multiprocessing to speed it up 
    print("Training: ", str(AGENT))
    # start_time = datetime.datetime.now()
    with Pool(config['numProcesses']) as p:
        data = p.starmap(run_training, [(config, AGENT),]*config['repeats'])

    # end_time = datetime.datetime.now()
    # print('Duration: {}'.format(end_time - start_time))
    R_DFS = [] # store the rewards for eac
    EP_LEN_DFS = [] # store the lengths of episodes
    for d in data:
        R_DFS.append(d[0])
        EP_LEN_DFS.append(d[1])

    EP_LEN_DFSS = pd.concat(EP_LEN_DFS, axis=1)
    DFSS = pd.concat(R_DFS, axis=1)

    return DFSS.mean(axis=1), EP_LEN_DFSS.mean(axis=1)
        
        
def main(eps=10, repeats=4):
    
    # RL Training Variables

    ENV =  "MountainCar-v0"
    # Primary learning rate
    ALPHA = .1/11
    # Secondary learning rate
    BETA = .0001/11
    # Probability of taking a random action
    EPSILON = .1
    # λ is the eligibility trace parameter. λ=0 is off.
    LAMBDA = 0.4
    # Discount Factor
    GAMMA = .99
    # Learning rate for our term
    KAPPA = 2

    # number of tiliings to create for Linear Function Approximation
    NUM_TILES = 10
    # How long to train our agents, mc tends do need around 600
    EPISODES = eps
    
    # Num of cpu processors for running repeats in parallel
    NUM_PROC = 4
    REPEATS = repeats



    # Where to save results.    
    PARENT = os.getcwd() + '/Results'

    if not os.path.isdir(PARENT):
        os.makedirs(PARENT)
    DT = str(datetime.datetime.now()).replace(' ','_').replace(':','_')
    FOLDER = 'Results_' + DT
    PATH = os.path.join(PARENT, FOLDER)
    os.mkdir(PATH)

    # config dictionaries to pass to objects
    config_greedy = {
        "env": ENV,
        "alpha": ALPHA,
        "beta": BETA,
        "numTiles": NUM_TILES,
        "kappa": 0,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "lambda": LAMBDA,
        "epsilon": EPSILON,
        "gamma": GAMMA,
        "numProcesses": NUM_PROC,
        "path": PATH
    }

    config_gdq = {
        "env": ENV,
        "alpha": ALPHA,
        "beta": ALPHA,
        "kappa": KAPPA,
        "numTiles": NUM_TILES,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "epsilon": EPSILON,
        "lambda": LAMBDA,
        "gamma": GAMMA,
        "numProcesses": NUM_PROC,
        "path": PATH
    }

    greedy_rewards, greedy_len_steps = repeat_mcar(config_greedy, GDQ)
    gdq_rewards, gdq_len_steps = repeat_mcar(config_gdq, GDQ)
    random_rewards, random_len_steps = repeat_mcar(config_greedy, RandomAgent)
    q_rewards, q_len_steps = repeat_mcar(config_greedy, QAgent)


    rewards_df = pd.DataFrame({'GreedyQ':greedy_rewards, 'Diff-Diff':gdq_rewards, 'Random Agent':random_rewards, 'Q(λ)': q_rewards})
    steps_df = pd.DataFrame({'GreedyQ':greedy_len_steps, 'Diff-Diff':gdq_len_steps, 'Random Agent':random_len_steps, 'Q(λ)': q_len_steps})

    save_plot(rewards_df, PATH, '/MC_AverageRewards' + str(REPEATS) + DT, str(REPEATS))
    save_plot(steps_df, PATH, '/MC_AverageStepsPerEpisode' + str(REPEATS) + DT, str(REPEATS))

    print("Finished.")
    
    
if __name__=='__main__':
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 2:
        # how many episodes
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        # episodes and repeats
        main(int(sys.argv[1]), int(sys.argv[2]))





