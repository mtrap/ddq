import numpy as np
import torch
import torch.nn as nn
import math
import TileCoderSutton as TC
import random
import gym
import pickle
import pandas as pd


class BaseAgent():
    def __init__(self, config):
        self.config = config
        env = gym.make(config['env'])
        # constants
        self.size_obs = (env.observation_space.sample())
        self.numActions =env.action_space.n
        self.gamma = config['gamma']
        self.alpha = config['alpha']
        self.beta = config['beta']
        self.env = config['env']
        self.epsilon = config['epsilon']

        # Scale the inputs for feature representation
        self.obs_low = np.asarray(env.observation_space.low).astype(float)
        # print('Obs low: ', self.obs_low)
        self.obs_high =  np.asarray(env.observation_space.high).astype(float)
        # print('Obs High: ', self.obs_low)

        # tile Coding constants
        self.numTilings = config['numTiles']
        self.iht_num = config['tileVecSize']
        self.iht = TC.IHT(self.iht_num)

        # length of weight vector
        self.weight_size = self.iht_num + 1 # bias 1

        # weight vectors
        self.weights = torch.rand(self.weight_size)
        self.eta = torch.zeros(self.weight_size)

        self.lastBestAction = random.randrange(self.numActions)

        # Trace
        self.trace = torch.zeros(self.weight_size)
        self.lam = config['lambda']

        # closing env
        env.close()

        # track the average of the weights
        self.weight_averages = []

        # agent performance tracking
        self.total_rewards = np.zeros(self.config['episodes'])
        self.episode_lengths = np.zeros(self.config['episodes'])

 
    def getFeatures(self, state, action):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/(self.obs_high[i] - self.obs_low[i]) for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled, [action])
        zeros = torch.zeros(self.iht_num)
        zeros[indices] = 1

        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float()))       
  
        
    def bestAction(self, state):
        # Get action value array
        actions = [torch.dot(self.weights, self.getFeatures(state, act)) for act in range(self.numActions)]
        # The max of action value is the best action
        action = torch.argmax(torch.tensor(actions)).item()
        return action

    def chooseAction(self, state):
        # epsilon Q action policy.
        # if less than epsilon than take random action
        if random.random() < self.epsilon:
            return random.randrange(self.numActions)
        else:
            return self.bestAction(state)

    def trace_reset(self):
        self.trace = torch.zeros(self.weight_size)


    def train(self):
        env = gym.make(self.config['env'])

        for episode in range(self.config["episodes"]):
            # resets for new episode
            obs = env.reset()
            rewards = 0
            steps = 0

            while True:
                # take step and get new obs
                action = self.chooseAction(obs)
                next_obs, reward, done, info = env.step(action)

                # Udate weights 
                self.update(reward, obs, action, next_obs, episode)
                
                # increment trackers
                steps += 1
                rewards += reward

                # update obs
                obs = next_obs
                if  done:  
                    break
                        

            # Save Trackers
            self.total_rewards[episode] = rewards
            self.episode_lengths[episode] = steps


        env.close()

        return (pd.Series(self.total_rewards), pd.Series(self.episode_lengths), self)  

    def save(self):
        #Export trained agent to a file to later get for testing.
        agent_file = '/' + str(self) + '.agent'

        
        with open(self.config['agents path'] + agent_file, 'wb') as af:
            # save agent state
            pickle.dump(self, af)

    def get_agent_performance(self):
        return np.mean(self.total_rewards), np.mean(self.episode_lengths)


class GreedyGQ(BaseAgent):
    def __init__(self, config):
        super().__init__(config)

    def update(self, reward, current_state_orig, action, next_state_orig, episode):
        # get next best action
        bestAction = self.bestAction(next_state_orig)
        
        # get state representations 
        current_state = self.getFeatures(current_state_orig, action)
        next_state = self.getFeatures(next_state_orig, bestAction)
            
        # Importance Sampling Ratio
        # rho = 1/(1-self.epsilon) if action == self.lastBestAction else 0

        # Trace 
        current_trace = current_state #+ rho * (self.gamma * self.lam * self.trace)

        # Temporal Difference
        temp_diff = reward + self.gamma * torch.matmul(next_state, self.weights) -  torch.matmul(current_state, self.weights)
        
        # x * eta
        xdoteta = torch.matmul(current_state, self.eta)
        # secondary weights update
        next_eta = self.eta + self.beta * (temp_diff * current_trace - xdoteta * current_state)    

        # Primary Weights update
        next_weights = self.weights + self.alpha * (temp_diff * current_trace - self.gamma * (1 - self.lam) * xdoteta * next_state)
        
        # Overwrite old weights
        self.weights = next_weights
        self.eta = next_eta
        self.trace = current_trace
        self.lastBestAction = bestAction
        self.weight_averages.append(torch.mean(self.weights))

    def __str__(self):
        return f'Greedy_GQ_{self.alpha}_{self.beta}'


class GDQ(BaseAgent):
    def __init__(self, config):
        super().__init__(config)
        # when Kappa = 0 GDQ the update equation is the same as Greedy-GQ.
        self.previous_weights = torch.zeros(self.weight_size) 
        # the kappa term needs the C factor for c*alpha
        self.kappa = config['kappa'] * config['alpha']
        self.kappa_factor = config['kappa']

  
    def update(self, reward, current_state_orig, action, next_state_orig, episode):
        # get next best action
        bestAction = self.bestAction(next_state_orig)
        
        # get state representations 
        current_state = self.getFeatures(current_state_orig, action)
        next_state = self.getFeatures(next_state_orig, bestAction)
        
        # importance sampling ratio
        # rho = 1/(1-self.epsilon) if action == self.lastBestAction else 0
        # Trace
        current_trace = current_state #+ rho * (self.gamma * self.lam * self.trace)
        # x * w
        xdotw = torch.dot(current_state, self.weights)

        # TD Error
        temp_diff = reward + self.gamma * torch.dot(next_state, self.weights) - xdotw
        
        # x * eta
        xdoteta = torch.matmul(current_state, self.eta)

        # Secondary weight update
        next_eta = self.eta + self.beta * (temp_diff * current_trace - xdoteta * current_state)

        #weight update
        next_weights = self.weights + self.alpha * (temp_diff * current_trace - self.gamma * (1 - self.lam) * xdoteta * next_state) - \
                         self.kappa * (xdotw - torch.dot(current_state, self.previous_weights)) * current_state
            
        # Overwrite old weights
        self.previous_weights = self.weights.clone().detach()
        self.weights = next_weights
        self.eta = next_eta
        self.trace = current_trace
        self.lastBestAction = bestAction
        self.weight_averages.append(torch.mean(self.weights))



    def __str__(self):
        return "GDQ_w_kappa_" + str(self.kappa_factor)

        
class QAgent(BaseAgent):
    def __init__(self, config):
        super().__init__( config)
        
    def update(self, reward, current_state_orig, action, next_state_orig, episode):
            # get next best action
            bestAction = self.bestAction(next_state_orig)

            # get state representations 
            current_state = self.getFeatures(current_state_orig, action)
            next_state = self.getFeatures(next_state_orig, bestAction)

            # importance sampling ratio
            # rho = 1/(1-self.epsilon) if action == self.bestAction(current_state_orig) else 0
            # trace
            current_trace = current_state #+ rho * (self.gamma * self.lam * self.trace)

            # Temporal Difference
            temp_diff = reward + self.gamma * torch.matmul(next_state, self.weights) -  torch.matmul(current_state, self.weights)
            # Weight calculation
            next_weights = self.weights + self.alpha * temp_diff * current_trace 
            
            # Overwrite old weights
            self.weights = next_weights
            self.trace = current_trace

    def __str__(self):
        return "Q_Learning"

class RandomAgent(QAgent):
    def __init__(self, config):
        super().__init__(config)


    def chooseAction(self, obs):
        return random.randint(0, self.numActions- 1)

    def __str__(self):
        return "RandomAgent"





