'''
Main.py for Cartpole with DDQ
'''

from TileFAAlgorithmsCP import GreedyGQ, GDQ, QAgent, RandomAgent
from sendmail import sendmail

import numpy as np
import concurrent.futures
import gym
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from tqdm import tqdm
import ray
from scipy.signal import savgol_filter

def save_plot(df, path, plotname, config, yaxis):
    pd.options.plotting.backend = "plotly"
    fig = df.plot()
    fig.update_xaxes(title_text="Episodes")
    fig.update_yaxes(title_text=yaxis)
    fig.update_layout(height=700, width=1400, title_text=config['env'] + " \n Repeated "+ str(config['repeats']) +"x")
    fig.write_html(path + plotname +'.html')

@ray.remote
def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    return agent.train()

@ray.remote
def train(config, AGENT):
    print("Training: ", str(AGENT), f'config: {config}')

    result_ids_train = [trainWrapper.remote((config, AGENT)) for _ in range(config['repeats'])]

    data = []
    while len(result_ids_train):
        done_ids_train, result_ids_train = ray.wait(result_ids_train)
        data += ray.get(done_ids_train)
        
    R_DFS = [] # store the rewards for eac
    EP_LEN_DFS = [] # store the lengths of episodes
    agents = []
    # end_rewards = []
    for d in data:
        # print('d from data: ', d)
        R_DFS.append(d[0])
        EP_LEN_DFS.append(d[1])
        agents.append(d[2])
    #     end_rewards.append(d[2])
    # sort agents
    def sort_func(a):
        return a.get_agent_performance()[0]

    agents.sort(reverse=True, key=sort_func)
    # save the first one.
    agents[0].save()
    
    EP_LEN_DFSS = pd.concat(EP_LEN_DFS, axis=1)
    DFSS = pd.concat(R_DFS, axis=1)
    mean_rewards = DFSS.mean(axis=1)
    # smooth the graph
    smoothed_mean_rewards = pd.Series(savgol_filter(mean_rewards, 17, 3))
    mean_eps = EP_LEN_DFSS.mean(axis=1)
    smoothed_mean_eps = pd.Series(savgol_filter(mean_eps, 17, 3))
    # weight averages
    W_AVES = pd.concat(weight_avs, axis=1)
    return {'reward': smoothed_mean_rewards, 
            'ep_len': smoothed_mean_eps, 
            'name': str(AGENT) + f'kappa: {config["kappa"]}',
            'all_metrics': {'rewards': DFSS, 'steps': EP_LEN_DFSS},
            'weight_ave': W_AVES.mean(axis=1),
            } 

        
def main(eps=10, repeats=4, cpus=4):
    
    # RL Training Variables
    ENV =  "CartPole-v1"
    # Primary learning rate
    ALPHA = .1
    # Secondary learning rate
    BETA = .001
    # Probability of taking a random action
    EPSILON = .1
    # λ is the eligibility trace parameter. λ=0 is off.
    LAMBDA = 0
    # Discount Factor
    GAMMA = .99
    # Learning rate for our term
    # go through many
    KAPPAS = [0, 0.25, 0.5, 0.75, 1, 2, 4]

    # number of tiliings to create for Linear Function Approximation
    NUM_TILES = 10
    TILE_VEC_LEN = 10**6
    # How long to train our agents, mc tends do need around 600
    EPISODES = eps
    
    # Num of cpu processors for running repeats in parallel
    REPEATS = repeats


    # Create a parent path in results to store a results_dt file 
    # the results_dt file will have a graphs folder, agents folder, and txt file with details of training
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)


    DT = datetime.datetime.now().strftime("%b_%d_%Y_%H_%M_%S")
    PARENT = ROOT_PATH + 'Results_' + DT
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'   
    RESULTS_PATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTS_PATH = os.path.join(PARENT, AGENTS_STR)
    os.mkdir(RESULTS_PATH)
    os.mkdir(AGENTS_PATH)

    # text file to write details of the run
    DOC_STRING = RESULTS_PATH + '/params.txt'


    # config dictionaries to pass to objects
    config = {
        "env": ENV,
        "alpha": ALPHA,
        "beta": BETA,
        "numTiles": NUM_TILES,
        "tileVecSize": TILE_VEC_LEN,
        "kappa": 0,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "lambda": LAMBDA,
        "epsilon": EPSILON,
        "gamma": GAMMA,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH
    }


    inputs = [(config, GreedyGQ), (config, RandomAgent), (config, QAgent)]
    # add GDQ agents with updated config w/ unqiue kappas
    for k in KAPPAS:
        config['kappa'] = k
        inputs.append((config.copy(), GDQ))

    cpus =  cpus

    ray.init(num_cpus=cpus)
    result_ids = [train.remote(pair[0], pair[1]) for pair in inputs]
    futures = ray.get(result_ids)
    

    rewards_dict = dict()
    len_dict = dict()
    averageRewards = []
    averageSteps = []
    aveWeights = dict()
    for res in futures:
        rewards_dict[res['name']]=res['reward']
        len_dict[res['name']]=res['ep_len']
        averageRewards.append((res['name'], np.mean(res['reward'])))
        averageSteps.append((res['name'], np.mean(res['ep_len'])))
        aveWeights[res['name']] = res['weight_ave']
    
    ray.shutdown()

    # with concurrent.futures.ThreadPoolExecutor(max_workers=6) as executor:
    #     futures = executor.map(repeat_training, *zip(*inputs))

    # rewards_dict = dict()
    # len_dict = dict()
    # for res in futures:
    #     rewards_dict[res['name']]=res['reward']
    #     len_dict[res['name']]=res['ep_len']

         


    rewards_df = pd.DataFrame(rewards_dict)
    steps_df = pd.DataFrame(len_dict)
    weight_ave_df = pd.DataFrame(aveWeights)

    save_plot(rewards_df, RESULTS_PATH, '/MeanRewards' + str(REPEATS) + DT, config, 'Total Rewards')
    save_plot(steps_df, RESULTS_PATH, '/MeanStepsPerEpisode' + str(REPEATS) + DT, config,'Total Steps')
    save_plot(weight_ave_df, RESULTS_PATH, '/AverageWeights', config, 'Average Weight Value')

    with open(DOC_STRING, 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(DOC_STRING, 'w') )
        prnt.pprint(config)



    print("Finished.")
    sendmail('mtrappett@icloud.com', ENV + ' has finished','All done on ' + socket.gethostname() + '\n\nRun started on ' + DT)

    
    
if __name__=='__main__':
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 2:
        # how many episodes
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        # episodes and repeats
        main(int(sys.argv[1]), int(sys.argv[2]))
    elif len(sys.argv) == 4:
        # episodes and repeats, cpus
        main(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))





