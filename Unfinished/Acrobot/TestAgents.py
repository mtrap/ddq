# TestAgents.py
# from TileFAAlgorithms import GreedyGQ, GDQ, QAgent, RandomAgent

import pickle
import glob
import os
import sys



def main(results_path):
    # check if parent Agents folder exists or make one
    VID_PATH = os.getcwd() + '/Videos'
    if not os.path.isdir(VID_PATH):
        os.makedirs(VID_PATH)

    # config dict
    config = {'env': 'Acrobot-v1',
              'vid_path': VID_PATH}
    # read in agents and run test
    agents = glob.glob(results_path + '/Agents/*.agent')
    # print(agents)
    for agent_file in agents: 
        # use pickle to recreate trained agents
        with open(agent_file, 'rb') as af:
            agent = pickle.load(af)

        agent.test(config)

        


if __name__=='__main__':
    # pass in /path/to/Results/Results_(date) - path
    main(sys.argv[1])


