'''
Author: Matthew Trappett
Tune file for Acrobot DDQ/greedyGQ
'''

# Get Agents
from TileFAAlgorithmsAcro import GreedyGQ
from utils.sendmail import sendmail

import numpy as np
import concurrent.futures
import gym
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
import ray

def save_plot(df, path, plotname, config, yaxis):
    pd.options.plotting.backend = "plotly"
    fig = df.plot()
    fig.update_xaxes(title_text="Episodes")
    fig.update_yaxes(title_text=yaxis)
    fig.update_layout(height=500, width=900, title_text=config['env'] + " Tune GreedyGQ <br> agent_alpha_beta")
    fig.write_html(path + plotname +'.html')


def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    return agent.train()

@ray.remote
def train(config, AGENT):
    # python parallel with multiple threads to speed it up 
    # Threads were faster than processes in practice.
    print("Training: ", str(AGENT), f'config: {config}')

    data = trainWrapper((config, AGENT))

    EP_LEN_DFSS = pd.Series(data[0])
    DFSS = pd.Series(data[1])
    return {'reward': DFSS, 
            'ep_len':EP_LEN_DFSS, 
            'name': str(AGENT) + f'alpha: {config["alpha"]}, beta: {config["beta"]}',
            }
      
        
        
def main(eps=2, repeats=1):
    repeats=1
    
    # RL Training Variables
    ENV =  "Acrobot-v1"
    # Hyper parameters
    ALPHA_list = [1.0, .5, 0.1, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001] # 8
#    ALPHA_list = [1.0, 0.001]
    ALPHA = 1.0
    BETA_list = [0.1, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001] # 6
 #   BETA_list = [0.1, 0.0001]
    BETA = 0.000001

    EPSILON = .1
    LAMBDA = 0
    GAMMA = .99

    # Learning rate for our term
    KAPPA = 2

    # number of tiliings to create for Linear Function Approximation
    NUM_TILES = 10
    TILE_VEC_LEN = 10**6
    # How long to train our agents, mc tends do need around 600
    EPISODES = eps
    
    # the number of repeats is the number of subthreads spawned. 
    REPEATS = repeats



    # Create a parent path in results to store a results_dt file 
    # the results_dt file will have a graphs folder, agents folder, and txt file with details of training
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)


    DT = datetime.datetime.now().strftime("%b_%d_%Y_%H_%M_%S")
    PARENT = ROOT_PATH + 'TuneResults_' + DT
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'   
    RESULTS_PATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTS_PATH = os.path.join(PARENT, AGENTS_STR)
    os.mkdir(RESULTS_PATH)
    os.mkdir(AGENTS_PATH)

    # text file to write details of the run
    DOC_STRING = RESULTS_PATH + '/params.txt'

    # config dictionaries to pass to objects
    config = {
        "env": ENV,
        "alpha": ALPHA,
        "beta": BETA,
        "numTiles": NUM_TILES,
        "tileVecSize": TILE_VEC_LEN,
        "kappa": 0,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "lambda": LAMBDA,
        "epsilon": EPSILON,
        "gamma": GAMMA,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH
    }

    inputs = []

    for a in ALPHA_list:
        for b in BETA_list:
            config['alpha'] = a
            config['beta'] = b
            inputs.append((config.copy(), GreedyGQ))

    cpus = 56

    ray.init(num_cpus=cpus)
    result_ids = [train.remote(pair[0], pair[1]) for pair in inputs]
    futures = ray.get(result_ids)
    

    rewards_dict = dict()
    len_dict = dict()
    averageRewards = []
    averageSteps = []
    for res in futures:
        rewards_dict[res['name']]=res['reward']
        len_dict[res['name']]=res['ep_len']
        averageRewards.append((res['name'], np.mean(res['reward'])))
        averageSteps.append((res['name'], np.mean(res['ep_len'])))
    
    ray.shutdown()
    # get the top 10 names based on mean rewards and ep length
    averageRewards.sort(key=lambda x: x[1])
    averageSteps.sort(key=lambda x: x[1])
    top_ten = [pair[0] for ave in [averageRewards, averageSteps] for pair in ave[:5]]
    # print(top_ten)
    rewards_df = pd.DataFrame({key:rewards_dict[key] for key in top_ten})
    steps_df = pd.DataFrame({key:len_dict[key] for key in top_ten})
    # rewards_df = pd.DataFrame(rewards_dict)
    # steps_df = pd.DataFrame(len_dict)
    save_plot(rewards_df, RESULTS_PATH, '/MeanRewards' + str(REPEATS) + DT, config, 'Total Rewards')
    save_plot(steps_df, RESULTS_PATH, '/MeanStepsPerEpisode' + str(REPEATS) + DT, config,'Total Steps')


    with open(DOC_STRING, 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(DOC_STRING, 'w') )
        prnt.pprint(config)
        prnt.pprint(rewards_df)


    print("Finished.")
    sendmail('mtrappett@icloud.com', ENV + ' has finished','All done on ' + socket.gethostname() + 
                                                            '\n' + str(config) + '\n' + str(top_ten))

    
    
if __name__=='__main__':
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 2:
        # how many episodes
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        # episodes and repeats
        main(int(sys.argv[1]), int(sys.argv[2]))





