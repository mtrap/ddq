# GTDD RL with Neural Networks
# Author: Matthew Trappett
# Date: April 18, 2022
#
# takes commandline arguments:
# agent: name of agent to train and run
# env: which python environemt to train on 1=MC, 2=CP, 3=Acrobot
# num_eps: how many episodes to train on
# repeats: number of times to repeat training

# will train agent on the env given for the number of episodes and repeats

# output:
# a collection of graphs in a results folder 
# saves the best agent to an agents folder

from Agents.DQN import DQNAgent
from Agents.DGQN import GQNAgent
from utils.sendmail import sendmail
from utils.DataProcessing import process_results

import numpy as np
import concurrent.futures
import gym
import torch
from datetime import datetime
import plotly.graph_objs as go
import pandas as pd
import random
import socket
import os
import sys
import traceback
import pickle
import pprint as pp
from tqdm import tqdm
import ray
from scipy.signal import savgol_filter





@ray.remote
def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    print(f'Training agent: {agent}')
    return agent.train()



def main(agent_str, env, numEpisodes, repeats, cpus, seed=1):
    '''
    folder needs to have the agent+env+datetime
    '''
    numEpisodes = int(numEpisodes)
    repeats = int(repeats)
    cpus = int(cpus)
    # torch.manual_seed(1423)

    print(f'Training with \n \
            Agent: {agent_str} \n \
            env: {env} \n \
            nEpisodes: {nEpisodes} \n \
            repeats: {repeats} \n \
            cpus: {cpus}')

    # agents dict
    agent_dict = {'dqn': DQNAgent,'gq': GQNAgent, 'gdq': GQNAgent} #'Greedy_GQ': GreedyGQ, 'GTDD':GTDD}
    AGENT = agent_dict[agent_str]

    # saved alphas tuned for each agent
    alphas = {'MountainCar-v0':{'gq': 0.05, 'gdq': 0.01, 'dqn': 0.001, 'r': 0.1},
              'CartPole-v0':   {'gq': 0.01, 'gdq': 0.01, 'dqn': 0.001, 'r': 0.1},
              'Acrobot-v1':    {'gq': 0.05, 'gdq': 0.1, 'dqn': 0.001, 'r': 0.1},
             }

    kappas = {'MountainCar-v0':4.0,
              'CartPole-v0':   1.5,
              'Acrobot-v1':    0.75,
             }

    betas = {'MountainCar-v0':{'gq': 0.05, 'gdq': 0.01, 'dqn': 0.01, 'r': 0.1},
              'CartPole-v0':   {'gq': 1e-05, 'gdq': 0.01, 'dqn': 0.05, 'r': 0.1},
              'Acrobot-v1':    {'gq': 0.05, 'gdq': 0.01, 'dqn': 0.05, 'r': 0.1},
             }


    # set up paths for saving graphs and agent
    # agent + env + DT
    date_string = '{:%B_%d_%Y_%H_%M_%S}'.format(datetime.now())
    env_string = env.replace('-', '_')
    FolderName = '_'.join([agent_str, env_string, date_string])

    # check if Results folder exits and create one if not
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)

    PARENT = ROOT_PATH + FolderName
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'  
    DATA_STR   = 'Data/' 
    GRAPHSPATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTSPATH = os.path.join(PARENT, AGENTS_STR)
    DATAPATH = os.path.join(PARENT, DATA_STR)
    os.mkdir(GRAPHSPATH)
    os.mkdir(AGENTSPATH)
    os.mkdir(DATAPATH)




    # master config dictionary
    config = {# for all agents
              'env':        env,
              'episodes':   int(numEpisodes),
              'repeats':    int(repeats),
              'cpus':       int(cpus),
              # for different initial conditions
              # for cp and mountain car
              'alpha':      1e-2, # 1e-3 for DQN on cartpole
              'h_size':     100,
              # 'h2_size':  10,
              'gamma':      0.99,
              'epsilon':    1,
              'debug':      False, # saves lots of graphs to print out.
              'tune':       False,
              
              # if converting obs to feaature vector
              'numTiles':   10, # number of tiles per tiling
              'tileVecSize': int(1e4), # number of tilings

              # dqn
              'c':          15, # zero means that a copy is made every time
              "force":      0.001, # originally 0.001 for discrete for MC
              'batch_size': 64, # dqn: 32
              'maxmem':     256, # dqn: 256
              ######### Acrobot testing
              # 'alpha':      1e-3,
              # 'h_size':     64,
              # 'h2_size':    128,
              # 'gamma':      0.95,
              # 'epsilon':    1,
              # 'debug':      True, # saves lots of graphs to print out.
              # 'tune':       False,

              # # dqn
              # 'c':          15, # zero means that a copy is made every time
              # "force":      0.001, # originally 0.001 for discrete for MC
              # 'batch_size': 32,
              # 'maxmem':     256,

              # for Greedy-GQ/GTDD
              'beta':       1e-3, 
              'kappa':      0.75,

              # paths
              'AgentPath': AGENTSPATH,
              'GraphPath': GRAPHSPATH,
              'DataPath' : DATAPATH,
                }

    if agent_str == 'gq':
        config['kappa'] = 0

    # # initialize agent
    # agent = AGENT(config)
    # data_dict, trained_agent = agent.train()
    # start ray
    ray.init(num_cpus=cpus)
    # for a number of repeats train the agent on the env
    result_ids_train = [trainWrapper.remote((config, AGENT)) for _ in range(config['repeats'])]
    # as the agent finishes then 
    data = []
    while len(result_ids_train):
        done_ids_train, result_ids_train = ray.wait(result_ids_train)
        data += ray.get(done_ids_train)

    # stop ray
    ray.shutdown()

    print('Finished Training...\nProcessing Data...')
    # process results, make graphs and save them and best agent
    process_results(config, data)



    # save config to know our parameters
    with open(GRAPHSPATH + '/params.txt', 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(GRAPHSPATH + '/params.txt', 'w') )
        prnt.pprint(config)


    # send email to signal done
    sendmail('mtrappett@icloud.com',  env + ' Nonlinear has finished',
        'All done on ' + socket.gethostname() + 
        '\n\nRun started on ' + 
        date_string + '\n\nCommandline script to download\n\nscp -r MTrappett@orthus.nic.uoregon.edu:'+PARENT+
         ' ~/Downloads/NonLinearResults')



if __name__=='__main__':
    assert len(sys.argv) >= 4, 'Need arguments: agent, env, numEpisodes'
    agent = sys.argv[1]
    env = sys.argv[2]
    nEpisodes = sys.argv[3]

    env_dict = {'mc':'MountainCar-v0' ,'cp':'CartPole-v0' ,'ab':'Acrobot-v1' }
    # optional repeat paramter, for production graph will want a lot.
    repeats = 1
    cpus = 4
    seed = 1423
    if len(sys.argv) > 4:
        repeats = sys.argv[4]
    if len(sys.argv) == 6:
        cpus = sys.argv[5]
    if len(sys.argv) == 7:
        seed = sys.argv[6]

    try:
        main(agent, env_dict[env], nEpisodes, repeats, cpus)

        print('Finished')
    except:
        # send email to signal done
        sendmail('mtrappett@icloud.com',  env + "threw an error",
            'error on ' + socket.gethostname() + 
            'error ' + 
            str(traceback.format_exc(limit=None, chain=True)))
















