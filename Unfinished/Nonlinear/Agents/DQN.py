'''
DQN Agent
Inherits from BaseAgent

learned a lot from this example:
https://github.com/mahakal001/reinforcement-learning/tree/master/cartpole-dqn
'''

from Agents.BaseAgent import BaseAgent

import numpy as np
import concurrent.futures
import gym
import torch.nn as nn
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from copy import deepcopy
from collections import deque


class memory():
    def __init__(self, maxlength, batch_size=1):
        self.memories = deque([], maxlen=maxlength)
        self.batch_size = batch_size

    def push(self, trajectory):
        self.memories.append(trajectory)

    def sample(self):
        size = self.batch_size
        if not self.batchable:
            size = len(self.memories)
        samples = random.sample(self.memories, size)
        obs = torch.stack([a[0] for a in samples])
        actions = torch.tensor([a[1] for a in samples]).long()
        rewards = torch.tensor([a[2] for a in samples]).float()
        next_obs = torch.stack([a[3] for a in samples])
        dones = torch.tensor([a[4] for a in samples]).float()
        return obs, actions, rewards, next_obs, dones


    def batchable(self):
        return len(self.memories) > self.batch_size


class DQNAgent(BaseAgent):
    def __init__(self,config):
        super().__init__(config)
        self.criterion = nn.SmoothL1Loss()
        self.optimizer = torch.optim.Adam(self.policy.parameters(), 
                                            lr=config['alpha'],)
        self.slow_policy = deepcopy(self.policy)
        self.slow_count = 0
        self.update_count = 0
        self.tot_updates = 0

        # memory
        self.mem = memory(config['maxmem'], config['batch_size'])

    def update_slow_policy(self):
        self.slow_policy.load_state_dict(self.policy.state_dict())
        # self.slow_policy.requires_grad = False
        
    @torch.no_grad()
    def slowActionValues(self, obs):
        # scale state values
        scaled = [obs[i]/self.spread[i] for i in range(len(obs))]
        scaled_obs = torch.tensor(scaled,
                               requires_grad=True, dtype=torch.float32)
        return self.slow_policy(scaled_obs)


    def update(self, trajectory, episode):
        '''
        inputs:
        trajectory -> tuple: (obs, action, reward, next_obs, done)
        episode -> int: 

        for DQN minimize the MSE loss.
        '''
        self.mem.push(trajectory)
        self.update_count += 1

        if self.update_count >= self.config['maxmem']/2:
            self.update_count = 0
            for _ in range(8):
                self.tot_updates += 1
                self.slow_count += 1
                self.optimizer.zero_grad()
                # get samples from memory
                obs, actions, rewards, next_obs, dones = self.mem.sample()

                with torch.no_grad():
                    next_actions = self.slow_policy(next_obs)
                best_next_action, _ = torch.max(next_actions, axis=1)
                ys = rewards + self.gamma * best_next_action

                act_vals = self.policy(obs)
                # print(f'act_vals: {act_vals}')

                stacked_actions = torch.stack([actions for _ in range(self.numActions)])
                # print(f'stacked_actions: {stacked_actions}')
                selected_act_vals = act_vals.gather(1, stacked_actions.T)[:,0]
                # print(f'selected_actions: {selected_actions}')
                # selected_actions, _ = torch.max(act_vals, axis=1)
                # print(f'shapes, ys: {ys.shape} ; act_vals: {selected_act_vals.shape}')
                loss = self.criterion(ys, selected_act_vals)
                # loss = torch.clamp(loss, min=-1, max=1)
                loss.backward(retain_graph=True) 
                self.optimizer.step()
                self.loss_tracker.append(loss.item())
                with torch.no_grad():
                    mean_td_error = torch.mean(ys - selected_act_vals).item()
                self.TDError_tracker.append(mean_td_error)

                if self.slow_count >= self.config['c']:
                    self.update_slow_policy()
                    self.slow_count = 0


    def __str__(self):
        return 'QNetwork'
        













