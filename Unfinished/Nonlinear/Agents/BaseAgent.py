'''
Base Agent for GTDD Non-linear algorithms
Author: Matthew Trappett
Date: April 18, 2022
'''

import numpy as np
import concurrent.futures
import gym
import torch.nn as nn
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from copy import deepcopy

from PyFixedReps import TileCoder # https://github.com/andnp/PyFixedReps
from utils import TileCoderSutton as TC


class BaseAgent():
    def __init__(self, config):
        torch.manual_seed(0)
        self.config = config
        env = gym.make(config['env'])
        # constants
        self.size_obs =   len(env.observation_space.sample())
        self.numActions = env.action_space.n
        self.gamma =      torch.tensor(config['gamma']).float()
        self.alpha =      config['alpha']
        self.beta =       config['beta']
        self.env =        config['env']
        self.epsilon =    config['epsilon']

        # Scale the inputs for NN input
        self.obs_low =     np.asarray(env.observation_space.low).astype(float)
        self.obs_high =    np.asarray(env.observation_space.high).astype(float)
        self.spread = self.obs_high - self.obs_low

        # closing env
        env.close()

        # tile Coding constants
        self.numTilings = config['numTiles']
        self.iht_num =    config['tileVecSize']
        self.iht =        TC.IHT(self.iht_num)
        # self.tc = TileCoder({
        #     'tiles': config['numTiles'],
        #     'tilings': config['tileVecSize'],
        #     'dims': self.size_obs,

        #     'random_offset': True,
        #     'input_ranges': [(a,b) for a,b in zip(self.obs_low, self.obs_high)],
        #     'scale_ouput': True,
        #     })

        num_features = self.iht_num
        # print(f'Num of features: {num_features}')


        # initialize a network
        # input size is size_obs
        # output size is num of actions, indexing determines the action
        self.policy = nn.Sequential(
          # nn.Linear(self.size_obs, config['h_size']),
          # nn.ReLU(),
          # nn.Identity(),
          # nn.Tanh(),
          # nn.Linear(config['h_size'], config['h2_size']),
          # nn.ReLU(),
          # nn.Linear(config['h2_size'], config['h3_size']),
          # nn.ReLU(),
          nn.Linear(num_features, self.numActions),
          nn.Identity()
        )

        # agent performance tracking
        self.total_rewards =   np.zeros(self.config['episodes'])
        self.observations =    []
        self.action_tracker =  []
        self.TDError_tracker = []
        self.epsilon_tracker = []
        self.loss_tracker =    []
        self.mean_weights =    np.zeros(self.config['episodes'])
        self.debug =           config['debug']

        #save weights if debug
        if self.debug:
            num_weights = sum(p.numel() for p in self.policy.parameters() if p.requires_grad)
            self.weight_history = np.zeros((num_weights, self.config['episodes']))

        # save last action to save a computational step
        # only used when we thought we needed eligibility traces
        self.lastBestAction = random.randrange(self.numActions)


    def getFeatures(self, state):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/self.spread[i] for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled)
        phi = torch.zeros(self.iht_num)
        phi[indices] = 1

        # convert to torch for easier math.
        return phi.float()


    def ActionValues(self, obs):
        # scale state values
        # scaled = [obs[i]/self.spread[i] for i in range(len(obs))]
        # scaled_obs = torch.tensor(scaled,
        #                        requires_grad=True, dtype=torch.float32)
        # return self.policy(scaled_obs)
        return self.policy(obs)


    def chooseAction(self, obs):
        # epsilon Q action policy.
        # if less than epsilon than take random action
        if random.random() < self.epsilon:
            return torch.randint(0,self.numActions,(1,)).item()
        else:
            with torch.no_grad():
                # act_values = self.ActionValues(obs)
                feats = self.getFeatures(obs)
                act_values = self.policy(feats) # if using features
            best = torch.argmax(act_values).item()
            return best


    def train(self):
        env = gym.make(self.config['env'])
        if self.config['env'] == 'MountainCar-v0':
            env.force = self.config['force'] # can be altered for MC
        for episode in range(self.config["episodes"]):
            # resets for new episode
            obs = env.reset()
            rewards = 0

            while True:
                # take step and get new obs
                action = self.chooseAction(obs)
                next_obs, reward, done, info = env.step(action)
                # Udate weights 
                self.update((torch.from_numpy(obs), action, reward, torch.from_numpy(next_obs), done), episode)
                # self.update((obs, action, reward, next_obs, done), episode) # if using features
                # increment trackers
                rewards += reward
                self.action_tracker.append(action)
                # if self.debug:
                #     self.observations.append([obs[i]/self.spread[i] for i in range(len(obs))])

                # update obs
                obs = next_obs

                if  done:  
                    break

            if self.epsilon > 0.05:
                self.epsilon -= (2/self.config['episodes'])

            # Save Trackers
            self.epsilon_tracker.append(self.epsilon)
            self.total_rewards[episode] = rewards
            self.mean_weights[episode] = torch.mean(torch.nn.utils.parameters_to_vector(self.policy.parameters()).detach())
            if self.debug:
                self.weight_history[:,episode] = torch.nn.utils.parameters_to_vector(self.policy.parameters()).detach()

        env.close()
        # print(f'Total updates: {self.tot_updates}')
        return self.return_data()


    def save(self):
        #Export trained agent to a file to later get for testing.
        agent_file = '/' + str(self) + '.agent'
        with open(self.config['AgentPath'] + agent_file, 'wb') as af:
            # save agent state
            pickle.dump(self, af)
        # print('NOT SAVING AGENT! PICKLE ERROR')
        # pass


    def return_data(self):
        # process the observations to turn into a pd DF for graphing.
        # return a list of pd.Series for each obs. 
        # len(obs) = # of obs
        data_dict = dict()

        data_dict['Total Rewards'] =  pd.Series(self.total_rewards)
        data_dict['Actions Taken'] =  pd.Series(self.action_tracker)
        data_dict['Average reward'] = np.mean(self.total_rewards)
        data_dict['Mean Weights']   = pd.Series(self.mean_weights)
        if self.debug:
            data_dict['network params'] = self.weight_history

            # stack_obs = np.stack(self.observations)
            # # obs is scaled
            # for ob in range(self.size_obs):
            #     data_dict[f'obs[{ob}]'] = pd.Series(stack_obs[:,ob])

        data_dict['TDErrors'] =       pd.Series(self.TDError_tracker)
        data_dict['Self Object'] =    self
        data_dict['Epsilon vals'] =   pd.Series(self.epsilon_tracker)
        data_dict['loss_tracker'] =   pd.Series(self.loss_tracker)


        return data_dict

    def update(self, *args):
        pass















