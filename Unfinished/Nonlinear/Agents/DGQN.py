'''
GreedyQNetwork
Inherits from Base Agent
'''

from Agents.BaseAgent import BaseAgent

import numpy as np
import concurrent.futures
import gym
import torch.nn as nn
import torch
from torch.autograd.functional import hessian
from torch.nn.utils import _stateless

import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from copy import deepcopy
from collections import deque




class memory():
    def __init__(self, maxlength, batch_size=1):
        self.memories = deque([], maxlen=maxlength)
        self.batch_size = batch_size

    def push(self, trajectory):
        self.memories.append(trajectory)

    def sample(self):
        size = self.batch_size
        if not self.batchable:
            size = len(self.memories)
        samples = random.sample(self.memories, size)
        obs = torch.stack([a[0] for a in samples])
        actions = torch.tensor([a[1] for a in samples]).long()
        rewards = torch.tensor([a[2] for a in samples]).float()
        next_obs = torch.stack([a[3] for a in samples])
        dones = torch.tensor([a[4] for a in samples]).float()
        return obs, actions, rewards, next_obs, dones

    def batchable(self):
        return len(self.memories) > self.batch_size


class GQNAgent(BaseAgent):
    def __init__(self,config):
        super().__init__(config)
        self.numParams = sum(p.numel() for p in self.policy.parameters() if p.requires_grad)
        self.eta = deepcopy(self.policy)
        with torch.no_grad():
            for e in self.eta.parameters():
                e.copy_(torch.zeros(e.shape))

        self.previous_policy = deepcopy(self.policy)
        self.update_count = 0
        self.tot_updates = 0
        self.kappa = config['kappa'] * config['alpha']
        self.kappa_factor = config['kappa']
        self.batch_size = config['batch_size']
        self.optimizer = torch.optim.SGD(self.policy.parameters(), lr=self.config['alpha']) 
        # memory
        self.mem = memory(config['maxmem'], 1) # gddq pulls a memory 1 at a time
        # Track dw
        self.track_dw1 = []
        self.track_dw2 = []
        # print('Prev policy id')
        # print(id(self.previous_policy))
        # print('policy id')
        # print(id(self.policy))


    def update(self, trajectory, episode):
        '''
        inputs:
        trajectory -> tuple: (obs, action, reward, next_obs, done)
        episode -> int: 

        Non-linear Greedy GQ update function
        '''
        self.mem.push(trajectory)
        self.update_count += 1

        if self.update_count >= self.config['maxmem']/2:
            self.update_count = 0
            all_dw1, all_dw2 = [],[]
            if self.tot_updates % 200 == 0:
                self.previous_policy = deepcopy(self.policy)
                # old_policy = deepcopy(self.policy)
                # 
            for _ in range(self.batch_size):
                self.tot_updates += 1

                # get samples from memory
                obs, action, reward, next_obs, done = self.mem.sample()
                obs = self.getFeatures(obs[0].tolist())
                next_obs = self.getFeatures(next_obs[0].tolist())
                next_act_vals = torch.unsqueeze(self.policy(next_obs),0)
                # print(f'next actvaals: {next_act_vals}')
                best_next_action_val, best_next_action = torch.max(next_act_vals, axis=1)
                # print(f'Best Next Action: {best_next_action}')
                phi_hat = self.get_phi(best_next_action_val[0])
                # print(f'Phi hat:\n {phi_hat}')
                act_vals = torch.unsqueeze(self.policy(obs),0)
                # print(f'actvals: {act_vals}')
                act_val = act_vals[0][action.item()]
                # print(f'act val: {act_val}')
                phi = self.get_phi(act_val)
                # print('\nphi\n')
                # print(phi)


                ys = reward + self.gamma * best_next_action_val  if not done else 0
                td_error = ys - act_val
                self.loss_tracker.append(td_error.item())
                self.TDError_tracker.append(td_error.item())
                
                # precompute products
                eta_dot_phi = self.param_mul(self.eta, phi)
                # print('\nETA dot Phi\n')
                # print(eta_dot_phi)
                hessian = self.compute_hessian(obs, action)
                # print('\nHessian:\n')
                # print(hessian)
                act_vals_previous = torch.unsqueeze(self.previous_policy(obs),0)
                # print('act_vals_prev: ', act_vals_previous)
                # print('action: ', action)
                act_val_prev = act_vals_previous[0][action.item()]
                # print('\nPrevious QSA:\n')
                # print(act_val_prev)
                # print('\nQSA\n')
                # print(act_val)
                d1, d2 = self.compute_dw(hessian, phi, phi_hat, eta_dot_phi, td_error, act_val, act_val_prev)
                # print('\nd2\n')
                # print(d2)
                all_dw1.append(d1)
                all_dw2.append(d2)

            ave_dw1, ave_dw2 = self.average_dws(all_dw1, all_dw2)
            if self.config['debug']:
                self.save_dw(ave_dw1, ave_dw2)
            # self.previous_policy = old_policy
            self.update_policy_params(ave_dw1, ave_dw2)
            

            # update eta
            self.update_eta(td_error, eta_dot_phi, phi)


    def __str__(self):
        if self.kappa == 0:
            return 'GreedyQNetwork'
        else:
            return 'DiffDiffQNetwork'


    def return_data(self):
        # process the observations to turn into a pd DF for graphing.
        # return a list of pd.Series for each obs. 
        # len(obs) = # of obs
        data_dict = dict()

        data_dict['Total Rewards'] =  pd.Series(self.total_rewards)
        data_dict['Actions Taken'] =  pd.Series(self.action_tracker)
        data_dict['Average reward'] = np.mean(self.total_rewards)
        data_dict['TDErrors'] =       pd.Series(self.TDError_tracker)
        data_dict['Self Object'] =    self
        data_dict['Epsilon vals'] =   pd.Series(self.epsilon_tracker)
        data_dict['loss_tracker'] =   pd.Series(self.loss_tracker)
        data_dict['Mean Weights']   = pd.Series(self.mean_weights)
        if self.config['debug']:
            data_dict['network params'] = self.weight_history
            data_dict['dw1 tracker'] = np.stack(self.track_dw1,axis=1)
            data_dict['dw2 tracker'] = np.stack(self.track_dw2, axis=1)
        return data_dict


    def prep_hess_func(self, _obs, _action_index, _names):
        def hess_func_(*params):
            # stateless removes the module inheritance for hessian to work. 
            # it requies named_parameters in case you only want a hessian with respect to some 
            out: torch.Tensor = _stateless.functional_call(self.policy, {n: p for n, p in zip(_names, params)}, _obs)
            # print('hess_prep out: ',out[0][_action_index])
            # print('\nHess out\n')
            # print(out)
            # return out[0][_action_index]
            return out[_action_index]
        return hess_func_

    def compute_hessian(self, obs, action_index):
        param_names = list(n for n,_ in self.policy.named_parameters())
        hess_func = self.prep_hess_func(obs, action_index, param_names)
        hessian_matrix = hessian(hess_func, tuple(self.policy.parameters()))
        # print('Action Index: ', action_index)
        return hessian_matrix


    def get_phi(self, q_val):
        # print('q val: ', q_val)
        q_val.backward() # calculates gradients, accumlates in the param.grad field
        derivatives = deepcopy([p.grad for p in self.policy.parameters()])
        # print('\nderivatives:\n')
        # print(derivatives)
        # manual zero gradients
        for p in self.policy.parameters():
            p.grad.data.zero_()
        # print('\nderivatives:\n')
        # print(derivatives)
        return derivatives

    def compute_dw(self, hessian, phi, phi_hat, etaxphi, td_e, qsa, qsa_wprev):
        with torch.no_grad():
            dw1_ = []
            dw2_ = []
            for p, h, e, ph, phh in zip(self.policy.parameters(), hessian, self.eta.parameters(), phi, phi_hat):
                ## use the approximation of outer gradient of products for the hessian
                # eta_vec = torch.flatten(e)
                # grad_vec = torch.flatten(ph)
                # approx_hessian = torch.outer(grad_vec, grad_vec)
                # h_sum = torch.reshape(torch.matmul(eta_vec, approx_hessian), ph.shape)

                h_sum = torch.zeros(e.shape)
                for h_i, e_i in zip(h, self.eta.parameters()):
                    if (len(h_i.shape) == 4 and len(e_i.shape) == 2) or (len(h_i.shape) == 3 and len(e_i.shape) == 1):
                        exh = torch.einsum('...,ac...->ac',e_i,h_i)
                        # print(exh.shape)
                    if len(h_i.shape) == 3 and len(e_i.shape) == 2:
                        exh = torch.einsum('...,a...->a',e_i,h_i)
                    if len(h_i.shape) ==2 and len(e_i.shape) == 1:
                        exh = torch.einsum('a,ca->c',e_i,h_i)
                    h_sum += exh
                # update the parameters
                dw1 = (etaxphi - td_e)*h_sum - (self.gamma*phh - ph)*etaxphi
                # print('\nPH\n')
                # print(ph)
                dw2 = ph * (qsa - qsa_wprev)
                # print('\ndw2\n')
                # print(dw2)
                dw1_.append(dw1)
                dw2_.append(dw2)
        # print('\ndw2\n')
        # print(dw2_)
        return dw1_, dw2_

    def average_dws(self, all_dw1, all_dw2):
        ave_d1, ave_d2 = [],[]
        with torch.no_grad():
            for i,j in zip(zip(*all_dw1), zip(*all_dw2)):
                dim = len(i[0].shape) -1
                ave_i = torch.mean(torch.stack(i, dim=dim), dim=dim)
                ave_j = torch.mean(torch.stack(j, dim=dim), dim=dim)
                ave_d1.append(ave_i)
                ave_d2.append(ave_j)
        return ave_d1, ave_d2

    def update_policy_params(self, dw1, dw2):
        with torch.no_grad():
            for p, d1, d2 in zip(self.policy.parameters(),dw1, dw2):
                # print('\nbefore update p:\n')
                # print(p)
                # print('\nupdaate piece:\n')
                update =  d1 - self.kappa_factor * d2
                # print(update)
                p.grad.data.copy_(update)
                # print('\nafter update p:\n')
                # print(p)
        self.optimizer.step()

    def sum_mat(self, a, b):
        with torch.no_grad():
            if len(a.shape) == len(b.shape) and len(a.shape)==1:
                return sum(a*b)
            else:
                return sum(sum(a*b))


    def param_mul(self, a,b:list):
        with torch.no_grad():
            c_sum = 0
            for ai, bi in zip(a.parameters(), b):
                c_sum += self.sum_mat(ai,bi)
        return c_sum


    def update_eta(self, td_e, etaxphi, phi):
        with torch.no_grad():
            for e, ph in zip(self.eta.parameters(), phi):
                e.copy_(e + self.beta * (td_e - etaxphi) * ph)

    def save_dw(self, dw1, dw2):
        dw1s = torch.cat([torch.flatten(d) for d in dw1])
        dw2s = torch.cat([torch.flatten(d) for d in dw2])
        self.track_dw1.append(dw1s)
        self.track_dw2.append(dw2s)





