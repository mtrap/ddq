'''
GTDD RL with Nerual Networks
Main Tune file
Author: Matthew Trappett
Date: May 09, 2022

Takes command line args:
agent: which agent to tune
# env: which python environemt to train on 1=MC, 2=CP, 3=Acrobot
eps: num episodes on which to tune; default 100
results will compare the highest average 
cpus: how many cpus to use; default 4

It saves outputs after each one
'''

from Agents.DQN import DQNAgent
from Agents.DGQN import GQNAgent
from utils.sendmail import sendmail
from utils.DataProcessing import process_results, tune_sub_process, process_tune_data, process_tune_data_k

import numpy as np
import concurrent.futures
import gym
import torch
from datetime import datetime
import plotly.graph_objs as go
import pandas as pd
import random
import socket
import os
import sys
import traceback
import pickle
import pprint as pp
from tqdm import tqdm
import ray
from scipy.signal import savgol_filter



@ray.remote
def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    print(f'Training agent: {agent}')
    return agent.train()

@ray.remote
def repeat_train(config, _agent):
    # for a number of repeats train the agent on the env
    result_ids_train = [trainWrapper.remote((config, _agent)) for _ in range(config['repeats'])]
    # as the agent finishes then 
    data = []
    while len(result_ids_train):
        done_ids_train, result_ids_train = ray.wait(result_ids_train)
        data += ray.get(done_ids_train)
    sub_results = tune_sub_process(config, data)
    return sub_results

def main(agent_str, env, numEpisodes, repeats, cpus):
    '''
    folder needs to have the agent+env+datetime
    '''
    numEpisodes = int(numEpisodes)
    repeats = int(repeats)
    cpus = int(cpus)

    print(f'Tune with \n \
            Agent: {agent_str} \n \
            env: {env} \n \
            nEpisodes: {nEpisodes} \n \
            repeats: {repeats} \n \
            cpus: {cpus}')

    # agents dict
    agent_dict = {'dqn': DQNAgent,'gq': GQNAgent, 'gdq': GQNAgent}
    AGENT = agent_dict[agent_str]

    # set up paths for saving graphs and agent
    # agent + env + DT
    date_string = '{:%B_%d_%Y_%H_%M_%S}'.format(datetime.now())
    env_string = env.replace('-', '_')
    FolderName = '_'.join(['Tune_', agent_str, env_string, date_string])

    # check if Results folder exits and create one if not
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)

    PARENT = ROOT_PATH + FolderName
    os.makedirs(PARENT)
    # GRAPHS_STR = 'Graphs/'
    # AGENTS_STR = 'Agents/'   
    # GRAPHSPATH = os.path.join(PARENT, GRAPHS_STR)
    # AGENTSPATH = os.path.join(PARENT, AGENTS_STR)
    # os.mkdir(GRAPHSPATH)
    # os.mkdir(AGENTSPATH)


    # master config dictionary
    config = {# for all agents
              'env':        env,
              'episodes':   int(numEpisodes),
              'repeats':    int(repeats),
              'cpus':       int(cpus),
              'alpha':      0.001,
              'h_size':     100,
              # 'h2_size':  10,
              'gamma':      0.99,
              'epsilon':    1,
              'debug':      False, # saves lots of graphs to print out.
              'tune':       True,
              # Linear Function Approximation values
              'numTiles':   10, # number of tiles per tiling
              'tileVecSize': 10, # number of tilings
              # dqn
              'c':          15, # zero means that a copy is made every time
              "force":      0.001, # originally 0.001 for discrete for MC
              'batch_size': 90, # dqn = 32
              'maxmem':     256, # dqn = 256

              # for Greedy-GQ/GTDD
              'beta':       0.00001,
              'kappa':      0,

              # paths
              'ParentPath': PARENT,
              'tune param': ['alpha','beta'] # default is learning rate
            }

    alpha_list = [1/(10**j) for j in range(1,5)] #  [0.001, 0.0001] #
    # c_list = [5,10,15,20,30,40,50]
    # h_size_list = [50, 64, 80, 100, 128, 200, 256]

    config['alpha_list'] = alpha_list
    # only for kappa
    kappa_list = [0.75, 1.0, 2.0, 4.0] # [0.23] # 
    force_list = [0.01, 0.1]
    if agent_str == 'gdq':
        config['kappa list'] = kappa_list
        config['tune param'].append('kappa')
        config['force list'] = force_list 
        config['tune param'].append('force')


    inputs = []
    for f in force_list:
        config['force'] = f
        for a in alpha_list:
            config['alpha'] = a
            for b in alpha_list:
                config['beta'] = b
                if agent_str == 'gdq':
                    for k in kappa_list:
                        config['kappa'] = k
                        inputs.append((config.copy(), AGENT))
                else:
                    inputs.append((config.copy(), AGENT))

    # start ray
    print(f'Tune with {len(inputs)} number of inputs')
    ray.init(num_cpus=cpus)
    inputs_ids = [repeat_train.remote(i[0], i[1]) for i in inputs]
    tune_data = []
    while len(inputs_ids):
        done_inputs_ids, inputs_ids = ray.wait(inputs_ids)
        tune_data += ray.get(done_inputs_ids)
    # stop ray
    ray.shutdown()


    # if agent_str == 'gdq':
    #     tune_data, best_params = process_tune_data_k(config, tune_data)
    # else:
    #     tune_data, best_params = process_tune_data(config, tune_data)
    tune_data, best_params = process_tune_data(config, tune_data)


    # save config to know our parameters
    with open(PARENT + '/params.txt', 'w') as f:
        prnt = pp.PrettyPrinter(stream=f)
        prnt.pprint(config)

    with open(PARENT + '/tune_data.txt', 'w') as f:
        prnt = pp.PrettyPrinter(stream=f)
        prnt.pprint(tune_data) 

    # # save data for future processing if we want.
    # with open(PARENT + '/tune_data.txt', 'wb') as f:
    #     pickle.dump(tune_data, f)

    # send email to signal done
    sendmail('mtrappett@icloud.com',  env + ' Non-Linear has finished',
        'Tune finished on ' + socket.gethostname() + '\n\nBest value: ' + str(best_params) + '\n\n'
        '\n\nRun started on ' + 
        date_string + '\n\nCommandline script to download from orthus:\n\nscp -r MTrappett@orthus.nic.uoregon.edu:'+PARENT+
         ' ~/Downloads/NonLinearResults')



if __name__=='__main__':
    assert len(sys.argv) >= 4, 'Need arguments: agent, env, numEpisodes'
    agent = sys.argv[1]
    env = sys.argv[2]
    nEpisodes = sys.argv[3]

    env_dict = {'mc':'MountainCar-v0' ,'cp':'CartPole-v0' ,'ab':'Acrobot-v1' }
    # optional repeat paramter, for production graph will want a lot.
    repeats = 5
    cpus = 4
    if len(sys.argv) > 4:
        repeats = sys.argv[4]
    if len(sys.argv) == 6:
        cpus = sys.argv[5]

    try:
        main(agent, env_dict[env], nEpisodes, repeats, cpus)

        print('Finished')
    except:
        # send email to signal done
        sendmail('mtrappett@icloud.com',  env + "threw an error",
            'error on ' + socket.gethostname() + 
            'error ' + 
            str(traceback.format_exc(limit=None, chain=True)))

