# TestAgents.py
from TileFAAlgorithms import GreedyGQ, GDQ, QAgent, RandomAgent

import pickle
import glob

import gym
import numpy
import torch
import os
import sys

from pyvirtualdisplay import Display



from gym.wrappers.monitoring.video_recorder import VideoRecorder
before_training = "before_training.mp4"



def test_agent(env):
    display = Display(visible=False, size=(1400, 900))
    _ = display.start()

    videoNameString = "practiceMC.mp4"
    video = VideoRecorder(env, before_training)
    
    obs = env.reset()
    env.render()
    video.capture_frame()
    # while True:
    for _ in range(200)
        # epsilon = 0 since for no exploring
        # action = agent.chooseAction(0, obs)
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        env.render()
        video.capture_frame()
        # if done:
        #     break

    return



def main(agent):
    test_agent(gym.make('MountainCar-v0'))
    # # get the .agent files
    # AGENTS_PATH = os.getcwd() + '/*.agent' 
    # trainedAgentFiles = glob.glob(AGENTS_PATH)

    # # use pickle to recreate trained agents
    # agents = []

    # for agent_file in trainedAgentFiles:
    #     with open(agent_file, 'rb') as af:
    #         agents.append(pickle.load(af))

    # agent_names = list(map(str, agents))
    # for i in range(len(agent_names)):
    #     if agent in agent_names[i]:
    #         test_agent(agents[i], gym.make('MountainCar-v0'))
    #         break
    # else:
    #     print('No Agent Found')





if __name__=='__main__':
    main():


