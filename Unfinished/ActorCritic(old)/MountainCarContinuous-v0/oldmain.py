## main.py
import numpy as np
from multiprocessing import Pool
import gym
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys

# Get Agents
import agents.RandomAgent as RA
import agents.DAC as DAC

def run_training(agent, config):
	env = gym.make(config['env'])
	agent = agent(env, config)
	rewards = []
	for episode in range(config['episodes']):
		obs = env.reset()
		cumulative_reward = 0
		while True:
			action = agent.chooseAction(obs)
			# print("Action: ", action)
			new_obs, reward, done, _ = env.step([action])

			agent.update({"s":obs, "a":action, "r": reward, "s_prime": new_obs}, episode)
			obs = new_obs
			cumulative_reward += reward

			if done:
				# print('Done!')
				break

		rewards.append(cumulative_reward)
	print("Complete")
	return rewards





if __name__ == '__main__':
	ENV_s = "MountainCarContinuous-v0"
	config = {"episodes": 500,
				"env": ENV_s}
	# random_rewards = run_training(RA.RandomAgent, config)
	# dac_rewards = run_training(DAC.DeterministicActorCritic, config)
	copdac_q_rewards = run_training(DAC.copdac_q, config)
	px.line(copdac_q_rewards)