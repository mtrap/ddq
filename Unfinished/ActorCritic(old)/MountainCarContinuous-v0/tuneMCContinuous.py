# -*- coding: utf-8 -*-
# Get Agents
from agents.DAC import DAC, RandomAgent, copdac_q
from utils.sendmail import sendmail

import numpy as np
import gym
import torch
import datetime
import plotly.express as px
import plotly.graph_objects as go
import matplotlib
import os
import random
import datetime
import socket
import traceback
import shutil
import pandas as pd

import ray
from ray import tune
from ray.tune.schedulers import ASHAScheduler


def save_plot(analysis, path, graphname, config):
    df_results = analysis.results_df
    df_trials = analysis.trial_dataframes
    df_analysis = analysis.dataframe()

    columns = [col for col in df_analysis if 'config' in col]

    figs = []
    for col in columns:
        col = col.replace('/', '.')
        fig = px.scatter(df_results, x=col, y="rewards", log_x=True)

        fig.update_xaxes(title_text=col)
        fig.update_yaxes(title_text="Total Reward")

        fig.update_layout(height=500, width=900, title_text="Episode Reward vs " + col)
        figs.append(fig)

    configs = [col for col in df_analysis if 'config' in col]

    fig = go.Figure()

    for key in df_trials.keys():
        d = df_trials[key]

        hps = [pair for s in key.split('_') if 'alpha' in s for pair in s.split(',')]
        string = '' 
        for s in hps:
            string += s + '<br>' 
        fig.add_trace(go.Scatter(y=d['rewards'], x=d.index,
                                 hovertemplate='<i>Reward</i>: %{y:.2f}'+
                                                '<br>Epoch: %{x}<br>'+
                                                '%{text}', 
                                 text = [string for _ in d.index] 
                                 ))
    fig.update_layout(title_text="Rewards per Epoch")
    figs.append(fig)
    # Save figs to file
    with open(path + graphname + '.html', 'a') as f:
        for fig in figs:
            f.write(fig.to_html(full_html=False, include_plotlyjs='cdn'))

def combined_plot(analyses, path, dt, config):
    figs = []
    for analysis in analyses:
        df_results = analysis.results_df
        df_trials = analysis.trial_dataframes
        df_analysis = analysis.dataframe()

        columns = [col for col in df_analysis if 'config' in col]

        fig = go.Figure()
        for key in df_trials.keys():
            d = df_trials[key]

            hps = [pair for s in key.split('_') if 'alpha' in s for pair in s.split(',')]

            string = '' 
            for s in hps:
                string += s + '<br>' 
            fig.add_trace(go.Scatter(y=d['rewards'], x=d.index,
                                 hovertemplate='<i>Reward</i>: %{y:.2f}'+
                                                '<br>Epoch: %{x}<br>'+
                                                '%{text}', 
                                 text = [string for _ in d.index] 
                                 ))
        fig.update_layout(title_text="Rewards per Epoch")
        figs.append(fig)

    # Save figs to file
    with open(path + '/Combinedplots.html', 'a') as f:
        for fig in figs:
            f.write(fig.to_html(full_html=False, include_plotlyjs='cdn'))
            
    return path + '/Combinedplots.html'
        
def wrap_training(outside_agent):
    inside_agent = outside_agent 
    def train_agent(config):
        env = gym.make(config['env'])


        # Create agent
        agent = inside_agent(config)
        for episode in range(config['episodes']):
            obs = env.reset()
            rewards = 0
            steps = 0
            try:
                while True:
                    # take step and get new obs
                    action = agent.chooseAction(obs)
                    next_obs, reward, done, info = env.step([action])

                    # Udate weights 
                    agent.update({"s":obs, "a":action, "r": reward, "s_prime": next_obs}, episode)
                    
                    # increment trackers
                    steps += 1
                    rewards += reward

                    # update obs
                    obs = next_obs
                    if config['debug']:
                        print(agent)
                        print('Action: ', action)
                        print('steps: ', steps)
                    if  done or agent.anyNan():  
                        break
            except ValueError:
                print('Divergence detected and learning failed.')


            tune.report(rewards=rewards, steps=steps)
        env.close()
    return train_agent


def main():
    try:
        device = socket.gethostname()
        ENV =  "MountainCarContinuous-v0"
        env_string = ENV.replace('-','_')
        NUM_SAMPLES = 1

        # Learning rate for our term
        KAPPA = 2

        # number of tiliings to create for Linear Function Approximation
        NUM_TILES = 10



        if device == 'MT' or device == 'talapas-ln2':
            WORKERS = 6  
            EPISODES = 1
            EXPONENTS = [i for i in range(1, 2, 5)]
            CONSTANTS = [1]
            
        else:
            WORKERS = 224 # for talapas
            EPISODES = 10000
            # Searching along C/10^E
            # Exponent
            EXPONENTS = [i for i in range(0, 8)]
            # Base
            CONSTANTS = [1,5]


        # Where to save results.    
        PARENT = os.getcwd() + '/Results'

        if not os.path.isdir(PARENT):
            os.makedirs(PARENT)
        DT = str(datetime.datetime.now()).replace(' ','_').replace(':','_')
        FOLDER = 'TuneResults_' + DT
        PATH = os.path.join(PARENT, FOLDER)
        os.mkdir(PATH)

        
        dw_graph_path = PATH + '/deltaWgraph.html'
        # For time tune only DAC
        agents = [(DAC, 'DAC')] #, (RandomAgent, 'Random Agent'), (copdac_q, 'COPDAC_Q')]
        
        search_space = {
                        "agent": None,
                        "env": ENV,
                        "at": tune.grid_search([const*1/10**exp for const in CONSTANTS for exp in EXPONENTS]),
                        "aw": tune.grid_search([const*1/10**exp for const in CONSTANTS for exp in EXPONENTS]),
                        "av": tune.grid_search([const*1/10**exp for const in CONSTANTS for exp in EXPONENTS]),
                        "au": tune.grid_search([const*1/10**exp for const in CONSTANTS for exp in EXPONENTS]),
                        "kappa": KAPPA,
                        "episodes": EPISODES,
                        "debug": False,
                        "dw_graph_path": dw_graph_path,
                        "samples": NUM_SAMPLES,
                        }




        # start ray for multiprocessing
        ray.init(num_cpus=WORKERS)

        asha_scheduler = ASHAScheduler(
        time_attr='training_iteration',
        metric='rewards',
        mode='max',
        max_t=100,
        grace_period=10,
        reduction_factor=3,
        brackets=1)      

        # run ray tune for each agent
        analyses = []
        Times = []
        total_start_time = datetime.datetime.now()
        start_time = total_start_time
        for agent in agents:
            start_time = datetime.datetime.now()
            train_agent = wrap_training(agent[0])
            search_space['agent'] = agent[1]
            analysis = tune.run(train_agent,
                                num_samples=search_space['samples'],
                                # scheduler=asha_scheduler,
                                config=search_space,
                                metric='rewards',
                                mode='max',
                                resources_per_trial={"cpu":1},
                                verbose=1,)

            end_time = datetime.datetime.now()
            Times.append((agent, end_time - start_time))
            analyses.append(analysis)
            save_plot(analysis, PATH, '/'+str(agent)+'TuneResults'+ DT, search_space)


        ray.shutdown()
        end_time = datetime.datetime.now()
        total_time = end_time - total_start_time
        # delta w's
        # print('weights???: ', search_space['delta_w'].weights)
        # graph_delta_weights(PATH, search_space['delta_w'], search_space_gdq['delta_w'])

        html_path = combined_plot(analyses, PATH, DT, search_space)


        # Plot by epoch
        ax = None  # This plots everything on the same plot
        for analysis in analyses:
            for d in analysis.trial_dataframes.values():
                ax = d.rewards.plot(ax=ax, legend=False)
        ax.set_xlabel('Epochs')
        ax.set_ylabel('Reward')


        ax.figure.savefig(PATH + '/'+env_string+DT+'.png')

        time_string = ''
        for time in Times:
            time_string += '\n{} Execution Time: {} '.format(time[0], time[1])
        with open(PATH + '/Timing'+env_string+DT+'.txt','w+') as f:
            f.write('Using {} num of CPUS'.format(WORKERS + 1)  +
                    time_string +
                    '\nTotal time: {}'.format(total_time))
            for analysis in analyses:
                f.write("\n\nBest hyperparameters found were: {}".format(analysis.best_config))

        

        # zip results folder using the PATH
        output_path = PATH
        # shutil.make_archive(output_path, 'zip', PATH)

        sendmail('mtrappett@icloud.com', env_string + ' has Successfully finished', 'Success!!!! Running on ' + device + '\n\nRun started on ' + DT)
    except:
        device = socket.gethostname()
        error = traceback.format_exc()
        print(error)
        sendmail('mtrappett@icloud.com', env_string + ' threw an error', 'Running on ' + device + '\n\n' + error + '\n\nRun started on ' + DT)
        


if __name__=='__main__':
    main()








