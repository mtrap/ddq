'''
Author: Matthew Trappett
main file for Deterministic Actor Critic Learning
'''

# Get Agents
from agents.DAC import DAC, RandomAgent, copdac_q
from utils.sendmail import sendmail

import numpy as np
import concurrent.futures
import gym
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
import ray

def save_plot(df, path, plotname, config, yaxis, xaxis='Episodes'):
    pd.options.plotting.backend = "plotly"
    fig = df.plot()
    fig.update_xaxes(title_text=xaxis)
    fig.update_yaxes(title_text=yaxis)
    fig.update_layout(height=500, width=900, title_text=config['env'] + " <br> Repeated "+ str(config['repeats']) +"x" +" <br> agent_at_aw_av_au")
    fig.write_html(path + plotname +'.html')


def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    return agent.train()

@ray.remote
def train(config, AGENT):
    print("Training: ", str(AGENT), f'config: {config}')

    data = trainWrapper((config, AGENT))

    EP_LEN_DFSS = pd.Series(data[1])
    DFSS = pd.Series(data[0])
    return {'reward': DFSS, 
            'ep_len':EP_LEN_DFSS, 
            'name': str(AGENT) + ' at: ' + str(config["at"]) + ', aw ' + str(config["aw"])+', av '+str(config['av'])+', au ' + str(config['au']) + ', pow: ' + str(config['power']),
            'end_reward': data[2],
            'xpos': data[3]}
      
        
        
def main(eps=5, repeats=1, cpus=4, slice_num=0):
    repeats=1
    
    # RL Training Variables
    ENV =  "MountainCarContinuous-v0"
    # For DAC their are many learning rates
    AT_list = [0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001] # 
    # AT_list = [0.1, 0.01]
    AT = 1.0
    AW_list = [0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001] # 
    # AW_list = [0.1, 0.01]
    AW = 0.000001
    AV_list = [0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001] # 
    # AV_list = [0.1, 0.01]
    AV = 0.0005
    # alpha_u is the learning rate for second weight vector
    # with DAC it has been reasonable for fast divergence
    AU_list = [0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001, 0.00000001] # 
    # AU_list = [0.01, 0.0001]
    AU = 0.0000000001
    # altering the power of the car to get it working
    # original was 0.0015
    # POWER = [0.002, 0.003, 0.004]
    # Learning rate for our term
    KAPPA = 2

    # number of tiliings to create for Linear Function Approximation
    NUM_TILES = 10
    # How long to train our agents, mc tends do need around 600
    EPISODES = eps
    
    # the number of repeats is the number of subthreads spawned. 
    REPEATS = repeats
    # print out weights from DAC
    DEBUG_W = False



    # Create a parent path in results to store a results_dt file 
    # the results_dt file will have a graphs folder, agents folder, and txt file with details of training
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)


    DT = datetime.datetime.now().strftime("%B_%d_%Y_%H_%M_%S")
    PARENT = ROOT_PATH + 'Results_' + str(slice_num) + DT
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'   
    RESULTS_PATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTS_PATH = os.path.join(PARENT, AGENTS_STR)
    os.mkdir(RESULTS_PATH)
    os.mkdir(AGENTS_PATH)

    # text file to write details of the run
    DOC_STRING = RESULTS_PATH + '/params.txt'


    # config dictionaries to pass to objects
    config = {
        "env": ENV,
        "at": AT,
        "aw": AW,
        "av": AV,
        "au": AU,
        "kappa": KAPPA,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "debug": DEBUG_W,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH,
        "tune": True,
        "power": 0.0015
    }


    inputs = [(config, DAC)]

    for at in AT_list:
        for aw in AW_list:
            for av in AV_list:
                config['at'] = at
                config['aw'] = aw
                config['av'] = av # removed loop over au for copdac_q
                inputs.append((config.copy(), copdac_q))

    # process in batches of 224
    # determine how many batches
    n_jobs = len(inputs)
    nodes = 8
    jobs_per_node = int(np.ceil(n_jobs/nodes))
    

    #start ray
    ray.init(num_cpus=cpus)

    # catch results
    finished = []

    if slice_num > 7:
        result_ids = [train.remote(pair[0], pair[1]) for pair in inputs[slice_num*jobs_per_node:(slice_num+1)*jobs_per_node]]
        while len(result_ids):
            done_ids, result_ids = ray.wait(result_ids)
            finished += ray.get(done_ids)

    # when slice_num = 7
    else:
        # run the leftovers
        result_ids = [train.remote(pair[0], pair[1]) for pair in inputs[slice_num*jobs_per_node:]]
        while len(result_ids):
            done_ids, result_ids = ray.wait(result_ids)
            finished += ray.get(done_ids)

    ray.shutdown()

    # process results
    rewards_dict = dict()
    len_dict = dict()
    xpos_dict =dict()
    averageRewards = []
    averageSteps = []
    for res in finished:
        xpos_dict[res['name']] = res['xpos']
        rewards_dict[res['name']]=res['reward']
        len_dict[res['name']]=res['ep_len']
        averageRewards.append((res['name'], np.mean(res['reward'])))
        averageSteps.append((res['name'], np.mean(res['ep_len'])))
    

    # get the top 10 names based on mean rewards and ep length
    averageRewards.sort(key=lambda x: x[1])
    averageSteps.sort(key=lambda x: x[1])
    top_ten = [pair[0] for ave in [averageRewards, averageSteps] for pair in ave[:5]]

    rewards_df = pd.DataFrame({key:rewards_dict[key] for key in top_ten})
    steps_df = pd.DataFrame({key:len_dict[key] for key in top_ten})
    xpos_df = pd.DataFrame({key:pd.Series(xpos_dict[key]) for key in top_ten})

    save_plot(rewards_df, RESULTS_PATH, '/MeanRewards' + str(REPEATS) + DT, config, 'Total Rewards')
    save_plot(steps_df, RESULTS_PATH, '/MeanStepsPerEpisode' + str(REPEATS) + DT, config,'Total Steps')
    save_plot(xpos_df, RESULTS_PATH, '/xpos' + DT, config,'X position of car', 'Steps')
    


    with open(DOC_STRING, 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(DOC_STRING, 'w') )
        prnt.pprint(config)
        prnt.pprint(rewards_df)


    print("Finished.")
    sendmail('mtrappett@icloud.com', ENV + ' has finished','All done on ' 
        + socket.gethostname() + ' comput slice '+ str(slice_num) + '\n' + str(config) + 
        '\n' +  '\n\n' 
        + 'Top Average combinations:' + str(top_ten) + '\n\n' +
        'list of all average rewards: ' + '\n\n'
        + str(averageRewards)+ '\n\n' +
        'list of all average steps: ' + '\n\n'
        + str(averageSteps))

    
    
if __name__=='__main__':
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 2:
        # how many episodes
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        # episodes and repeats
        main(int(sys.argv[1]), int(sys.argv[2]))
    elif len(sys.argv) == 4:
        # episodes and repeats, cpus
        main(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
    elif len(sys.argv) == 5:
        # episodes and repeats, cpus
        main(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]))




