# TestAgents.py
# from TileFAAlgorithms import GreedyGQ, GDQ, QAgent, RandomAgent

import pickle
import glob
import datetime

import gym
import numpy
import torch
import os
import sys

# For recording
from pyvirtualdisplay import Display
from gym.wrappers.monitoring.video_recorder import VideoRecorder

def take_action(agent, obs):
    features = agent.getFeatures(obs)
    a = torch.matmul(agent.theta, features)
    if a < -1:
        a = -1
    elif a > 1:
        a = 0.99999999

    return a



def test_agent(folder, agent):
    display = Display(visible=False, size=(1400, 900))
    _ = display.start()

    env = gym.make('MountainCarContinuous-v0')

    videoNameString = folder + '/' + str(agent) + "AC_MCC.mp4"
    video = VideoRecorder(env, videoNameString)
    for _ in range(3):
        obs = env.reset()
        env.render()
        video.capture_frame()
        # while True:
        while True:
            action = take_action(agent, obs)
            # action = env.action_space.sample()
            obs, reward, done, info = env.step([action])
            env.render()
            video.capture_frame()
            if done:
                break
    video.close()
    env.close()
    return



def main(paths_to_agents):
    # check if parent Agents folder exists or make one
    VID_PATH = os.getcwd() + '/Videos'
    if not os.path.isdir(VID_PATH):
        os.makedirs(VID_PATH)



    for path in paths_to_agents: 

        # use pickle to recreate trained agents
        with open(path, 'rb') as af:
            agent = pickle.load(af)

            test_agent(VID_PATH,  agent)


if __name__=='__main__':
    main(sys.argv[1:])
    # path to agents


