'''
Author: Matthew Trappett
main file for Deterministic Actor Critic Learning
'''

# Get Agents
from agents.DAC import DAC, RandomAgent, copdac_q
from utils.sendmail import sendmail

import numpy as np
import concurrent.futures
import gym
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
import ray
from scipy.signal import savgol_filter

def save_plot(df, path, plotname, config, yaxis):
    pd.options.plotting.backend = "plotly"
    fig = df.plot()
    fig.update_xaxes(title_text="Episodes")
    fig.update_yaxes(title_text=yaxis)
    fig.update_layout(height=500, width=900, title_text=config['env'] + " \n Repeated "+ str(config['repeats']) +"x" +" <br/> \n agent_at_aw_av_au")
    fig.write_html(path + plotname +'.html')

@ray.remote
def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    return agent.train()

@ray.remote
def train(config, AGENT):
    # python parallel with multiple threads to speed it up 
    # Threads were faster than processes in practice.
    print("Training: ", str(AGENT), f'config: {config}')

    # don't know if I can use thread with ray
    # with concurrent.futures.ThreadPoolExecutor(max_workers=config['repeats']) as executor:
    #     data = executor.map(trainWrapper, ((config, AGENT),)*config['repeats'])
    result_ids_train = [trainWrapper.remote((config, AGENT)) for _ in range(config['repeats'])]

    data = []
    while len(result_ids_train):
        done_ids_train, result_ids_train = ray.wait(result_ids_train)
        data += ray.get(done_ids_train)

    R_DFS = [] # store the rewards for eac
    EP_LEN_DFS = [] # store the lengths of episodes
    agents = []
    # end_rewards = []
    for d in data:
        # print('d from data: ', d)
        R_DFS.append(d[0])
        EP_LEN_DFS.append(d[1])
        agents.append(d[2])
    #     end_rewards.append(d[2])
    # sort agents
    def sort_func(a):
        return a.get_agent_performance()[0]

    agents.sort(reverse=True, key=sort_func)
    # save the first one.
    agents[0].save()

    # # print(EP_LEN_DFS)
    # EP_LEN_DFSS = pd.Series(EP_LEN_DFS)
    # DFSS = pd.Series(R_DFS)
    EP_LEN_DFSS = pd.concat(EP_LEN_DFS, axis=1)
    DFSS = pd.concat(R_DFS, axis=1)
    mean_rewards = DFSS.mean(axis=1)
    win_len = 17
    poly_order = 3
    if len(mean_rewards) < win_len:
        win_len = 3
        poly_order = 1
    smoothed_mean_rewards = pd.Series(savgol_filter(mean_rewards, win_len, poly_order))
    mean_eps = EP_LEN_DFSS.mean(axis=1)
    smoothed_mean_eps = pd.Series(savgol_filter(mean_eps, win_len, poly_order))
    return {'reward': smoothed_mean_rewards, 
            'ep_len': smoothed_mean_eps, 
            'name': str(AGENT) + ' at: ' +str(config["at"])+', aw '+str(config["aw"])+', av '+str(config['av'])+', au ' + str(config['au']),
            'all_metrics': {'rewards': DFSS, 'steps': EP_LEN_DFSS},
            } 

        
def main(eps=2, repeats=2, cpus=4):
    
    # RL Training Variables
    ENV =  "MountainCarContinuous-v0"
    # For DAC their are many learning rates
    AT = 0.0001
    AW = 0.0001
    AV = 0.1
    # alpha_u is the learning rate for second weight vector
    # with DAC it has been responsible for fast divergence
    AU = 0.01
    # Learning rate for our term
    KAPPA = 2

    # number of tiliings to create for Linear Function Approximation
    NUM_TILES = 10
    # How long to train our agents, mc tends do need around 600
    EPISODES = eps
    
    # the number of repeats is the number of subthreads spawned. 
    REPEATS = repeats
    # print out weights from DAC
    DEBUG_W = False



    # Create a parent path in results to store a results_dt file 
    # the results_dt file will have a graphs folder, agents folder, and txt file with details of training
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)


    DT = datetime.datetime.now().strftime("%b_%d_%Y_%H_%M_%S")
    PARENT = ROOT_PATH + 'Results_' + DT
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'   
    RESULTS_PATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTS_PATH = os.path.join(PARENT, AGENTS_STR)
    os.mkdir(RESULTS_PATH)
    os.mkdir(AGENTS_PATH)

    # text file to write details of the run
    DOC_STRING = RESULTS_PATH + '/params.txt'


    # config dictionaries to pass to objects
    config = {
        "env": ENV,
        "at": AT,
        "aw": AW,
        "av": AV,
        "au": AU,
        "kappa": KAPPA,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "debug": DEBUG_W,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH,
        "tune": False,
        "power": 0.0015
    }

    # no au for copdac
    inputs = [(config, copdac_q),(config, DAC), (config, RandomAgent)] # , (config, copdac_q)


    ray.init(num_cpus=cpus)
    result_ids = [train.remote(pair[0], pair[1]) for pair in inputs]
    futures = ray.get(result_ids)
    ray.shutdown()

    rewards_dict = dict()
    len_dict = dict()
    for res in futures:
        rewards_dict[res['name']]=res['reward']
        len_dict[res['name']]=res['ep_len']

         


    rewards_df = pd.DataFrame(rewards_dict)
    steps_df = pd.DataFrame(len_dict)

    save_plot(rewards_df, RESULTS_PATH, '/MeanRewards' + str(REPEATS) + DT, config, 'Total Rewards')
    save_plot(steps_df, RESULTS_PATH, '/MeanStepsPerEpisode' + str(REPEATS) + DT, config,'Total Steps')


    with open(DOC_STRING, 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(DOC_STRING, 'w') )
        prnt.pprint(config)


    print("Finished.")
    sendmail('mtrappett@icloud.com', ENV + ' has finished','All done on ' 
                                        + socket.gethostname() + '\n' 
                                        + str(config) + str(os.getcwd()))

    
    
if __name__=='__main__':
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 2:
        # how many episodes
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        # episodes and repeats
        main(int(sys.argv[1]), int(sys.argv[2]))
    elif len(sys.argv) == 4:
        # episodes and repeats, cpus
        main(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))


