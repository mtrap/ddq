from TileFAAlgorithms import GreedyGQ, GDQ
from TileFAAlgorithmsJax import GreedyGQJ, GDQJ
from TileFAAlgorithmsNumba import GreedyGQNB, GDQNB
from TileFAAlgorithmsNumpy import GreedyGQNP, GDQNP
from TileFAAlgorithmsTDot import GreedyGQTdot, GDQTdot
from TileFAAlgorithmsMatmul import  GDQM
from sendmail import sendmail

import numpy as np
import concurrent.futures
import gym
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
import time

def save_plot(df, path, plotname, config, yaxis, xaxis="Episodes"):
    pd.options.plotting.backend = "plotly"
    fig = df.plot()
    fig.update_xaxes(title_text=xaxis)
    fig.update_yaxes(title_text=yaxis)
    fig.update_layout(height=500, width=900, title_text=config['env'] + " \n Repeated "+ str(config['repeats']) +"x")
    fig.write_html(path + plotname +'.html')


def run_training(args):
    config, AGENT = args[0], args[1]
    env = gym.make(config['env'])
    num_states = len(env.observation_space.sample())    
    num_actions = env.action_space.n
    num_episodes = config["episodes"]
    epsilon = config["epsilon"]
    total_time = np.zeros(num_episodes)
    total_rewards = np.zeros(num_episodes)
    episode_lengths = np.zeros(num_episodes)

    # Create agent
    agent = AGENT(env, num_states, num_actions, config['alpha'], 
                config['beta'], config['gamma'], config['kappa'], 
                config['lambda'], config['numTiles'], 
                # config['array length'],
                )

    for episode in range(num_episodes):
        tic = time.perf_counter()
        obs = env.reset()
        rewards = 0
        agent.trace_reset()
        count = 0

        while True:
            count += 1
            new_action = agent.chooseAction(epsilon, obs)
            next_obs, reward, done, info = env.step(new_action)

            # Udate weights 
            agent.update(reward, obs, new_action, next_obs, episode, epsilon)

            rewards += reward
            obs = next_obs
            if  done:  
                break
        toc = time.perf_counter()
        total_time[episode] = toc - tic
        total_rewards[episode] = rewards
        episode_lengths[episode] = count

    #Export trained agent to a file to later get for testing.
    agent_file = '/' + str(agent) + '.agent'

    
    with open(config['agents path'] + agent_file, 'wb') as af:
        # save agent state
        pickle.dump(agent, af)

    return (pd.Series(total_rewards), pd.Series(episode_lengths), str(agent), pd.Series(np.trim_zeros(agent.update_times)), pd.Series(total_time))



def repeat_training(config, AGENT ):
    # python parallel with multiple threads to speed it up 
    # Threads were faster than processes in practice.
    print("Training: ", str(AGENT))

    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        data = executor.map(run_training, ((config, AGENT),)*config['repeats'])

    R_DFS = [] # store the rewards for eac
    EP_LEN_DFS = [] # store the lengths of episodes
    time_data = []
    total_time = []
    for d in data:
        R_DFS.append(d[0])
        EP_LEN_DFS.append(d[1])
        name = d[2]
        time_data.append(d[3])
        total_time.append(d[4])

    EP_LEN_DFSS = pd.concat(EP_LEN_DFS, axis=1)
    DFSS = pd.concat(R_DFS, axis=1)
    all_times = pd.concat(time_data, axis=1)
    total_times = pd.concat(total_time, axis=1)

    return {'reward': DFSS.mean(axis=1), 
            'ep_len':EP_LEN_DFSS.mean(axis=1), 
            'name': name, 'times': all_times.mean(axis=1),
            'total times': total_times.mean(axis=1)}
      
        
        
def main(eps=10, repeats=4):
    
    # RL Training Variables

    ENV =  "MountainCar-v0"
    # Primary learning rate
    ALPHA = .1/11
    # Secondary learning rate
    BETA = .0001/11
    # Probability of taking a random action
    EPSILON = .1
    # λ is the eligibility trace parameter. λ=0 is off.
    LAMBDA = 0
    # Discount Factor
    GAMMA = .99
    # Learning rate for our term
    KAPPA = 2

    # number of tiliings to create for Linear Function Approximation
    NUM_TILES = 10
    ARRAY_LENGTH = 10**6
    # How long to train our agents, mc tends do need around 600
    EPISODES = eps
    
    # Num of cpu processors for running repeats in parallel
    NUM_PROC = 4
    REPEATS = repeats


    # Create a parent path in results to store a results_dt file 
    # the results_dt file will have a graphs folder, agents folder, and txt file with details of training
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)


    DT = datetime.datetime.now().strftime("%b_%d_%Y_%H_%M_%S")
    PARENT = ROOT_PATH + 'Results_' + DT
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'   
    RESULTS_PATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTS_PATH = os.path.join(PARENT, AGENTS_STR)
    os.mkdir(RESULTS_PATH)
    os.mkdir(AGENTS_PATH)

    # text file to write details of the run
    DOC_STRING = PARENT + '/params.txt'


    # config dictionaries to pass to objects
    config_greedy = {
        "env": ENV,
        "alpha": ALPHA,
        "beta": BETA,
        "numTiles": NUM_TILES,
        "kappa": 0,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "lambda": LAMBDA,
        "epsilon": EPSILON,
        "gamma": GAMMA,
        "numProcesses": NUM_PROC,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH,
        "array length": ARRAY_LENGTH
    }

    config_gdq = {
        "env": ENV,
        "alpha": ALPHA,
        "beta": BETA,
        "kappa": KAPPA,
        "numTiles": NUM_TILES,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "epsilon": EPSILON,
        "lambda": LAMBDA,
        "gamma": GAMMA,
        "numProcesses": NUM_PROC,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH,
        "array length": ARRAY_LENGTH
    }

    config_qagent = {
        "env": ENV,
        "alpha": ALPHA, #0.7,
        "beta": BETA, #0.001,
        "kappa": KAPPA,
        "numTiles": NUM_TILES,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "epsilon": EPSILON,
        "lambda": LAMBDA,
        "gamma": GAMMA,
        "numProcesses": NUM_PROC,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH,
        "array length": ARRAY_LENGTH
    }



    inputs = [(config_gdq, GDQ),
                (config_gdq, GDQJ),
                (config_gdq, GDQNB),
                (config_gdq, GDQNP),
                (config_gdq, GDQTdot),
                (config_gdq, GDQM),
                ]

    # with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    #     futures = executor.map(repeat_training, *zip(*inputs))

    results = []
    for pair in inputs:
        results.append(repeat_training(pair[0], pair[1]))

    rewards_dict = dict()
    len_dict = dict()
    timers = dict()
    total_timers = dict()
    for res in results:
        rewards_dict[res['name']]=res['reward']
        len_dict[res['name']]=res['ep_len']
        timers[res['name']]=res['times']
        total_timers[res['name']] = res['total times']

         

    timers_df = pd.DataFrame(timers)
    rewards_df = pd.DataFrame(rewards_dict)
    steps_df = pd.DataFrame(len_dict)
    total_timers_df = pd.DataFrame(total_timers)

    save_plot(rewards_df, RESULTS_PATH, '/MeanRewards' + str(REPEATS) + DT, config_gdq, 'Total Rewards')
    save_plot(steps_df, RESULTS_PATH, '/MeanStepsPerEpisode' + str(REPEATS) + DT, config_gdq,'Total Steps')
    save_plot(timers_df, RESULTS_PATH, '/times' + DT, config_gdq,'Time (s)', 'Steps')
    save_plot(total_timers_df, RESULTS_PATH, '/totaltimes' + DT, config_gdq,'Time (s)',)


    with open(DOC_STRING, 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(DOC_STRING, 'w') )
        prnt.pprint(config_gdq)



    print("Finished.")
    sendmail('mtrappett@icloud.com', ENV + ' has finished','All done on ' + socket.gethostname() + '\n\nRun started on ' + DT)

    
    
if __name__=='__main__':
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 2:
        # how many episodes
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        # episodes and repeats
        main(int(sys.argv[1]), int(sys.argv[2]))





