# TestAgents.py
# from TileFAAlgorithms import GreedyGQ, GDQ, QAgent, RandomAgent

import pickle
import glob
import datetime

import gym
import numpy
import torch
import os
import sys

# For recording
from pyvirtualdisplay import Display
from gym.wrappers.monitoring.video_recorder import VideoRecorder




def test_agent(folder, agent):
    display = Display(visible=False, size=(1400, 900))
    _ = display.start()

    env = gym.make('MountainCar-v0')

    videoNameString = folder + '/' + str(agent) + "MC.mp4"
    video = VideoRecorder(env, videoNameString)
    for _ in range(3):
        obs = env.reset()
        env.render()
        video.capture_frame()
        # while True:
        while True:
            # epsilon = 0 since for no exploring
            action = agent.chooseAction(0, obs)
            # action = env.action_space.sample()
            obs, reward, done, info = env.step(action)
            env.render()
            video.capture_frame()
            if done:
                break
    video.close()
    env.close()
    return



def main(results_path):
    # check if parent Agents folder exists or make one
    VID_PATH = os.getcwd() + '/Videos'
    if not os.path.isdir(VID_PATH):
        os.makedirs(VID_PATH)

    agents = glob.glob(results_path + '/Agents/*.agent')
    print(agents)
    for agent_file in agents: 
        # use pickle to recreate trained agents
        with open(agent_file, 'rb') as af:
            agent = pickle.load(af)

        test_agent(VID_PATH,  agent)


if __name__=='__main__':
    main(sys.argv[1])


