import numpy as np
import torch
import torch.nn as nn
import math
import TileCoderSutton as TC
import random
import time


class BaseAgent():
    def __init__(self, env, sizeObs, numActions, alpha, beta, gamma, trace, numTiles, tile_len=10**6):
        # constants
        self.size_obs = sizeObs 
        self.numActions = numActions
        self.gamma = gamma 
        self.alpha = alpha
        self.beta = beta
        self.env = env

        # Scale the inputs for feature representation
        self.obs_low = env.observation_space.low
        # print('obs low: ', self.obs_low)
        self.obs_high =  env.observation_space.high
        # print('obs high: ', self.obs_high)
        # print('Working????')

        # tile Coding constants
        self.numTilings = numTiles
        self.iht_num = tile_len
        self.iht = TC.IHT(self.iht_num)

        # length of weight vector
        self.weight_size = self.iht_num + 1 # bias 1

        # weight vectors
        self.weights = torch.rand(self.weight_size)
        self.eta = torch.zeros(self.weight_size)

        # Trace
        self.trace = torch.zeros(self.weight_size)
        self.lam = trace

        # timing arrays
        self.update_times = np.zeros([10**6])
        self.update_time_index = 0

 
    def getFeatures(self, state, action):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/(self.obs_high[i] - self.obs_low[i]) for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled, [action])
        zeros = torch.zeros(self.iht_num)
        for i in indices:
            zeros[i] = 1

        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float()))       
  
        
    def bestAction(self, state):
        # Get action value array
        actions = [torch.dot(self.weights, self.getFeatures(state, act)) for act in range(self.numActions)]
        # The max of action value is the best action
        action = torch.argmax(torch.tensor(actions)).item()
        return action

    def chooseAction(self, epsilon, state):
        # epsilon Q action policy.
        # if less than epsilon than take random action
        if random.random() < epsilon:
            return random.randrange(self.numActions)
        else:
            return self.bestAction(state)

    def trace_reset(self):
        self.trace = torch.zeros(self.weight_size)


class GreedyGQ(BaseAgent):
    def __init__(self, env, size_obs, numActions, alpha, beta, gamma, kappa=0, trace=0.4, numTiles=10):
        super().__init__(env, size_obs, numActions, alpha, beta, gamma, trace, numTiles=10)
        print('obs high: ', self.obs_high)
        print('obs low: ', self.obs_low)

    def update(self, reward, current_state_orig, action, next_state_orig, episode, epsilon):
        tic = time.perf_counter()
        # get next best action
        bestAction = self.bestAction(next_state_orig)
        
        # get state representations 
        current_state = self.getFeatures(current_state_orig, action)
        next_state = self.getFeatures(next_state_orig, bestAction)
            
        # Importance Sampling Ratio
        rho = 1/(1-epsilon) if action == self.bestAction(current_state_orig) else 0

        # Trace 
        current_trace = rho * (current_state + self.gamma * self.lam * self.trace)

        # Temporal Difference
        temp_diff = reward + self.gamma * torch.matmul(next_state, self.weights) -  torch.matmul(current_state, self.weights)
        
        # secondary weights update
        next_eta = self.eta + self.beta * (temp_diff * current_trace - torch.matmul(current_state, self.eta) * current_state)    

        # Primary Weights update
        next_weights = self.weights + self.alpha * (temp_diff * current_trace - self.gamma * (1 - self.lam) * torch.matmul(current_state, self.eta) * next_state)
        
        # Overwrite old weights
        self.weights = next_weights
        self.eta = next_eta
        self.trace = current_trace

        # save timer
        toc = time.perf_counter()
        self.update_times[self.update_time_index] = toc - tic
        self.update_time_index += 1

    def __str__(self):
        return "Greedy_GQ"


class GDQ(BaseAgent):
    def __init__(self, env, size_obs, numActions, alpha, beta, gamma, kappa=0, trace=0.4, numTiles=10, tile_length=10**4):
        super().__init__(env, size_obs, numActions, alpha, beta, gamma, trace, numTiles, tile_length)
        # when Kappa = 0 GDQ the update equation is the same as Greedy-GQ.
        # 
        self.previous_weights = torch.zeros(self.weight_size) 
        # the kappa term needs the C factor for c*alpha
        self.kappa = kappa * self.alpha
        self.kappa_factor = kappa

  
    def update(self, reward, current_state_orig, action, next_state_orig, episode, epsilon):
        tic = time.perf_counter()
        # get next best action
        bestAction = self.bestAction(next_state_orig)
        
        # get state representations 
        current_state = self.getFeatures(current_state_orig, action)
        next_state = self.getFeatures(next_state_orig, bestAction)
        
        # importance sampling ratio
        rho = 1/(1-epsilon) if action == self.bestAction(current_state_orig) else 0
        # Trace
        current_trace = rho * (current_state + self.gamma * self.lam * self.trace)
        
        # TD Error
        temp_diff = reward + self.gamma * torch.dot(next_state, self.weights) - torch.dot(current_state, self.weights)
        
        # Secondary weight update
        next_eta = self.eta + self.beta * (temp_diff * current_trace - torch.matmul(current_state, self.eta) * current_state)

        #weight update
        next_weights = self.weights + self.alpha * (temp_diff * current_trace - self.gamma * (1 - self.lam) * torch.matmul(current_state, self.eta) * next_state) - \
                         self.kappa * (torch.dot(current_state, self.weights) - torch.dot(current_state, self.previous_weights)) * current_state
            
        # Overwrite old weights
        self.previous_weights = self.weights.clone().detach()
        self.weights = next_weights
        self.eta = next_eta
        self.trace = current_trace

        # save timer
        toc = time.perf_counter()
        self.update_times[self.update_time_index] = toc - tic
        self.update_time_index += 1


    def __str__(self):
        return "GDQ_w_kappa_" + str(self.kappa_factor) + '_vec_size_'+str(self.iht_num)

        
class QAgent(BaseAgent):
    def __init__(self, env, size_obs, numActions, alpha, beta, gamma, kappa=0, trace=0.4, numTiles=10):
        super().__init__(env, size_obs, numActions, alpha, beta, gamma, trace, numTiles=10)
        
    def update(self, reward, current_state_orig, action, next_state_orig, episode, epsilon):
            # get next best action
            bestAction = self.bestAction(next_state_orig)

            # get state representations 
            current_state = self.getFeatures(current_state_orig, action)
            next_state = self.getFeatures(next_state_orig, bestAction)

            # importance sampling ratio
            rho = 1/(1-epsilon) if action == self.bestAction(current_state_orig) else 0
            # trace
            current_trace = rho * (current_state + self.gamma * self.lam * self.trace)

            # Temporal Difference
            temp_diff = reward + self.gamma * torch.matmul(next_state, self.weights) -  torch.matmul(current_state, self.weights)
            # Weight calculation
            next_weights = self.weights + self.alpha * temp_diff * current_trace 
            
            # Overwrite old weights
            self.weights = next_weights
            self.trace = current_trace

    def __str__(self):
        return "Q_Learning"

class RandomAgent(QAgent):
    def __init__(self, env, size_obs, numActions, alpha, beta, gamma, kappa=0, trace=0.4, numTiles=10):
        super().__init__(env, size_obs, numActions, alpha, beta, gamma, kappa, trace, numTiles)
        pass

    def chooseAction(self, epsilon, obs):
        return random.randint(0, self.env.action_space.n - 1)

    def __str__(self):
        return "RandomAgent"





