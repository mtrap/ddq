# read in the agent objects and calculate the timing stats
import pickle
import glob
import datetime
import sys
import os
import numpy as np
import matplotlib.pyplot as plt



def main(agents_folder):
    # load 
    agent_files = glob.glob(agents_folder + '/*.agent')
    agents = []
    for agent_file in agent_files:
        with open(agent_file, 'rb') as af:
            agents.append(pickle.load(af))


    # # calculate mean and stdev
    # means = [np.mean(a.update_times) for a in agents]
    # stdevs = [np.std(a.update_times) for a in agents]

    # # plot stats
    # x = np.arange(1,len(means)+1)
    # print(x)
    # print(means)
    # plt.errorbar(x, means, yerr=stdevs)
    # # plt.show()
    for a in agents:
        plt.plot(np.trim_zeros(a.update_times))
    plt.legend([str(a) for a in agents])
    plt.title('Time for a single Update from TileFA algorithms \n With Numba')
    plt.show()








if __name__=='__main__':
    main(sys.argv[1]) # read in the path to agents folder




