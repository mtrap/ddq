from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import smtplib


def sendmail(recipient, subject='',message='hello', *attchments):
    # make connection
    s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    s.login('onebonsai.warrior@gmail.com',"code2mail")

    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = 'matttrappe@gmail.com'
    msg['To'] = recipient
    msg.attach(MIMEText(message+'\n\n\n\n'))
    for att in attchments:
        with open(att,'rb') as f:
            part = MIMEApplication(f.read())
            part.add_header('Content-Disposition','attachment; filename='+att)
        msg.attach(part)
    s.send_message(msg)
    s.quit()