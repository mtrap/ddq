'''
Main.py for Cliff with DDQ
'''

from TileFAAlgorithmsCliff import GreedyGQ, GDQ, QAgent, RandomAgent
from sendmail import sendmail

import numpy as np
import concurrent.futures
import gym
import gym_gridworlds
import torch
import datetime
import plotly.express as px
import pandas as pd
import random
import socket
import os
import sys
import pickle
import pprint as pp
from tqdm import tqdm
import ray

def save_plot(df, path, plotname, config, yaxis):
    pd.options.plotting.backend = "plotly"
    fig = df.plot()
    fig.update_xaxes(title_text="Episodes")
    fig.update_yaxes(title_text=yaxis)
    fig.update_layout(height=500, width=900, title_text=config['env'] + " \n Repeated "+ str(config['repeats']) +"x")
    fig.write_html(path + plotname +'.html')


def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    return agent.train()

@ray.remote
def train(config, AGENT):
    # python parallel with multiple threads to speed it up 
    # Threads were faster than processes in practice.
    print("Training: ", str(AGENT), f'config: {config}')

    # don't know if I can use thread with ray
    # with concurrent.futures.ThreadPoolExecutor(max_workers=config['repeats']) as executor:
    #     data = executor.map(trainWrapper, ((config, AGENT),)*config['repeats'])
    data = [trainWrapper((config, AGENT)) for _ in range(config['repeats'])]

    R_DFS = [] # store the rewards for eac
    EP_LEN_DFS = [] # store the lengths of episodes
    # end_rewards = []
    for d in data:
        # print('d from data: ', d[0], d[1], d[2])
        R_DFS.append(d[0])
        EP_LEN_DFS.append(d[1])
    #     end_rewards.append(d[2])
        
    # # print(EP_LEN_DFS)
    # EP_LEN_DFSS = pd.Series(EP_LEN_DFS)
    # DFSS = pd.Series(R_DFS)
    EP_LEN_DFSS = pd.concat(EP_LEN_DFS, axis=1)
    DFSS = pd.concat(R_DFS, axis=1)
    return {'reward': DFSS.mean(axis=1), 
            'ep_len':EP_LEN_DFSS.mean(axis=1), 
            'name': str(AGENT) + f'kappa: {config["kappa"]}',
            'all_metrics': {'rewards': DFSS, 'steps': EP_LEN_DFSS}
            }   
        
def main(eps=10, repeats=4):
    
    # RL Training Variables
    ENV =  "Cliff-v0"
    # Primary learning rate
    ALPHA = .1
    # Secondary learning rate
    BETA = .001
    # Probability of taking a random action
    EPSILON = .1
    # λ is the eligibility trace parameter. λ=0 is off.
    LAMBDA = 0
    # Discount Factor
    GAMMA = .99
    # Learning rate for our term
    # go through many
    KAPPAS = [0, 4] # 0.25, .50, .75, 1, 2,

    # number of tiliings to create for Linear Function Approximation
    NUM_TILES = 10
    TILE_VEC_LEN = 10**6
    # How long to train our agents, mc tends do need around 600
    EPISODES = eps
    
    # Num of cpu processors for running repeats in parallel
    REPEATS = repeats


    # Create a parent path in results to store a results_dt file 
    # the results_dt file will have a graphs folder, agents folder, and txt file with details of training
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)


    DT = datetime.datetime.now().strftime("%b_%d_%Y_%H_%M_%S")
    PARENT = ROOT_PATH + 'Results_' + DT
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'   
    RESULTS_PATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTS_PATH = os.path.join(PARENT, AGENTS_STR)
    os.mkdir(RESULTS_PATH)
    os.mkdir(AGENTS_PATH)

    # text file to write details of the run
    DOC_STRING = RESULTS_PATH + '/params.txt'
    METRICS_STRING = RESULTS_PATH + 'all_metrics.rl'


    # config dictionaries to pass to objects
    config = {
        "env": ENV,
        "alpha": ALPHA,
        "beta": BETA,
        "numTiles": NUM_TILES,
        "tileVecSize": TILE_VEC_LEN,
        "kappa": 0,
        "episodes": EPISODES,
        "repeats": REPEATS,
        "lambda": LAMBDA,
        "epsilon": EPSILON,
        "gamma": GAMMA,
        "results path": RESULTS_PATH,
        "agents path": AGENTS_PATH,
        "metrics path": METRICS_STRING
    }


    inputs = [(config, GreedyGQ),(config, RandomAgent), (config, QAgent)]
    # add GDQ agents with updated config w/ unqiue kappas
    for k in KAPPAS:
        config['kappa'] = k
        inputs.append((config.copy(), GDQ))

    cpus = 5

    ray.init(num_cpus=cpus)
    result_ids = [train.remote(pair[0], pair[1]) for pair in inputs]
    futures = ray.get(result_ids)
    

    rewards_dict = dict()
    len_dict = dict()
    averageRewards = []
    averageSteps = []
    all_metrics = dict()
    for res in futures:
        rewards_dict[res['name']]=res['reward']
        len_dict[res['name']]=res['ep_len']
        all_metrics[res['name']]=res['all_metrics']
        averageRewards.append((res['name'], np.mean(res['reward'])))
        averageSteps.append((res['name'], np.mean(res['ep_len'])))
    
    ray.shutdown()

    # with concurrent.futures.ThreadPoolExecutor(max_workers=6) as executor:
    #     futures = executor.map(repeat_training, *zip(*inputs))

    # rewards_dict = dict()
    # len_dict = dict()
    # for res in futures:
    #     rewards_dict[res['name']]=res['reward']
    #     len_dict[res['name']]=res['ep_len']

         


    rewards_df = pd.DataFrame(rewards_dict)
    steps_df = pd.DataFrame(len_dict)

    save_plot(rewards_df, RESULTS_PATH, '/MeanRewards' + str(REPEATS) + DT, config, 'Total Rewards')
    save_plot(steps_df, RESULTS_PATH, '/MeanStepsPerEpisode' + str(REPEATS) + DT, config,'Total Steps')
    # save config to know our parameters
    with open(DOC_STRING, 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(DOC_STRING, 'w') )
        prnt.pprint(config)

    # save data for future processing if we want.
    with open(METRICS_STRING, 'wb') as f:
        pickle.dump(all_metrics, f)


    print("Finished.")
    sendmail('mtrappett@icloud.com', ENV + ' has finished','All done on ' + socket.gethostname() + '\n\nRun started on ' + DT)

    
    
if __name__=='__main__':
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 2:
        # how many episodes
        main(int(sys.argv[1]))
    elif len(sys.argv) == 3:
        # episodes and repeats
        main(int(sys.argv[1]), int(sys.argv[2]))





