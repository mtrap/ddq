### actor-critic agent
import random
import torch
from utils import TileCoderSutton as TC
import math
import gym
import numpy as np
import pandas as pd
import pickle
from tqdm import tqdm




class DAC():
    def __init__(self, config):
        self.config = config
        # open environment for instantiation but close it after
        env = gym.make(self.config['env'])
        self.obsLen = len(env.observation_space.sample())

        # for MountainCarContinuous the action is [-1, 1)
        self.numActions = 1
        self.actionMax, self.actionMin = 1.0, -1.0
        self.actionLow = env.action_space.low
        self.actionHigh = env.action_space.high

        # learning rates and constants
        # discount rate
        self.gamma = 0.99
        self.epsilon = 0.1
        # need to do a sweep to find best performance
        # using values from deghris 2012 for now
        self.alpha_theta = config['at']
        self.alpha_w = config['aw']
        self.alpha_v = config['av']
        # large a_u is breaking causing divergence.
        self.alpha_u = config['au']
        
        ##### Tile coding variables

        # scale the inputs
        self.obs_low = env.observation_space.low
        self.obs_high = env.observation_space.high
        # Tile coding constructor constructs
        self.numTilings = 10
        self.iht_num = 10**6
        self.iht = TC.IHT(self.iht_num)

        ############# weights and vectors
        # updates the actor weights
        # convert the obs feature vector of len
        self.weightSize = self.iht_num + 1

        # weights for the actor with a bias term
        self.theta = torch.zeros(self.weightSize)
        # gradient q learning weights for actor
        self.w = torch.zeros(self.weightSize)
        # gradient q learning weights for the critic
        self.v = torch.zeros(self.weightSize)
        # secondary weight update for gradient q learning
        self.u = torch.zeros(self.weightSize)

        # closing env
        env.close()

        self.debug = config['debug']

        self.tune = config['tune']
        self.xpos = []
        # agent performance tracking
        self.total_rewards = np.zeros(self.config['episodes'])
        self.episode_lengths = np.zeros(self.config['episodes'])


    def getFeatures(self, obs):
        # takes state and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/(self.obs_high[i] - self.obs_low[i]) for i in range(len(obs))]
        scaled = [s*sc for s,sc in zip(obs,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled)
        zeros = torch.zeros(self.iht_num)
        zeros[indices] = 1

        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float())) 

    def chooseAction(self, obs) -> float:
        # get the μ(obs) and add some noise to it from N(0,1).
        phi = self.getFeatures(obs)
        a = torch.matmul(self.theta,phi) + torch.normal(0, 1, size=(1,))
        # clip the action values
        a = a.item()
        if a < -1:
            a = -1
        elif a > 1:
            a = 0.99999999

        return a


    def update(self, trajectory, episode):
        # trajectory is a dict with keys: s, a, r, s_prime
        s = trajectory['s'] 
        a = trajectory['a'] 
        r = trajectory['r'] 
        s_prime = trajectory['s_prime']

        # convert states to features
        phi_s = self.getFeatures(s)

        # phi_s_prime = self.getFeatures(s_prime)
        try:
            phi_s_prime = self.getFeatures(s_prime)
        except:
            print('Self: ', self)
            print('S: ', s)
            print('S_prime:', s_prime)
            print({"s":s, "a":a, "r": r, "s_prime": s_prime})
            print(f'theta weights: {self.theta}\nW weights: {self.w}\nV weights: {self.v}\nU weights: {self.u}')
            phi_s_prime = self.getFeatures([0, 0])


        # grad of mu_theta is the feature vector
        # in linear 
        # current state
        grad_mu = phi_s
        # next state
        grad_mu_prime = phi_s_prime

        # mu(s)
        # current state action_value
        mu_s = torch.matmul(self.theta, phi_s)
        # next state action value
        mu_s_prime = torch.matmul(self.theta, phi_s_prime)

        # state-action features
        phi_sa = grad_mu*(a - mu_s)
        phi_s_prime_mu = grad_mu_prime * (mu_s_prime - mu_s)

        phisadotu = torch.matmul(phi_sa, self.u)

        # Temporal Difference Error
        # the Q_w_sa_hat simplifies to V(s'), because 
        # μ(s') - μ(s') = 0 and so the advantage = 0
        td_error = r + self.gamma * torch.matmul(phi_s_prime, self.v) - self.Q_w_sa(phi_s, phi_sa)

        # u update
        print("u: ", self.u) if self.debug else False
        self.u += self.alpha_u * (td_error - phisadotu) * phi_sa


        # v update
        print("v: ", self.v) if self.debug else False
        self.v += self.alpha_v * td_error * phi_s - \
                self.alpha_v * self.gamma * phi_s_prime * phisadotu

        # w update
        print("W: ", self.w) if self.debug else False
        self.w += self.alpha_w * td_error * phi_sa - \
                self.alpha_w * self.gamma * phi_s_prime_mu * phisadotu

        # Theta update
        delta_theta = self.alpha_theta * phi_s * torch.matmul(phi_s, self.w)
        print('delta_theta: ', delta_theta) if self.debug else False
        self.theta += delta_theta


    def Q_w_sa(self, phi_s_, phi_sa_):
        # phi_s is also grad_mu in the linear setting

        # State values
        V_s = torch.matmul(self.v, phi_s_)

        # Advantage of taking action a over action mu_s
        # φ(s,a) dot w
        A_w_sa = torch.matmul(phi_sa_, self.w)

        return A_w_sa + V_s

    def anyNan(self):
        for w in [self.theta, self.w, self.v, self.u]:
            if any(torch.isnan(w)) or any(torch.isinf(w)):
                return True

    def resetWeights(self):
        # weights for the actor with a bias term
        self.theta = torch.rand(self.weightSize)
        # gradient q learning weights for actor
        self.w = torch.rand(self.weightSize)
        # gradient q learning weights for the critic
        self.v = torch.rand(self.weightSize)
        # secondary weight update for gradient q learning
        self.u = torch.zeros(self.weightSize)


    def train(self):
        env = gym.make(self.config['env'])
        env.power = self.config['power'] # originally 0.0015, making easier for DAC testing

        for episode in range(self.config["episodes"]):
            if self.debug:
                print('Episode: ', episode)
                print(self)
                print(f'theta weights: {self.theta}\nW weights: {self.w}\nV weights: {self.v}\nU weights: {self.u}')
            # resets for new episode
            obs = env.reset()
            rewards = 0
            count = 0
            try:
                while True:
                    # take step and get new obs
                    action = self.chooseAction(obs)
                    next_obs, reward, done, info = env.step([action])


                    # Udate weights 
                    self.update({"s":obs, "a":action, "r": reward, "s_prime": next_obs}, episode)
                    
                    # increment trackers
                    count += 1
                    rewards += reward
                    # save x position
                    self.xpos.append(obs[0])

                    # update obs
                    obs = next_obs

                    if  done:  
                        break
                        
            except ValueError:
                # overflow to NaN in the weights
                #if any nan in weights
                if self.anyNan():
                    self.total_rewards[episode] = -500
                    self.episode_lengths[episode] = 5000
                    if self.debug:
                        print({"s":obs, "a":action, "r": reward, "s_prime": next_obs})
                        print(f'theta weights: {self.theta}\nW weights: {self.w}\nV weights: {self.v}\nU weights: {self.u}')
                    self.resetWeights()
                    break

            # Save Trackers
            self.total_rewards[episode] = rewards
            self.episode_lengths[episode] = count

        env.close()

        return (pd.Series(self.total_rewards), pd.Series(self.episode_lengths), self, self.xpos)  
    

    def save(self):
        #Export trained agent to a file to later get for testing.
        agent_file = '/' + str(self) + '.agent'

        
        with open(self.config['agents path'] + agent_file, 'wb') as af:
            # save agent state
            pickle.dump(self, af)

    def get_agent_performance(self):
        return np.mean(self.total_rewards), np.mean(self.episode_lengths)

    def __str__(self):
        '''        self.alpha_theta = config['at']
        self.alpha_w = config['aw']
        self.alpha_v = config['av']
        # large a_u is breaking causing divergence.
        self.alpha_u = config['au']'''
        return f'DAC_{self.alpha_theta}_{self.alpha_w}_{self.alpha_v}_{self.alpha_u}'     


class copdac_q(DAC):
    def __init__(self, config):
        super().__init__(config)

        # Learning rates are the only different variables
        self.alpha_theta = config['at']
        self.alpha_w = config['aw']
        self.alpha_v = config['av']

    def update(self, trajectory, episode):
        # trajectory is a dict with keys: s, a, r, s_prime
        s = trajectory['s'] 
        a = trajectory['a'] 
        r = trajectory['r'] 
        s_prime = trajectory['s_prime']

        # convert states to features
        phi_s = self.getFeatures(s)
        try:
            phi_s_prime = self.getFeatures(s_prime)
        except:
            print('Self: ', self)
            print('S: ', s)
            print('S_prime:', s_prime)
            phi_s_prime = self.getFeatures([0, 0])


        # grad of mu_theta is the feature vector in linear
        grad_mu = phi_s
        grad_mu_prime = phi_s_prime

        # mu(s)
        mu_s = torch.matmul(self.theta, phi_s)
        mu_s_prime = torch.matmul(self.theta, phi_s_prime)

        # state-action features
        phi_sa = grad_mu*(a - mu_s)
        phi_s_prime_mu = grad_mu_prime * (mu_s_prime - mu_s)

        # Temporal Difference Error
        # for COPDAC, the Q_w_sa_hat simplifies to V(s'), because 
        # μ(s') - μ(s') = 0 and so the advantage = 0
        td_error = r + self.gamma * torch.matmul(phi_s_prime, self.v)  - self.Q_w_sa(phi_s, phi_sa)
        # v update
        # print("v: ", self.v)
        self.v = self.v + self.alpha_v * td_error * phi_s 
        # w update
        # print("W: ", self.w)
        self.w = self.w + self.alpha_w * td_error * phi_sa 

        # Theta update
        delta_theta = self.alpha_theta * phi_s * torch.matmul(phi_s, self.w)
        # print('delta_theta: ', delta_theta)
        self.theta = self.theta + delta_theta


    def __str__(self):
        return f'COPDAC_Q_{self.alpha_theta}_{self.alpha_w}_{self.alpha_v}'





class RandomAgent(DAC):
    def __init__(self, config):
        super().__init__(config)
        self.name = 'RandomAgent'


    def chooseAction(self, obs) -> float:
        # action is single continuous between -1.0 and 1.0
        return random.uniform(self.actionLow, self.actionHigh)[0]


    def update(self, trajectory, episode):
        pass

    def __str__(self):
        return self.name





