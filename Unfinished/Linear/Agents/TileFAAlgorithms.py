import numpy as np
import torch
import torch.nn as nn
import math
import sys
import random
import gym
import pickle
import pandas as pd

from utils import TileCoderSutton as TC


class BaseAgent():
    def __init__(self, config):
        torch.manual_seed(0)
        self.config = config
        env = gym.make(config['env'])
        # constants
        self.size_obs =  len(env.observation_space.sample())
        self.numActions =env.action_space.n
        self.gamma =     config['gamma']
        self.alpha =     config['alpha']
        self.beta =      config['beta']
        self.env =       config['env']
        self.epsilon =   config['epsilon']

        # Scale the inputs for feature representation
        obs_low =     np.asarray(env.observation_space.low).astype(float)
        obs_high =    np.asarray(env.observation_space.high).astype(float)
        self.spread = obs_high - obs_low

        # tile Coding constants
        self.numTilings = config['numTiles']
        self.iht_num = config['tileVecSize']
        self.iht = TC.IHT(self.iht_num)

        # length of weight vector
        self.weight_size = self.iht_num + 1 # bias 1

        # weight vectors
        self.weights = torch.rand(self.weight_size)
        self.eta = torch.zeros(self.weight_size)

        self.lastBestAction = random.randrange(self.numActions)


        # closing env
        env.close()


        # agent performance tracking
        self.total_rewards =   np.zeros(self.config['episodes'])
        self.observations =    []
        self.action_tracker =  []
        self.TDError_tracker = []
        self.epsilon_tracker = []
        self.loss_tracker =    [] # not used because no torch criterion used
        self.debug =           config['debug']

        #save weights if debug
        if not self.config['tune'] and self.config['debug']:
            self.weight_history = np.zeros((self.weight_size, self.config['episodes']))


 
    def getFeatures(self, state, action):
        # takes state and action and returns a feature vector constructed from tile codings.
        # first scale the state according to the high and low vals for each index
        scale = [self.numTilings/self.spread[i] for i in range(len(state))]
        scaled = [s*sc for s,sc in zip(state,scale)]
        
        # TC.tiles returns the indices of where in the array is a value, else 0.
        indices = TC.tiles(self.iht, self.numTilings, scaled, [action])
        zeros = torch.zeros(self.iht_num)
        zeros[indices] = 1

        # convert to torch for easier math.
        return torch.cat((zeros, torch.tensor([1]).float()))       
  
        
    def bestAction(self, state):
        # Get action value array
        actions = [torch.dot(self.weights, self.getFeatures(state, act)) for act in range(self.numActions)]
        # The max of action value is the best action
        action = torch.argmax(torch.tensor(actions)).item()
        return action

    def chooseAction(self, state):
        # epsilon Q action policy.
        # if less than epsilon than take random action
        if random.random() < self.epsilon:
            return random.randrange(self.numActions)
        else:
            return self.bestAction(state)


    def train(self):
        env = gym.make(self.config['env'])

        for episode in range(self.config["episodes"]):
            # resets for new episode
            obs = env.reset()
            rewards = 0
            steps = 0

            while True:
                # take step and get new obs
                action = self.chooseAction(obs)
                next_obs, reward, done, info = env.step(action)

                # Udate weights 
                self.update(obs, action, reward, next_obs, episode)
                
                # increment trackers
                rewards += reward
                self.action_tracker.append(action)
                self.observations.append([obs[i]/self.spread[i] for i in range(len(obs))])

                # update obs
                obs = next_obs
                if  done:  
                    break
            
            # decrease epsilon
            if self.epsilon > 0.05:
                self.epsilon -= (2/self.config['episodes'])


            # Save Trackers
            self.epsilon_tracker.append(self.epsilon)
            self.total_rewards[episode] = rewards
            if not self.config['tune'] and self.config['debug']:
                self.weight_history[:,episode] = self.weights.detach()

        env.close()

        return self.return_data() 


    def return_data(self):
        # process the observations to turn into a pd DF for graphing.
        # return a list of pd.Series for each obs. 
        # len(obs) = # of obs
        data_dict = dict()
        stack_obs = np.stack(self.observations)
        # obs is scaled
        for ob in range(self.size_obs):
            data_dict[f'obs[{ob}]'] = pd.Series(stack_obs[:,ob])

        data_dict['Total Rewards'] =  pd.Series(self.total_rewards)
        data_dict['Actions Taken'] =  pd.Series(self.action_tracker)
        data_dict['Average reward'] = np.mean(self.total_rewards)
        data_dict['Self Object'] =    self
        data_dict['Epsilon vals'] =   pd.Series(self.epsilon_tracker)
        # tracked in each agent class
        data_dict['loss_tracker'] =   pd.Series(self.TDError_tracker)
        data_dict['TDErrors'] =       pd.Series(self.TDError_tracker)
        if not self.config['tune'] and self.config['debug']:
            data_dict['network params'] = self.weight_history

        return data_dict

    def save(self):
        #Export trained agent to a file to later get for testing.
        agent_file = '/' + str(self) + '.agent'
        with open(self.config['AgentPath'] + agent_file, 'wb') as af:
            # save agent state
            pickle.dump(self, af)



class GreedyGQ(BaseAgent):
    def __init__(self, config):
        super().__init__(config)

    def update(self, current_state_orig, action, reward, next_state_orig, episode):
        # get next best action
        bestAction = self.bestAction(next_state_orig)
        
        # get state representations 
        current_state = self.getFeatures(current_state_orig, action)
        next_state = self.getFeatures(next_state_orig, bestAction)

        # Temporal Difference
        temp_diff = reward + self.gamma * torch.matmul(next_state, self.weights) -  torch.matmul(current_state, self.weights)
        
        # x * eta
        xdoteta = torch.matmul(current_state, self.eta)
        # secondary weights update
        next_eta = self.eta + self.beta * (temp_diff * current_state - xdoteta * current_state)    

        # Primary Weights update
        next_weights = self.weights + self.alpha * (temp_diff * current_state - self.gamma * xdoteta * next_state)
        
        # Overwrite old weights
        self.weights = next_weights
        self.eta = next_eta
        self.lastBestAction = bestAction
        self.TDError_tracker.append(temp_diff)


    def __str__(self):
        return f'Greedy_GQ_{self.alpha}_{self.beta}'


class GDQ(BaseAgent):
    def __init__(self, config):
        super().__init__(config)
        # when Kappa = 0 GDQ the update equation is the same as Greedy-GQ.
        self.previous_weights = torch.zeros(self.weight_size) 
        # the kappa term needs the C factor for c*alpha
        self.kappa = config['kappa'] * config['alpha']
        self.kappa_factor = config['kappa']
        

  
    def update(self, current_state_orig, action, reward, next_state_orig, episode):
        # get next best action
        bestAction = self.bestAction(next_state_orig)
        
        # get state representations 
        current_state = self.getFeatures(current_state_orig, action)
        next_state = self.getFeatures(next_state_orig, bestAction)
        
        # x * w
        xdotw = torch.dot(current_state, self.weights)
        # x * eta
        xdoteta = torch.matmul(current_state, self.eta)

        # TD Error
        temp_diff = reward + self.gamma * torch.dot(next_state, self.weights) - xdotw

        # Secondary weight update
        next_eta = self.eta + self.beta * (temp_diff * current_state - xdoteta * current_state)

        #weight update
        next_weights = self.weights + self.alpha * (temp_diff * current_state - self.gamma * xdoteta * next_state) - \
                         self.kappa * (xdotw - torch.dot(current_state, self.previous_weights)) * current_state
            
        # Overwrite old weights
        self.previous_weights = self.weights.clone().detach()
        self.weights = next_weights
        self.eta = next_eta
        self.lastBestAction = bestAction
        self.TDError_tracker.append(temp_diff)



    def __str__(self):
        return "GDQ_w_kappa_" + str(self.kappa_factor)

        
class QAgent(BaseAgent):
    def __init__(self, config):
        super().__init__( config)
        
    def update(self, current_state_orig, action, reward, next_state_orig, episode):
            # get next best action
            bestAction = self.bestAction(next_state_orig)

            # get state representations 
            current_state = self.getFeatures(current_state_orig, action)
            next_state = self.getFeatures(next_state_orig, bestAction)

            # Temporal Difference
            temp_diff = reward + self.gamma * torch.matmul(next_state, self.weights) -  torch.matmul(current_state, self.weights)
            # Weight calculation
            next_weights = self.weights + self.alpha * temp_diff * current_state
            
            # Overwrite old weights
            self.weights = next_weights
            self.TDError_tracker.append(temp_diff)

    def __str__(self):
        return "Q_Learning"

class RandomAgent(QAgent):
    def __init__(self, config):
        super().__init__(config)


    def chooseAction(self, obs):
        return random.randint(0, self.numActions- 1)

    def __str__(self):
        return "RandomAgent"





