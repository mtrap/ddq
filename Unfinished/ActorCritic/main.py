'''
main File for Actor Critic Learning - with Linear Function Approximation
Author Matthew Trappett
Date: June 21, 2022


Very similar to other main file, but the environemnts are continuous'''

from utils.sendmail import sendmail
from utils.DataProcessing import process_results
from Agents.LinearActorCritic import CopdacQ, CopdacGQ, CopdacDDQ, RandomAgent

import numpy as np
import concurrent.futures
import gym
import torch
from datetime import datetime
import plotly.graph_objs as go
import pandas as pd
import random
import socket
import os
import sys
import traceback
import pickle
import pprint as pp
from tqdm import tqdm
import ray
from scipy.signal import savgol_filter
import time





@ray.remote
def trainWrapper(args):
    config = args[0]
    AGENT = args[1]
    agent = AGENT(config)
    print(f'Training agent: {agent}')
    return agent.train()



def main(agent_str, env, numEpisodes, repeats, cpus):
    '''
    folder needs to have the agent+env+datetime
    '''
    numEpisodes = int(numEpisodes)
    repeats = int(repeats)
    cpus = int(cpus)
    # torch.manual_seed(1423)

    print(f'Training with \n \
            Agent: {agent_str} \n \
            env: {env} \n \
            nEpisodes: {nEpisodes} \n \
            repeats: {repeats} \n \
            cpus: {cpus}')

    # agents dict
    agent_dict = {'copdacq': CopdacQ, 'cgq':CopdacGQ, 'cddq': CopdacDDQ, 'r': RandomAgent}
    AGENT = agent_dict[agent_str]

    # saved alphas tuned for each agent
    alphas = {'MountainCarContinuous-v0':{'copdacq': 1e-5, 'cgq': 1e-5, 'cddq': 1e-5, 'r': 0.0001},
              'Pendulum-v1':   {'copdacq': 1e-6,  'cgq': 5e-6,  'cddq': 5e-7, 'r': 0.0001},
             }
    kappas = {'MountainCarContinuous-v0':.5,
              'Pendulum-v1':   4.0,
             }

    # set up paths for saving graphs and agent
    # agent + env + DT
    date_string = '{:%B_%d_%Y_%H_%M_%S}'.format(datetime.now())
    env_string = env.replace('-', '_')
    FolderName = '_'.join([agent_str, env_string, date_string])

    # check if Results folder exits and create one if not
    ROOT_PATH = os.getcwd() + '/Results/'
    if not os.path.isdir(ROOT_PATH):
        os.makedirs(ROOT_PATH)

    PARENT = ROOT_PATH + FolderName
    os.makedirs(PARENT)
    GRAPHS_STR = 'Graphs/'
    AGENTS_STR = 'Agents/'  
    DATA_STR   = 'Data/' 
    GRAPHSPATH = os.path.join(PARENT, GRAPHS_STR)
    AGENTSPATH = os.path.join(PARENT, AGENTS_STR)
    DATAPATH = os.path.join(PARENT, DATA_STR)
    os.mkdir(GRAPHSPATH)
    os.mkdir(AGENTSPATH)
    os.mkdir(DATAPATH)


    # master config dictionary
    config = {# for all agents
              'env':        env,
              'episodes':   int(numEpisodes),
              'repeats':    int(repeats),
              'cpus':       int(cpus),
              # Linear Function Approximation values
              'numTiles':   25, # number of tiles per tiling
              'tileVecSize': 10, # number of tilings
              # learning rate values
              'alpha':      0.00001,
              'at':         alphas[env][agent_str],
              'aw':         alphas[env][agent_str],
              'av':         alphas[env][agent_str],
              'au':         alphas[env][agent_str],
              'beta' :      alphas[env][agent_str],
              'kappa':      kappas[env] if agent_str=='cddq' else 0, # only if Greedy

              'gamma':      0.99,
              'epsilon':    5, # decreases to 0.05 
              'eFactor':    2, # how fast to decrease to 0.05
              'debug':      False, # saves lots of graphs to print out.
              'tune':       False,


              # paths
              'AgentPath': AGENTSPATH,
              'GraphPath': GRAPHSPATH,
              'DataPath' : DATAPATH,
                }


    # # initialize agent
    # agent = AGENT(config)
    # data_dict, trained_agent = agent.train()
    # start ray
    ray.init(num_cpus=cpus)
    # for a number of repeats train the agent on the env
    result_ids_train = [trainWrapper.remote((config, AGENT)) for _ in range(config['repeats'])]
    # as the agent finishes then 
    data = []
    while len(result_ids_train):
        done_ids_train, result_ids_train = ray.wait(result_ids_train)
        data += ray.get(done_ids_train)

    # stop ray
    ray.shutdown()

    print('Finished Training...\nProcessing Data...')
    # process results, make graphs and save them and best agent
    process_results(config, data)



    # save config to know our parameters
    with open(GRAPHSPATH + '/params.txt', 'w') as f:
        prnt = pp.PrettyPrinter(stream=open(GRAPHSPATH + '/params.txt', 'w') )
        prnt.pprint(config)


    # send email to signal done
    sendmail('mtrappett@icloud.com',  env + ' actor-critic has finished',
        'All done on ' + socket.gethostname() + 
        '\n\nRun started on ' + 
        date_string + '\n\nCommandline script to download\n\nscp -r MTrappett@orthus.nic.uoregon.edu:'+PARENT+
         ' ~/Downloads/ActorCriticResults')



if __name__=='__main__':
    env_dict = {'mc':'MountainCarContinuous-v0' ,'pen':'Pendulum-v1'}
    agent_dict = {'copdacq': CopdacQ, 'cgq': CopdacGQ, 'cddq': CopdacDDQ, 'r': RandomAgent}
    # optional repeat paramter, for production graph will want a lot.
    repeats = 1
    cpus = 4
    if sys.argv[1] == 'h':
        print(f'example script:\npython main.py greedy mc 1000 10 10 &\nfields:\n-agent -env -numEpisodes -repeats -cpus')
        print(f'envs: {env_dict.keys()}')
        print(f'agents: {agent_dict.keys()}')
        quit()


    assert len(sys.argv) >= 4, 'Need arguments: agent, env, numEpisodes'
    agent = sys.argv[1]
    env = sys.argv[2]
    nEpisodes = sys.argv[3]

    if len(sys.argv) > 4:
        repeats = sys.argv[4]
    if len(sys.argv) == 6:
        cpus = sys.argv[5]

    try:
        start_time = time.time()
        if agent == 'all':
            for key in agent_dict.keys():
                print(f'Starting {key} training')
                main(key, env_dict[env], 5000, 30, 30)

        else:
            
            main(agent, env_dict[env], nEpisodes, repeats, cpus)
            
        elapsed_time = time.time()-start_time
        print('Finished')
        print(f'Total Time for completion:\n{elapsed_time}')
    except:
        # send email to signal done
        sendmail('mtrappett@icloud.com',  env + "threw an error",
            'error on ' + socket.gethostname() + 
            'error ' + 
            str(traceback.format_exc(limit=None, chain=True)))


















