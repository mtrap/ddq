'''
Agents for linear actor critic learning
Author: Matthew Trappett
Date: August 1, 2022
'''


import numpy as np
import torch
import torch.nn as nn
import math
import sys
import random
import gym
import pickle
import pandas as pd
import time
from copy import copy

# from utils import TileCoderSutton as TC
from PyFixedReps import TileCoder # https://github.com/andnp/PyFixedReps


class BaseAgent():
    def __init__(self, config):
        # torch.manual_seed(0)
        self.config =      config
        env =              gym.make(config['env'])
        # constants
        self.size_obs =    len(env.observation_space.sample())
        self.gamma =       config['gamma']
        self.beta =        config['beta']
        self.env =         config['env']
        self.epsilon =     config['epsilon']
        self.epsilon_factor = config['eFactor']
        # alpha values
        # using values from deghris 2012 for now
        self.alpha_theta = config['alpha']
        self.alpha_w =     config['alpha']#config['aw']
        self.alpha_v =     config['alpha']#config['av']
        # large a_u is breaking causing divergence.
        self.alpha_u =     config['alpha']#config['au']

        # Scale the inputs for feature representation
        self.act_low =     env.action_space.low[0]
        self.act_high=     env.action_space.high[0]
        self.obs_low =     np.asarray(env.observation_space.low).astype(float)
        self.obs_high =    np.asarray(env.observation_space.high).astype(float)
        self.spread =      self.obs_high - self.obs_low
        # self.scale =       [self.numTilings/self.spread[i] for i in range(self.size_obs)]

        # tile Coding constants
        # self.numTilings = config['numTiles']
        # self.iht_num =    config['tileVecSize']
        # self.iht =        TC.IHT(self.iht_num)
        self.tc = TileCoder({
            'tiles': config['numTiles'],
            'tilings': config['tileVecSize'],
            'dims': self.size_obs,

            'random_offset': True,
            'input_ranges': [(a,b) for a,b in zip(self.obs_low, self.obs_high)],
            'scale_ouput': True,
            })

        num_features = self.tc.features()
        # print(f'Num of features: {num_features}')

        # length of weight vector
        self.weightSize =  num_features + 1 # bias 1
        # weight vectors
        self.theta =       torch.rand(self.weightSize).double()
        # gradient q learning weights for actor
        self.w =           torch.rand(self.weightSize).double()
        # gradient q learning weights for the critic
        self.v =           torch.rand(self.weightSize).double()
        # secondary weight update for gradient q learning
        self.u =           torch.rand(self.weightSize).double()

        # closing env
        env.close()

        # agent performance tracking
        self.total_rewards =   np.zeros(self.config['episodes'])
        self.observations =    []
        self.action_tracker =  []
        self.TDError_tracker = []
        self.epsilon_tracker = []
        self.loss_tracker =    [] # not used because no torch criterion used
        self.debug =           config['debug']
        self.eps_times =       []

        #save weights if debug
        if self.debug:
            self.weight_history = np.zeros((self.weightSize, self.config['episodes']))


 
    def getFeatures_indices(self, state):
        # return the indices of the features, since it is binary,
        # the product between weights and features is just the weights 
        # at the index location
        indices = torch.from_numpy(np.array(self.tc.get_indices(state)))
        # print(f'length of indices: {len(indices)}')
        return torch.cat((indices, torch.tensor([1]))) 


    def getFeatures(self, state):
        try:
            # takes state and action and returns a feature vector constructed from tile codings.
            # print('state: ', state)
            # scaled = [(state[i] - self.obs_low[i])*self.scale[i] for i in range(self.size_obs)]
            # print('Scaled: ', scaled)
            # TC.tiles returns the indices of 1 in feature vector
            # indices = TC.tiles(self.iht, self.numTilings, scaled)
            # indices = TC.tiles(self.iht, self.numTilings, state)
            #
            # using the alternate Tilings
            features = self.tc.encode(state)
            # print('Indices: ', indices)
            # zeros = torch.zeros(self.iht_num)
            # zeros[indices] = 1
        except ValueError as err:
            print('Encountered this error:\n')
            print(err)
            print('Weights:')
            print('theta\n')
            print(self.theta)
            print('w\n')
            print(self.w)
            print('v\n')
            print(self.v)
            self.resetWeights()
            features = torch.zeros(self.weightSize-1)
        # convert to torch for easier math.
        return torch.cat((features, torch.tensor([1]).float()))       


    def chooseAction(self, obs) -> float:
        # get the μ(obs) and add some noise to it from N(0,1).
        phi = self.getFeatures_indices(obs)
        a = torch.sum(self.theta[phi]) + torch.normal(0, self.epsilon, size=(1,))
        # clip the action values
        a = torch.clip(a, min=self.act_low, max=self.act_high)
        return [a.item()]

    def anyNan(self):
        for w in [self.theta, self.w, self.v, self.u]:
            if any(torch.isnan(w)) or any(torch.isinf(w)):
                return True

    def resetWeights(self):
        # weights for the actor with a bias term
        self.theta = torch.rand(self.weightSize)
        # gradient q learning weights for actor
        self.w = torch.rand(self.weightSize)
        # gradient q learning weights for the critic
        self.v = torch.rand(self.weightSize)
        # secondary weight update for gradient q learning
        self.u = torch.zeros(self.weightSize)


    def train(self):
        env = gym.make(self.config['env'])

        for episode in range(self.config["episodes"]):
            # print('Running Episode ', episode)
            # resets for new episode
            obs = env.reset()
            rewards = 0
            steps = 0

            while True:
                start_time = time.time()
                # take step and get new obs
                action = self.chooseAction(obs)
                next_obs, reward, done, info = env.step(action)
                # the reward is weird, so we'll make it similar to discrete mountain car
                if next_obs[0] > 0.45:
                    reward = 0
                else:
                    reward = -1
                # elapsed_time = time.time()-start_time
                # print(f'time per choose action/step : {elapsed_time}')
                # print(f'Next obs: {next_obs}, action: {action}')
                # Udate weights 
                self.update(obs, action[0], reward, next_obs, episode)
                
                # increment trackers
                rewards += reward
                self.action_tracker.append(action)
                self.observations.append([obs[i]/self.spread[i] for i in range(len(obs))])

                # update obs
                obs = next_obs
                if  done:  
                    break
                elapsed_time = time.time()-start_time
                self.eps_times.append(elapsed_time)

                # if self.anyNan():
                #     print('Nan, Breaking.')
                #     break

            # if self.anyNan():
            #     print('Nan, Breaking.')
            #     break

            # decrease epsilon
            if self.epsilon > 0.05:
                self.epsilon -= (self.epsilon_factor/self.config['episodes'])
            if self.epsilon < 0:
                self.epsilon = 0.05


            # Save Trackers
            self.epsilon_tracker.append(self.epsilon)
            self.total_rewards[episode] = rewards
            # theta are the main weights
            if self.debug:
                self.weight_history[:,episode] = self.theta.detach()

        env.close()

        return self.return_data() 


    def return_data(self):
        # process the observations to turn into a pd DF for graphing.
        # return a list of pd.Series for each obs. 
        # len(obs) = # of obs
        data_dict = dict()
        stack_obs = np.stack(self.observations)
        # obs is scaled
        for ob in range(self.size_obs):
            data_dict[f'obs[{ob}]'] = pd.Series(stack_obs[:,ob])

        data_dict['Total Rewards'] =  pd.Series(self.total_rewards)
        data_dict['Actions Taken'] =  pd.Series(self.action_tracker)
        data_dict['Average reward'] = np.mean(self.total_rewards)
        if self.debug:
            data_dict['network params'] = self.weight_history
        data_dict['Self Object'] =    self
        data_dict['Epsilon vals'] =   pd.Series(self.epsilon_tracker)
        # tracked in each agent class
        data_dict['loss_tracker'] =   pd.Series(self.TDError_tracker)
        data_dict['TDErrors'] =       pd.Series(self.TDError_tracker)
        data_dict['Episode Times'] =  pd.Series(self.eps_times)

        return data_dict

    def save(self):
        #Export trained agent to a file to later get for testing.
        agent_file = '/' + str(self) + '.agent'
        with open(self.config['AgentPath'] + agent_file, 'wb') as af:
            # save agent state
            # pickle.dump(self, af)
            pass

    def Q_w_sa(self, _phi_s_mask, _phi_sa):
        # phi_s is also grad_mu in the linear setting

        # State values
        V_s = torch.sum(self.v[_phi_s_mask])

        # Advantage of taking action a over action mu_s
        # φ(s,a) dot w
        A_w_sa = torch.matmul(_phi_sa.float(), self.w[_phi_s_mask].float())

        return A_w_sa + V_s



class CopdacQ(BaseAgent):
    def __init__(self, config):
        super().__init__(config)

    def update(self,  state, action, reward, next_state, done):
        
        # print('starting update')
        # convert states to features
        phi_s_mask = self.getFeatures_indices(state)
        # next state features
        phi_s_prime_mask = self.getFeatures_indices(next_state)

        # start_time = time.time()
        # the derivative of u(s) = phi(s)
        grad_mu_mask = copy(phi_s_mask)
        grad_mu_prime_mask = copy(phi_s_prime_mask)
        # get smaller vectors
        grad_mu = torch.ones(len(grad_mu_mask)) # phi_s
        grad_mu_prime = torch.ones(len(grad_mu_prime_mask)) # phi_s_prime


        # action value (I think) or deterministic value
        # mu_s = torch.matmul(self.theta, phi_s)
        mu_s = torch.sum(self.theta[phi_s_mask])
        # mu_s_prime = torch.matmul(self.theta, phi_s_prime)
        mu_s_prime = torch.sum(self.theta[phi_s_prime_mask])


        # state-action features
        phi_sa = grad_mu*(action - mu_s)
        phi_s_prime_mu = grad_mu_prime * (mu_s_prime - mu_s)

        # phisadotu = torch.matmul(phi_sa, self.u)
        phisadotu = (action - mu_s) * torch.sum(self.u[grad_mu_mask])

        # Temporal Difference Error
        # the Q_w_sa_hat simplifies to V(s'), because 
        # μ(s') - μ(s') = 0 and so the advantage = 0
        # removed torch.matmul(phi_s_prime, self.v)  = torch.sum(self.v[phi_s_prime_mask])
        td_error = reward + self.gamma * torch.sum(self.v[phi_s_prime_mask]) - self.Q_w_sa(phi_s_mask, phi_sa)
        # elapsed_time = time.time()-start_time
        # print(f'time/matmuls update : {elapsed_time}')
        # update the weight vectors
        # remvoed torch.matmul(grad_mu, self.w) = torch.sum(self.w[grad_mu_mask])
        self.theta[phi_s_mask] = self.theta[phi_s_mask] + self.alpha_theta * grad_mu * torch.sum(self.w[grad_mu_mask]) 
        self.w[phi_s_mask] = self.w[phi_s_mask] + self.alpha_w * td_error * phi_sa # phi_sa is a vector of 1's
        self.v[phi_s_mask] = self.v[phi_s_mask] + self.alpha_v * td_error # phi_s is a binary vector, so just need to index

        # track the error
        self.TDError_tracker.append(td_error)
        # print('ending update')

    def __str__(self):
        return 'COPDAC_Q_'


class CopdacDDQ(BaseAgent):
    def __init__(self, config):
        super().__init__(config)
        self.kappa = config['kappa']
        self.prev_v = copy(self.v)
        self.prev_w = copy(self.w)

    def prev_Q_w_sa(self, _phi_s_mask, _phi_sa):
        # phi_s is also grad_mu in the linear setting

        # State values
        V_s = torch.sum(self.prev_v[_phi_s_mask])

        # Advantage of taking action a over action mu_s
        # φ(s,a) dot w
        A_w_sa = torch.matmul(_phi_sa.float(), self.prev_w[_phi_s_mask].float())

        return A_w_sa + V_s

    def update(self,  state, action, reward, next_state, done):
        # save the old weights to update after
        old_w = copy(self.w)
        old_v = copy(self.v)

        # print('starting update')
        # convert states to features
        phi_s_mask = self.getFeatures_indices(state)
        # next state features
        phi_s_prime_mask = self.getFeatures_indices(next_state)

        # start_time = time.time()
        # the derivative of u(s) = phi(s)
        # phi(s) is a binary vector but we will work with the indices to save computation
        grad_mu_mask = copy(phi_s_mask)
        grad_mu_prime_mask = copy(phi_s_prime_mask)
        # get smaller vectors
        grad_mu = torch.ones(len(grad_mu_mask)) # also equals: phi_s
        grad_mu_prime = torch.ones(len(grad_mu_prime_mask)) # also equals: phi_s_prime


        # action value (I think) or deterministic value
        # mu_s = torch.matmul(self.theta, phi_s)
        mu_s = torch.sum(self.theta[phi_s_mask])
        # mu_s_prime = torch.matmul(self.theta, phi_s_prime)
        mu_s_prime = torch.sum(self.theta[phi_s_prime_mask])


        # state-action features
        phi_sa = grad_mu*(action - mu_s)
        phi_s_prime_mu = grad_mu_prime * (mu_s_prime - mu_s)

        # phisadotu = torch.matmul(phi_sa, self.u)
        phisadotu = (action - mu_s) * torch.sum(self.u[grad_mu_mask])

        # Temporal Difference Error
        # the Q_w_sa_hat simplifies to V(s'), because 
        # μ(s') - μ(s') = 0 and so the advantage = 0
        # removed torch.matmul(phi_s_prime, self.v)  = torch.sum(self.v[phi_s_prime_mask])
        td_error = reward + self.gamma * torch.sum(self.v[phi_s_prime_mask]) - self.Q_w_sa(phi_s_mask, phi_sa)
        # elapsed_time = time.time()-start_time
        # print(f'time/matmuls update : {elapsed_time}')
        # update the weight vectors
        # remvoed torch.matmul(grad_mu, self.w) = torch.sum(self.w[grad_mu_mask])
        #phi_sa_dot_u = torch.matmul(phi_sa, self.u[phi_s_mask]) # get only the u weights that correspond to the state phi_s
        self.theta[phi_s_mask] = self.theta[phi_s_mask] + self.alpha_theta * grad_mu * torch.sum(self.w[grad_mu_mask]) 
        self.w[phi_s_mask] = self.w[phi_s_mask] + self.alpha_w * td_error * phi_sa - self.alpha_w * self.gamma * phi_s_prime_mu * phisadotu \
                                - self.kappa * phi_sa * (self.Q_w_sa(phi_s_mask, phi_sa) - self.prev_Q_w_sa(phi_s_mask, phi_sa)) # phi_sa is a vector of 1's
        self.v[phi_s_mask] = self.v[phi_s_mask] + self.alpha_v * td_error - self.alpha_v * self.gamma * grad_mu * phisadotu \
                            - self.kappa * grad_mu * (torch.sum(self.v[phi_s_mask]) - torch.sum(self.prev_v[phi_s_mask]))# phi_s is a binary vector, so just need to index
        self.u[phi_s_mask] = self.u[phi_s_mask] + self.alpha_u * (td_error - phisadotu) * phi_sa

        self.prev_w = copy(old_w)
        self.prev_v = copy(old_v)

        # track the error
        self.TDError_tracker.append(td_error)
        # print('ending update')

    def __str__(self):
        return 'COPDAC_DDQ'
        

class CopdacGQ(BaseAgent):
    def __init__(self, config):
        super().__init__(config)

    def update(self,  state, action, reward, next_state, done):
        
        # print('starting update')
        # convert states to features
        phi_s_mask = self.getFeatures_indices(state)
        # next state features
        phi_s_prime_mask = self.getFeatures_indices(next_state)

        # start_time = time.time()
        # the derivative of u(s) = phi(s)
        # phi(s) is a binary vector but we will work with the indices to save computation
        grad_mu_mask = copy(phi_s_mask)
        grad_mu_prime_mask = copy(phi_s_prime_mask)
        # get smaller vectors
        grad_mu = torch.ones(len(grad_mu_mask)) # also equals: phi_s
        grad_mu_prime = torch.ones(len(grad_mu_prime_mask)) # also equals: phi_s_prime


        # action value (I think) or deterministic value
        # mu_s = torch.matmul(self.theta, phi_s)
        mu_s = torch.sum(self.theta[phi_s_mask])
        # mu_s_prime = torch.matmul(self.theta, phi_s_prime)
        mu_s_prime = torch.sum(self.theta[phi_s_prime_mask])


        # state-action features
        phi_sa = grad_mu*(action - mu_s)
        phi_s_prime_mu = grad_mu_prime * (mu_s_prime - mu_s)

        # phisadotu = torch.matmul(phi_sa, self.u)
        phisadotu = (action - mu_s) * torch.sum(self.u[grad_mu_mask])

        # Temporal Difference Error
        # the Q_w_sa_hat simplifies to V(s'), because 
        # μ(s') - μ(s') = 0 and so the advantage = 0
        # removed torch.matmul(phi_s_prime, self.v)  = torch.sum(self.v[phi_s_prime_mask])
        td_error = reward + self.gamma * torch.sum(self.v[phi_s_prime_mask]) - self.Q_w_sa(phi_s_mask, phi_sa)
        # elapsed_time = time.time()-start_time
        # print(f'time/matmuls update : {elapsed_time}')
        # update the weight vectors
        # remvoed torch.matmul(grad_mu, self.w) = torch.sum(self.w[grad_mu_mask])
        #phi_sa_dot_u = torch.matmul(phi_sa, self.u[phi_s_mask]) # get only the u weights that correspond to the state phi_s
        self.theta[phi_s_mask] = self.theta[phi_s_mask] + self.alpha_theta * grad_mu * torch.sum(self.w[grad_mu_mask]) 
        self.w[phi_s_mask] = self.w[phi_s_mask] + self.alpha_w * td_error * phi_sa - self.alpha_w * self.gamma * phi_s_prime_mu * phisadotu # phi_sa is a vector of 1's
        self.v[phi_s_mask] = self.v[phi_s_mask] + self.alpha_v * td_error - self.alpha_v * self.gamma * grad_mu * phisadotu # phi_s is a binary vector, so just need to index
        self.u[phi_s_mask] = self.u[phi_s_mask] + self.alpha_u * (td_error - phisadotu) * phi_sa

        # track the error
        self.TDError_tracker.append(td_error)
        # print('ending update')

    def __str__(self):
        return 'COPDAC_GQ'




class RandomAgent(BaseAgent):
    def __init__(self, config):
        super().__init__(config)

    def chooseAction(self, obs):
        # randomly choose action
        return (self.act_high - self.act_low) * torch.rand(1) + self.act_low

 
    def update(self, *args):
        pass


    def __str__(self):
        return "RandomAgent"





