### testing.py

import gym
from ansitoimg.render import ansiToSVG, ansiToRaster, ansiToSVGRaster
import os
from contextlib import redirect_stdout
import codecs




def main():
    env = gym.make('Taxi-v3')
    env.render(mode='human')

    with codecs.open('taxicapturetest.txt', 'w', encoding='mbcs') as f:
        with redirect_stdout(f):
            env.render(mode='human')

    with codecs.open('taxicapturetest.txt', 'r', encoding='mbcs') as f:
        print(f.readlines())
            

    


if __name__ == '__main__':
    main()